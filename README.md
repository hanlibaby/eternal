<h1 align="center"><a href="https://gitee.com/lwjppz/eternal" target="_blank">Eternal</a></h1>

> Eternal 是一款清新简约的个人独立博客系统，给喜欢写博客的同学一个新的选择

## 💡 简介

**Eternal** ` [ɪˈtɜːrnl] `，意为永恒。

一个优秀的开源博客发布应用，值得一试。

## 🎨 声明

> 本项目起初只是为了个人学习使用而开发，但因为课程需要等原因，也就把这个项目作为了课程设计，项目起初是很简陋的，经过本人不断重构、完善，重构、完善......，所以博客的功能越来越多，项目也变得越来越好，有兴趣的童鞋可以看看，有什么问题也可以随时交流

## ✍ 项目介绍

- 项目采用前后端分离模式进行开发，所涉及到的技术栈包含且不限于Vue、SpringBoot、Mybatis-Plus、Ant-Design-Vue.....
- 博客后台管理采用Vue + Ant-Design-Vue开发，后台是通用模板，当然如果不喜欢的话你可以独立开发，项目有完善的API文档提供使用。
- 博客前台可进行多主题适配，这里选用了thymeleaf模板引擎进行渲染，这里为什么不使用freemarker呢？主要是比较熟悉thymeleaf，而且个人感觉语法看着也比freemarker舒服。
- 后续也有考虑搭建app和微信小程序端，目前已在准备使用ColorUI库搭建微信小程序端

##  🗃 案例

http://39.106.117.247/

##  ✨ 功能

- 自定义导航链接 / 自定义文章永久链接 / 自定义页面（发挥你的想象力）
- 文章自定义排序  / 置顶
- 配置站点 SEO 参数 / 公告 / 页脚
- 代码高亮 / 数学公式 / 流程图 / 五线谱
- 多主题，可自行开发
- 多语言 / 国际化
- 友情链接管理
- Hexo / Markdown 导入
- SQL / Markdown 导出
- 更多功能请自行探索😄

## ✌ TODO

- Atom / RSS / Sitemap
- 支持生成导出静态站点，用于发布 GitHub Pages

## 🌞 预览图

![](https://i.loli.net/2020/06/28/sJYBuHCA8DX6mjr.png)

![](https://i.loli.net/2020/06/28/4MGgua3qVcK9Q8f.png)

![](https://i.loli.net/2020/06/28/4lb6KwjnveS9REk.png)

![](https://i.loli.net/2020/06/28/njVu791hOPgxedt.png)

![](https://i.loli.net/2020/06/28/Cnva7NUES5w4lsu.png)

![](https://i.loli.net/2020/06/28/g5MN1kl4K9pz6da.png)

![](https://i.loli.net/2020/06/28/GjImg7lX6preMW8.png)

![](https://i.loli.net/2020/06/28/JPf6GazQmbyOD89.png)

-----------