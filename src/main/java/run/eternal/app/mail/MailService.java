package run.eternal.app.mail;

import java.util.Map;

/**
 * 邮件发送接口
 *
 * @author : lwj
 * @since : 2020-09-05
 */
public interface MailService {

    /**
     * 发送一封纯文本邮件
     *
     * @param to      接受者
     * @param subject 主题
     * @param content 内容
     */
    void sendTextMail(String to, String subject, String content);

    /**
     * 发送一封带html格式的邮件
     *
     * @param to           接受者
     * @param subject      主题
     * @param content      内容
     * @param templateName 模板名称
     */
    void sendTemplateMail(String to, String subject, Map<String, Object> content, String templateName);

    /**
     * 发送一封带附件的邮件
     *
     * @param to             接受者
     * @param subject        主题
     * @param content        内容
     * @param templateName   模板名称
     * @param attachFilePath 附件全路径名
     */
    void sendAttachMail(String to, String subject, Map<String, Object> content, String templateName, String attachFilePath);

    /**
     * 测试邮件服务器是否连接
     */
    void testConnection();
}
