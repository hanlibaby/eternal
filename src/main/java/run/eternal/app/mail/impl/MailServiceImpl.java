package run.eternal.app.mail.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import run.eternal.app.event.options.OptionUpdatedEvent;
import run.eternal.app.service.OptionsService;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

/**
 * @author : lwj
 * @since : 2020-09-05
 */
@Slf4j
@Service
public class MailServiceImpl extends AbstractMailService implements ApplicationListener<OptionUpdatedEvent> {

    public MailServiceImpl(OptionsService optionsService) {
        super(optionsService);
    }

    @Override
    public void sendTextMail(String to, String subject, String content) {
        sendMailTemplate(true, messageHelper -> {
            messageHelper.setSubject(subject);
            messageHelper.setTo(to);
            messageHelper.setText(content);
        });
    }

    @Override
    public void sendTemplateMail(String to, String subject, Map<String, Object> content, String templateName) {
        sendMailTemplate(true, messageHelper -> {
            // build message content with freemarker
            String contentResult = "<p>" + content + "</p>";

            messageHelper.setSubject(subject);
            messageHelper.setTo(to);
            messageHelper.setText(contentResult, true);
        });
    }

    @Override
    public void sendAttachMail(String to, String subject, Map<String, Object> content, String templateName, String attachFilePath) {
        sendMailTemplate(true, messageHelper -> {
            messageHelper.setSubject(subject);
            messageHelper.setTo(to);
            Path attachmentPath = Paths.get(attachFilePath);
            messageHelper.addAttachment(attachmentPath.getFileName().toString(), attachmentPath.toFile());
        });
    }

    @Override
    public void testConnection() {
        super.testConnection();
    }


    @Override
    public void onApplicationEvent(@NonNull OptionUpdatedEvent event) {
        // clear the cached java mail sender
        clearCache();
    }
}

