package run.eternal.app.utils;

import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.data.MutableDataSet;

/**
 * <pre>
 *     markdown 工具类
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-15
 */
public final class MarkDownUtils {

    public static String convertToHtml(String md) {
        MutableDataSet options = new MutableDataSet();

        Parser parser = Parser.builder(options).build();
        HtmlRenderer renderer = HtmlRenderer.builder(options).build();

        Node document = parser.parse(md);
        return renderer.render(document);
    }

    public static String convertToText(String html) {
        String content = html.replaceAll("</?[^>]+>", "");
        content = content.replaceAll("<a>\\s*|\t|\r|\n</a>", "");
        return content;
    }

}
