package run.eternal.app.utils;

import cn.hutool.core.util.URLUtil;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import run.eternal.app.model.support.EternalConst;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static run.eternal.app.model.support.EternalConst.FILE_SEPARATOR;

/**
 * <p>
 * 常用工具类
 * </p>
 *
 * @author : lwj
 * @since : 2020-08-05
 */
public final class EternalUtils {

    public static final String URL_SEPARATOR = "/";

    @NonNull
    public static String ensureBoth(@NonNull String string, @NonNull String bothfix) {
        return ensureBoth(string, bothfix, bothfix);
    }

    @NonNull
    public static String ensureBoth(@NonNull String string, @NonNull String prefix, @NonNull String suffix) {
        return ensureSuffix(ensurePrefix(string, prefix), suffix);
    }

    /**
     * 确保字符串包含前缀
     *
     * @param string 字符串不能为空
     * @param prefix 前缀不能为空
     * @return 字符串包含指定的前缀
     */
    @NonNull
    public static String ensurePrefix(@NonNull String string, @NonNull String prefix) {
        Assert.hasText(string, "字符串不能为空");
        Assert.hasText(prefix, "前缀不能为空");

        return prefix + StringUtils.removeStart(string, prefix);
    }


    /**
     * 确保字符串包含后缀。
     *
     * @param string 字符串不能为空
     * @param suffix 后缀不能为空
     * @return 字符串包含指定的后缀
     */
    @NonNull
    public static String ensureSuffix(@NonNull String string, @NonNull String suffix) {
        Assert.hasText(string, "字符串不能为空");
        Assert.hasText(suffix, "后缀不能为空");

        return StringUtils.removeEnd(string, suffix) + suffix;
    }

    /**
     * Normalize url
     *
     * @param originalUrl original url
     * @return normalized url.
     */
    @NonNull
    public static String normalizeUrl(@NonNull String originalUrl) {
        Assert.hasText(originalUrl, "Original Url must not be blank");

        if (StringUtils.startsWithAny(originalUrl, URL_SEPARATOR, EternalConst.PROTOCOL_HTTPS, EternalConst.PROTOCOL_HTTP)
                && !StringUtils.startsWith(originalUrl, "//")) {
            return originalUrl;
        }

        return URLUtil.normalize(originalUrl);
    }

    /**
     * 获取不带破折号的随机UUID
     *
     * @return 不带破折号的UUID
     */
    @NonNull
    public static String randomUuidWithoutDash() {
        return StringUtils.remove(UUID.randomUUID().toString(), '-');
    }

    /**
     * 将文件分隔符更改为url分隔符
     *
     * @param pathname 全路径名
     * @return url路径名
     */
    public static String changeFileSeparatorToUrlSeparator(@NonNull String pathname) {
        Assert.hasText(pathname, "路径名不能为空！");

        return pathname.replace(FILE_SEPARATOR, "/");
    }

    /**
     * 手机号验证
     *
     * @param str 手机号
     * @return <code>true</code> 若验证通过
     */
    public static boolean isMobile(String str) {
        final Pattern pattern = Pattern.compile("^[1][3,4,5,7,8,9][0-9]{9}$");
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

    /**
     * MD5加密
     *
     * @param strSrc 要加密字符串
     * @return 加密后字符串
     */
    public static String encrypt(String strSrc) throws Exception {
        try {
            char[] hexChars = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
                    '9', 'a', 'b', 'c', 'd', 'e', 'f'};
            byte[] bytes = strSrc.getBytes();
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(bytes);
            bytes = md.digest();
            int j = bytes.length;
            char[] chars = new char[j * 2];
            int k = 0;
            for (byte b : bytes) {
                chars[k++] = hexChars[b >>> 4 & 0xf];
                chars[k++] = hexChars[b & 0xf];
            }
            return new String(chars);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new Exception("MD5加密出错！！+" + e);
        }
    }

    /**
     * 驼峰转下划线
     *
     * @param value 待转换值
     * @return 结果
     */
    public static String camelToUnderscore(String value) {
        if (StringUtils.isBlank(value)) {
            return value;
        }
        String[] arr = StringUtils.splitByCharacterTypeCamelCase(value);
        if (arr.length == 0) {
            return value;
        }
        StringBuilder result = new StringBuilder();
        IntStream.range(0, arr.length).forEach(i -> {
            if (i != arr.length - 1) {
                result.append(arr[i]).append(StringPool.UNDERSCORE);
            } else {
                result.append(arr[i]);
            }
        });
        return StringUtils.lowerCase(result.toString());
    }
}
