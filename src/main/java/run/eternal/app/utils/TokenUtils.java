package run.eternal.app.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

/**
 * <pre>
 *     生成token
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-08
 */
public final class TokenUtils {

    public static String getToken(String id) {
        String token = "";
        token = JWT.create()
                .withAudience(id)
                .withClaim("id", id)
                .sign(Algorithm.HMAC256(id));
        return token;
    }
}
