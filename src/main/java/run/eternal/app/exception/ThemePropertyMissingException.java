package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * Missing theme property exception.
 *
 * @author : lwj
 * @since : 2020-08-21
 */
public class ThemePropertyMissingException extends AbstractEternalException {

    public ThemePropertyMissingException(ResultStatus status) {
        super(status);
    }

    public ThemePropertyMissingException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return status;
    }

}
