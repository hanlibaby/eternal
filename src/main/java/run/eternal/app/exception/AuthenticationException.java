package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * Executed user authenticate exception.
 *
 * @author : lwj
 * @since : 2020-08-18
 */
public class AuthenticationException extends AbstractEternalException {


    public AuthenticationException(ResultStatus status) {
        super(status);
    }

    public AuthenticationException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return status;
    }

}
