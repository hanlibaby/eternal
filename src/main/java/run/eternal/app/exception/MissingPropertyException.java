package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * Missing property exception.
 *
 * @author : lwj
 * @since : 2020-08-19
 */
public class MissingPropertyException extends AbstractEternalException {

    public MissingPropertyException(ResultStatus status) {
        super(status);
    }

    public MissingPropertyException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return status;
    }

}
