package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * Unsupported Media type exception.
 *
 * @author : lwj
 * @since : 2020-08-21
 */
public class UnsupportedMediaTypeException extends AbstractEternalException {

    public UnsupportedMediaTypeException(ResultStatus status) {
        super(status);
    }

    public UnsupportedMediaTypeException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return status;
    }

}
