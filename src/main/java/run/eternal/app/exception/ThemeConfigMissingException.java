package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * Missing theme config file exception.
 *
 * @author : lwj
 * @since : 2020-08-21
 */
public class ThemeConfigMissingException extends AbstractEternalException {

    public ThemeConfigMissingException(ResultStatus status) {
        super(status);
    }

    public ThemeConfigMissingException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return status;
    }

}
