package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * Entity already exist exception
 *
 * @author : lwj
 * @since : 2020-08-18
 */
public class AlreadyExistsException extends AbstractEternalException {


    public AlreadyExistsException(ResultStatus status) {
        super(status);
    }

    public AlreadyExistsException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return status;
    }

}
