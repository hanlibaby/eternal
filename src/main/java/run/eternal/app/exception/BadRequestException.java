package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * Bad request exception.
 *
 * @author : lwj
 * @since : 2020-08-18
 */
public class BadRequestException extends AbstractEternalException {

    public BadRequestException(ResultStatus status) {
        super(status);
    }

    public BadRequestException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return status;
    }

}
