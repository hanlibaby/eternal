package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * File operation exception.
 *
 * @author : lwj
 * @since : 2020-08-18
 */
public class FileOperationException extends AbstractEternalException {

    public FileOperationException(ResultStatus status) {
        super(status);
    }

    public FileOperationException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return status;
    }

}
