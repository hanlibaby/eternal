package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * Entity not find exception.
 *
 * @author : lwj
 * @since : 2020-08-18
 */
public class NotFoundException extends AbstractEternalException {

    public NotFoundException(ResultStatus status) {
        super(status);
    }

    public NotFoundException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return status;
    }
}
