package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * @author : lwj
 * @since : 2020-09-05
 */
public class EmailException extends AbstractEternalException {

    public EmailException(ResultStatus status) {
        super(status);
    }

    public EmailException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return status;
    }
}
