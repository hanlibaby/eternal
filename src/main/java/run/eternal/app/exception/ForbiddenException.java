package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * Forbidden exception.
 *
 * @author : lwj
 * @since : 2020-08-21
 */
public class ForbiddenException extends AbstractEternalException {

    public ForbiddenException(ResultStatus status) {
        super(status);
    }

    public ForbiddenException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return ResultStatus.FORBIDDEN;
    }

}
