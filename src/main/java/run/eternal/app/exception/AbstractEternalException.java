package run.eternal.app.exception;

import lombok.Getter;
import run.eternal.app.model.enums.ResultStatus;

/**
 * Base exception of the project.
 *
 * @author : lwj
 * @since : 2020-08-05
 */
@Getter
public abstract class AbstractEternalException extends RuntimeException {

    public final ResultStatus status;

    public AbstractEternalException(ResultStatus status) {
        super(status.getMessage());
        this.status = status;
    }

    public AbstractEternalException(String msg) {
        super(msg);
        this.status = ResultStatus.ERROR;
    }

    /**
     * Get exception status
     *
     * @return exception status
     */
    public abstract ResultStatus getStatus();

}
