package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * Repeat type exception.
 *
 * @author : lwj
 * @since : 2020-08-18
 */
public class RepeatTypeException extends AbstractEternalException {

    public RepeatTypeException(ResultStatus status) {
        super(status);
    }

    public RepeatTypeException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return status;
    }

}
