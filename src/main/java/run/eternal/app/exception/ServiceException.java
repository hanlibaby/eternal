package run.eternal.app.exception;

import run.eternal.app.model.enums.ResultStatus;

/**
 * Service exception.
 *
 * @author : lwj
 * @since : 2020-08-19
 */
public class ServiceException extends AbstractEternalException {

    public ServiceException(ResultStatus status) {
        super(status);
    }

    public ServiceException(String msg) {
        super(msg);
    }

    @Override
    public ResultStatus getStatus() {
        return status;
    }

}
