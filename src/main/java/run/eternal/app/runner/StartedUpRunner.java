package run.eternal.app.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.ansi.AnsiColor;
import org.springframework.boot.ansi.AnsiOutput;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.ResourceUtils;
import run.eternal.app.config.properties.EternalProperties;
import run.eternal.app.model.properties.PrimaryProperties;
import run.eternal.app.model.support.EternalConst;
import run.eternal.app.service.impl.OptionsServiceImpl;
import run.eternal.app.utils.FileUtils;

import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

/**
 * <pre>
 *     应用程序启动后执行的方法
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-05
 */
@Slf4j
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class StartedUpRunner implements ApplicationRunner {

    @Autowired
    private ConfigurableApplicationContext context;

    @Autowired
    private EternalProperties eternalProperties;

    @Autowired
    private OptionsServiceImpl optionsService;

    private final static String FILE_JAR = "jar";

    @Override
    public void run(ApplicationArguments args) {
        if (context.isActive()) {
            initThemes();
            initDirectory();
            printInfo();
        }
    }

    /**
     * 初始化主题目录
     */
    private void initThemes() {
        // 博客是否已初始化
        Boolean isInstalled = optionsService.getByPropertyOrDefault(PrimaryProperties.IS_INSTALLED, Boolean.class, false);
        try {
            String themeClassPath = ResourceUtils.CLASSPATH_URL_PREFIX + EternalConst.THEME_FOLDER;
            URI themeUri = ResourceUtils.getURL(themeClassPath).toURI();
            log.info("Theme uri: [{}]", themeUri);
            Path source = null;
            if (FILE_JAR.equalsIgnoreCase(themeUri.getScheme())) {
                FileSystem fileSystem = getFileSystem(themeUri);
                source = fileSystem.getPath("/BOOT-INF/classes/" + EternalConst.THEME_FOLDER);
            } else {
                source = Paths.get(themeUri);
            }
            // 创建主题文件夹
            Path themePath = Paths.get(eternalProperties.getWorkDir(), EternalConst.THEME_FOLDER);
            if (Files.notExists(themePath) || !isInstalled) {
                FileUtils.copyFolder(source, themePath);
                log.info("Copied theme folder from [{}] to [{}]", source, themePath);
            } else {
                log.info("Skipped copying theme folder due to existence of theme folder");
            }
        } catch (Exception e) {
            log.error("Initialize internal theme to user path error!", e);
        }
    }

    /**
     * 初始化各目录
     */
    private void initDirectory() {
        Path workPath = Paths.get(eternalProperties.getWorkDir());
        Path backupPath = Paths.get(eternalProperties.getBackupDir());
        Path dataExportPath = Paths.get(eternalProperties.getDataExportDir());
        Path uploadPath = Paths.get(eternalProperties.getUploadDir());
        try {
            if (Files.notExists(workPath)) {
                Files.createDirectories(workPath);
                log.info("Created work directory: [{}]", workPath);
            }
            if (Files.notExists(backupPath)) {
                Files.createDirectories(backupPath);
                log.info("Created backup directory: [{}]", backupPath);
            }
            if (Files.notExists(dataExportPath)) {
                Files.createDirectories(dataExportPath);
                log.info("Created data export directory: [{}]", dataExportPath);
            }
            if (Files.notExists(uploadPath)) {
                Files.createDirectories(uploadPath);
                log.info("Created upload directory: [{}]", uploadPath);
            }
        } catch (IOException ie) {
            throw new RuntimeException("初始化目录失败", ie);
        }
    }

    @NonNull
    private FileSystem getFileSystem(@NonNull URI uri) throws IOException {
        Assert.notNull(uri, "Uri must not be null");

        FileSystem fileSystem;

        try {
            fileSystem = FileSystems.getFileSystem(uri);
        } catch (FileSystemNotFoundException e) {
            fileSystem = FileSystems.newFileSystem(uri, Collections.emptyMap());
        }

        return fileSystem;
    }

    private void printInfo() {
        String blogUrl = optionsService.getBlogBaseUrl();
        log.info(AnsiOutput.toString(AnsiColor.BRIGHT_BLUE, "Eternal started at         ", blogUrl));
        log.info(AnsiOutput.toString(AnsiColor.BRIGHT_BLUE, "Eternal admin started at   ", blogUrl, "/templates/admin"));
        log.info("Eternal has started successfully!，current time：" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }
}
