package run.eternal.app.logger.aspect;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.model.entity.Logs;
import run.eternal.app.model.enums.LogStatus;
import run.eternal.app.service.LogsService;
import run.eternal.app.utils.HttpContextUtils;
import run.eternal.app.utils.IpUtils;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * Record logs of user operations
 *
 * @author : lwj
 * @since : 2020-08-05
 */
@Slf4j
@Aspect
@Component
public class LogAspect {

    @Autowired
    private LogsService logsService;

    private Long startTime;

    @Pointcut("@annotation(run.eternal.app.logger.annotation.Log)")
    public void pointCut() {
    }

    @Before("pointCut()")
    public void before(JoinPoint point) {
        // before method execute start
        startTime = System.currentTimeMillis();
    }

    @AfterReturning("pointCut()")
    public void afterReturning(JoinPoint point) {
        // after method normal return
        handlerLog(point, LogStatus.SUCCESS);
    }

    @AfterThrowing("pointCut()")
    public void afterThrowing(JoinPoint point) {
        // when method throw execute exception
        handlerLog(point, LogStatus.FAIL);
    }

    @Async
    public void handlerLog(JoinPoint point, LogStatus logStatus) {
        // get request
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();

        // get ip address
        String addr = IpUtils.getIpAddr(request);

        // execute time (ms)
        long time = System.currentTimeMillis() - startTime;

        MethodSignature signature = (MethodSignature) point.getSignature();

        // get current method
        Method method = signature.getMethod();

        // get method annotation
        Log logAnnotation = method.getAnnotation(Log.class);

        // judge whether save params
        if (logAnnotation.saveRequestParams()) {
            Logs logs = new Logs();
            logs.setLogType(logAnnotation.logType().getValue());
            logs.setRequestMethod(request.getMethod());
            logs.setStatus(logStatus.getValue());
            logs.setParams(argsToJson(point.getArgs()));
            logs.setIpAddress(addr);
            logs.setRequestTime(time + "ms");
            logs.setOpeMethod(signature.getDeclaringTypeName() + "." + method.getName());

            // save operation log
            logsService.save(logs);
        }
    }

    /**
     * Transform args to json
     *
     * @param args an object array
     * @return a json string
     */
    private String argsToJson(Object[] args) {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        for (Object arg : args) {
            sb.append(JSONObject.toJSONString(arg)).append(" ");
        }
        return sb.append("}").toString();
    }
}
