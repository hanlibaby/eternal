package run.eternal.app.logger.annotation;

import run.eternal.app.model.enums.LogType;

import java.lang.annotation.*;

/**
 * Custom log annotation
 *
 * @author : lwj
 * @since : 2020-08-05
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    /**
     * Log type
     */
    LogType logType() default LogType.OTHER;

    /**
     * Whether save request params, default false
     */
    boolean saveRequestParams() default false;
}
