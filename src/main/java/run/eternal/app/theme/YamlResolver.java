package run.eternal.app.theme;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.springframework.lang.NonNull;

/**
 * Yaml resolver.
 *
 * @author : lwj
 * @since : 2020-08-21
 */
public enum YamlResolver {

    /**
     * a YamlResolver instance
     */
    INSTANCE;

    private final ObjectMapper yamlMapper;

    YamlResolver() {
        // create a default yaml mapper
        yamlMapper = new ObjectMapper(new YAMLFactory());
        yamlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * Get yaml mapper.
     *
     * @return non-null yaml mapper
     */
    @NonNull
    public ObjectMapper getYamlMapper() {
        return yamlMapper;
    }
}
