package run.eternal.app.model.enums;

import org.springframework.lang.NonNull;

/**
 * <pre>
 *     枚举博客日志类型
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-05
 */
public enum LogType implements ValueEnum<String> {

    /**
     * 博客日志类型
     */
    BLOG_INITIALIZED("博客初始化"),

    POST_PUBLISHED("发布文章"),

    POST_EDIT("编辑文章"),

    POST_STATUS("修改文章状态"),

    POST_DELETED("删除文章"),

    TOP_POST("置顶文章"),

    ATTACHMENT_UPLOAD("上传附件"),

    ATTACHMENT_UPLOAD_BATCH("批量上传附件"),

    ATTACHMENT_EDIT("编辑附件"),

    ATTACHMENT_DELETE("删除附件"),

    ATTACHMENT_DELETE_BATCH("批量删除附件"),

    MENU_PUBLISH("新增菜单"),

    MENU_EDIT("修改菜单"),

    MENU_DELETE("删除菜单"),

    CATEGORY_PUBLISHED("新增分类"),

    CATEGORY_EDIT("编辑分类"),

    CATEGORY_DELETE("删除分类"),

    TAG_PUBLISHED("新增标签"),

    TAG_EDIT("编辑标签"),

    TAG_DELETE("删除标签"),

    SHEET_PUBLISHED("页面发布"),

    SHEET_EDIT("编辑页面"),

    SHEET_STATUS("修改页面状态"),

    SHEET_DELETE("删除页面"),

    LINK_PUBLISHED("新增友链"),

    JOURNAL_PUBLISHED("新增随记"),

    JOURNAL_EDIT("修改随记"),

    JOURNAL_DELETE("删除随记"),

    LINK_EDIT("编辑友链"),

    LINK_DELETE("删除友链"),

    PASSWORD_UPDATED("更新博主密码"),

    PROFILE_UPDATED("更新博主信息"),

    LOGGED_IN("管理员登录"),

    LOGGED_OUT("退出登录"),

    THEME_UPLOAD("上传主题"),

    THEME_DELETE("删除主题"),

    THEME_UPDATE("修改主题设置"),

    EXPORT_LOGS("导出日志"),

    OTHER("其他日志");

    private final String value;

    LogType(String value) {
        this.value = value;
    }

    @Override
    @NonNull
    public String getValue() {
        return value;
    }
}
