package run.eternal.app.model.enums;

/**
 * <pre>
 *     日志成功与否
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-12
 */

public enum LogStatus implements ValueEnum<String> {

    /**
     * 日志成功与否状态
     */
    SUCCESS("success"),

    FAIL("failure");

    private final String value;

    LogStatus(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
