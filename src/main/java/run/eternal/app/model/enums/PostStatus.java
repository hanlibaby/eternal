package run.eternal.app.model.enums;

/**
 * <pre>
 *     文章状态
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-10
 */
public enum PostStatus implements ValueEnum<Integer> {

    /**
     * 发布状态
     */
    PUBLISHED(0),

    /**
     * 草稿状态
     */
    DRAFT(1),

    /**
     * 回收状态
     */
    RECYCLE(2);

    private final int value;

    PostStatus(int value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

}
