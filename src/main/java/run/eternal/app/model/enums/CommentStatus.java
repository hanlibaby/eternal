package run.eternal.app.model.enums;

/**
 * <pre>
 *     评论状态
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-10
 */
public enum CommentStatus implements ValueEnum<Integer> {

    /**
     * 待审核状态
     */
    PENDING(0),

    /**
     * 审核通过状态
     */
    PASS(1),

    /**
     * 回收状态
     */
    RECYCLE(2);

    private final int value;

    CommentStatus(int value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

}
