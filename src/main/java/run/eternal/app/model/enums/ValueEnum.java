package run.eternal.app.model.enums;

import org.springframework.util.Assert;

import java.util.stream.Stream;

/**
 * <pre>
 *      值枚举的接口
 * </pre>
 *
 * @param <T> value 类型
 * @author : lwj
 * @since : 2020-08-05
 */

public interface ValueEnum<T> {

    /**
     * 将值转换为相应的枚举
     *
     * @param enumType 枚举类型
     * @param value    数据库字段值
     * @param <V>      value
     * @param <E>      enum
     * @return 对应的枚举
     */
    static <V, E extends ValueEnum<V>> E valueToEnum(Class<E> enumType, V value) {
        Assert.notNull(enumType, "枚举类型不能为空");
        Assert.notNull(value, "值不能为空");
        Assert.isTrue(enumType.isEnum(), "类型必须是枚举类型");

        return Stream.of(enumType.getEnumConstants())
                .filter(item -> item.getValue().equals(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("unknown database value: " + value));
    }

    /**
     * 获取枚举值
     *
     * @return 枚举值
     */
    T getValue();
}
