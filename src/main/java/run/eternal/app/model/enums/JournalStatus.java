package run.eternal.app.model.enums;

/**
 * @author : lwj
 * @since : 2020-08-16
 */
public enum JournalStatus implements ValueEnum<Integer> {

    /**
     * 随记是否公开
     */
    PRIVATE(0),

    PUBLIC(1);

    private final Integer value;

    JournalStatus(Integer value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }
}
