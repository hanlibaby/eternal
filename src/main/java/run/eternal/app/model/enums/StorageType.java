package run.eternal.app.model.enums;

/**
 * <pre>
 *     附件存储位置类型
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-18
 */
public enum StorageType implements ValueEnum<String> {

    /**
     * 本地服务器
     */
    LOCAL("本地"),

    /**
     * sm.ms
     */
    SMMS("sm.ms");

    private final String value;

    StorageType(String value) {
        this.value = value;
    }

    /**
     * Get enum value.
     *
     * @return enum value
     */
    @Override
    public String getValue() {
        return value;
    }

}
