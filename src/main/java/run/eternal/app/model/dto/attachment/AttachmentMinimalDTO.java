package run.eternal.app.model.dto.attachment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-18
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttachmentMinimalDTO {

    private String id;

    private String name;

    private String mediaType;

    private String thumbPath;

}
