package run.eternal.app.model.dto.post;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import run.eternal.app.model.entity.Category;
import run.eternal.app.model.entity.Tag;

import java.util.Date;
import java.util.List;

/**
 * @author : lwj
 * @since : 2020-08-11
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PostDTO {

    private String id;

    private String title;

    private String summary;

    private String formatContent;

    private String originalContent;

    private Integer allowComment;

    private Integer status;

    private Integer isTop;

    private String thumbnail;

    private String url;

    private Integer sort;

    private Long visit;

    private Date gmtCreate;

    private List<String> tagIds;

    private List<String> categoryIds;

    private List<Tag> tags;

    private List<Category> categories;
}
