package run.eternal.app.model.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-12
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginPassDTO {

    private String username;

    private String avatar;

}
