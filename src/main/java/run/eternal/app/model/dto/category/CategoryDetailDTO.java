package run.eternal.app.model.dto.category;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-13
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDetailDTO {

    private String id;

    private String name;

    private String alias;

    private String description;

    private String thumbnail;

}
