package run.eternal.app.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-12
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StatisticDTO {

    private String blogUrl;

    private Integer postCount;

    private Integer commentCount;

    private Integer categoryCount;

    private Integer attachmentCount;

    private Integer tagCount;

    private Integer journalCount;

    private String birthday;

    private Long establishDays;

    private Integer linkCount;

    private Long visitCount;

    private Long likeCount;

}
