package run.eternal.app.model.dto.sheet;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author : lwj
 * @since : 2020-08-15
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SheetDTO {

    private String id;

    private String title;

    private String formatContent;

    private String originalContent;

    private String thumbnail;

    private Integer allowComment;

    private Integer status;

    private String url;

    private Long visit;

    private Long likes;

    private Date gmtCreate;

}
