package run.eternal.app.model.dto.post;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author : lwj
 * @since : 2020-08-12
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PostMinimalDTO {

    private String title;

    private String url;

    private Date gmtCreate;

}
