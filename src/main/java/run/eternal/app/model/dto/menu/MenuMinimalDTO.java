package run.eternal.app.model.dto.menu;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-16
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MenuMinimalDTO {
    
    private String id;
    
    private String name;
    
}
