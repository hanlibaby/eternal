package run.eternal.app.model.dto.log;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author : lwj
 * @since : 2020-08-12
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LogMinimalDTO {

    private String logType;

    private String status;

    private String requestMethod;

    private Date gmtCreate;

}
