package run.eternal.app.model.dto.link;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-16
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LinkDetailDTO {

    private String id;

    private String name;

    private String url;

    private String logo;

    private Integer sort;

    private String description;

}
