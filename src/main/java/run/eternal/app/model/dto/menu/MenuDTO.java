package run.eternal.app.model.dto.menu;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : lwj
 * @since : 2020-08-16
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MenuDTO {

    private String id;

    private String name;

    private String icon;

    private Integer sort;

    private String url;

    @TableField(exist = false)
    List<MenuDTO> children;

}
