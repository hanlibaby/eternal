package run.eternal.app.model.dto.journal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author : lwj
 * @since : 2020-08-16
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JournalDTO {

    private String id;

    private String avatar;

    private String content;

    private Date gmtCreate;

}
