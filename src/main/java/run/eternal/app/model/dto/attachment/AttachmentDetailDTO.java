package run.eternal.app.model.dto.attachment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author : lwj
 * @since : 2020-08-19
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttachmentDetailDTO {

    private String id;

    private String name;

    private String fileKey;

    private String storageType;

    private String mediaType;

    private Integer width;

    private Integer height;

    private Long size;

    private String path;

    private String thumbPath;

    private Date gmtCreate;

}
