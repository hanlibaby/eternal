package run.eternal.app.model.dto.comment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import run.eternal.app.model.dto.post.PostMinimalDTO;

/**
 * @author : lwj
 * @since : 2020-08-13
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PostCommentMinimalDTO {

    private String author;

    private String authorUrl;

    private String content;

    private String gravatarMd5;

    private PostMinimalDTO postMinimalDTO;

}
