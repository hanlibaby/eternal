package run.eternal.app.model.dto.comment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-13
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SheetCommentMinimalDTO {

    private String author;

    private String authorUrl;

    private String content;

    private String gravatarMd5;

    private SheetCommentMinimalDTO sheetCommentMinimalDTO;

}
