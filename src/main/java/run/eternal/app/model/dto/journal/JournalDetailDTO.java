package run.eternal.app.model.dto.journal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-16
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JournalDetailDTO {

    private String id;

    private String content;

    private Integer isPrivate;

}
