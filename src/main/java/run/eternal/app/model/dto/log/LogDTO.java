package run.eternal.app.model.dto.log;

//import com.alibaba.excel.annotation.ExcelIgnore;
//import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author : lwj
 * @since : 2020-08-20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogDTO {

    private String id;

    private String logType;

    private String requestMethod;

    private String status;

    private String params;

    private String ipAddress;

    private String requestTime;

    private String opeMethod;

    private Date gmtCreate;

    private Date gmtModified;
}
