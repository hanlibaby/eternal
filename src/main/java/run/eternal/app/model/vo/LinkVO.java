package run.eternal.app.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-16
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "LinkVO对象", description = "友链表单")
public class LinkVO {

    @ApiModelProperty(value = "友链id")
    private String id;

    @ApiModelProperty(value = "友链名称", required = true)
    private String name;

    @ApiModelProperty(value = "友链地址", required = true)
    private String url;

    @ApiModelProperty(value = "友链logo")
    private String logo;

    @ApiModelProperty(value = "友链排序编号")
    private Integer sort;

    @ApiModelProperty(value = "友链描述")
    private String description;

}
