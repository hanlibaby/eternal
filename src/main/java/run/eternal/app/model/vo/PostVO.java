package run.eternal.app.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : lwj
 * @since : 2020-08-10
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "PostVO对象", description = "文章表单")
public class PostVO {

    @ApiModelProperty(value = "文章id")
    private String id;

    @ApiModelProperty(value = "文章标题")
    private String title;

    @ApiModelProperty(value = "文章摘要")
    private String summary;

    @ApiModelProperty(value = "文章内容的html格式")
    private String formatContent;

    @ApiModelProperty(value = "文章内容的markdown格式")
    private String originalContent;

    @ApiModelProperty(value = "是否允许评论（0-允许，1-不允许）")
    private Integer allowComment;

    @ApiModelProperty(value = "文章状态（0-表示已发布，1-表示草稿，2-表示回收站）")
    private Integer status;

    @ApiModelProperty(value = "文章缩略图")
    private String thumbnail;

    @ApiModelProperty(value = "文章地址")
    private String url;

    @ApiModelProperty(value = "排序（值越大越靠前）")
    private Integer sort;

    @ApiModelProperty(value = "是否置顶")
    private Integer isTop;

    @ApiModelProperty(value = "标签集合")
    private List<String> tagIds;

    @ApiModelProperty(value = "分类集合")
    private List<String> categoryIds;

}
