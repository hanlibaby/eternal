package run.eternal.app.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-11
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "CommentVO对象", description = "评论信息")
public class CommentVO {

    @ApiModelProperty(value = "评论作者")
    private String author;

    @ApiModelProperty(value = "电子邮件")
    private String email;

    @ApiModelProperty(value = "作者地址")
    private String authorUrl;

    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "评论者IP地址")
    private String ipAddress;

    @ApiModelProperty(value = "是否是管理员（0-表示不是管理员，1-表示是管理员）")
    private Boolean isAdmin;

    @ApiModelProperty(value = "评论文章id")
    private String postId;

    @ApiModelProperty(value = "父评论id")
    private String parentId;

}
