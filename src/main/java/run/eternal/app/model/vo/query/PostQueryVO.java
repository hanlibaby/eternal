package run.eternal.app.model.vo.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "PostQueryVO对象", description = "文章查询")
public class PostQueryVO {

    @ApiModelProperty(value = "关键词")
    private String keyword;

    @ApiModelProperty(value = "文章状态（0,1,2）")
    private String postStatus;

    @ApiModelProperty(value = "分类id")
    private String categoryId;

}
