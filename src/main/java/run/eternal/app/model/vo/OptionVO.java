package run.eternal.app.model.vo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "OptionVO对象", description = "博客设置项")
public class OptionVO {

    private String optionKey;

    private String optionValue;

}
