package run.eternal.app.model.vo.query;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "LogQueryVO对象", description = "日志查询")
public class LogQueryVO {

    private String logType;

    private String status;

    private String startTime;

    private String endTime;

}
