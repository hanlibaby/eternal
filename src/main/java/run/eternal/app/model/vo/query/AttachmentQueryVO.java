package run.eternal.app.model.vo.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-15
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "AttachmentQueryVO对象", description = "附件查询")
public class AttachmentQueryVO {

    @ApiModelProperty(value = "关键词")
    private String keyword;

    @ApiModelProperty(value = "附件存储位置类型")
    private String storageType;

    @ApiModelProperty(value = "附件类型")
    private String mediaType;

}
