package run.eternal.app.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-10
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "SheetVO对象", description = "自定义页面表单")
public class SheetVO {

    @ApiModelProperty(value = "页面id")
    private String id;

    @ApiModelProperty(value = "页面标题")
    private String title;

    @ApiModelProperty(value = "页面内容的html格式")
    private String formatContent;

    @ApiModelProperty(value = "页面内容的markdown格式")
    private String originalContent;

    @ApiModelProperty(value = "自定义页面缩略图")
    private String thumbnail;

    @ApiModelProperty(value = "是否允许评论（0-允许，1-不允许）")
    private Integer allowComment;

    @ApiModelProperty(value = "页面状态（0-表示已发布，1-表示草稿，2-表示回收站）")
    private Integer status;

    @ApiModelProperty(value = "页面地址")
    private String url;

}
