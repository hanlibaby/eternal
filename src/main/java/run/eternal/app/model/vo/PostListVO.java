package run.eternal.app.model.vo;

import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;
import run.eternal.app.model.entity.Category;
import run.eternal.app.model.entity.Tag;

import java.util.Date;
import java.util.List;

/**
 * @author : lwj
 * @since : 2020-08-21
 */
@Data
@ToString
public class PostListVO {

    private String id;

    private String title;

    private String summary;

    private String formatContent;

    private String thumbnail;

    private Long visit;

    private String url;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date gmtCreate;

    private List<Category> categories;

    private List<Tag> tags;

}
