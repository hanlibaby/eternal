package run.eternal.app.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-10
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "CategoryVO对象", description = "分类表单")
public class CategoryVO {

    @ApiModelProperty(value = "分类id")
    private String id;

    @ApiModelProperty(value = "分类名", required = true)
    private String name;

    @ApiModelProperty(value = "分类别名")
    private String alias;

    @ApiModelProperty(value = "分类页面封面图")
    private String thumbnail;

    @ApiModelProperty(value = "分类描述")
    private String description;

}
