package run.eternal.app.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <pre>
 *     登录 vo
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-08
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "LoginVo对象", description = "登录表单")
public class LoginVO {

    @ApiModelProperty(value = "用户id")
    private String id;

    @ApiModelProperty(value = "用户名或邮箱")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;
}
