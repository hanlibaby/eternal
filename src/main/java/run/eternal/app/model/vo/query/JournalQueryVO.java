package run.eternal.app.model.vo.query;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : lwj
 * @since : 2020-08-16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "JournalQueryVO对象", description = "随记查询")
public class JournalQueryVO {

    private String keyword;

    private String isPrivate;

}
