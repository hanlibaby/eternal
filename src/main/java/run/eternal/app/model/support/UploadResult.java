package run.eternal.app.model.support;

import lombok.*;
import org.springframework.http.MediaType;

/**
 * @author : lwj
 * @since : 2020-08-18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UploadResult {

    private String filename;

    private String filePath;

    private String key;

    private String thumbPath;

    private String suffix;

    private MediaType mediaType;

    private Long size;

    private Integer width;

    private Integer height;

}
