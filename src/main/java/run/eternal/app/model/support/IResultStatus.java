package run.eternal.app.model.support;

/**
 * <pre>
 *     提供http响应状态接口
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-05
 */
public interface IResultStatus {
    /**
     *  获取响应码
     * @return 响应码
     */
    Integer getCode();

    /**
     * 获取响应信息
     *
     * @return 响应信息
     */
    String getMessage();
}
