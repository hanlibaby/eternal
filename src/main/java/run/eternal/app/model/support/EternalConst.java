package run.eternal.app.model.support;

import org.springframework.http.HttpHeaders;

import java.io.File;

/**
 * <pre>
 *     公共常量
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-05
 */
public class EternalConst {
    /**
     * 用户主目录
     */
    public final static String USER_HOME = System.getProperties().getProperty("user.home");

    public final static String PROTOCOL_HTTPS = "https://";

    public final static String PROTOCOL_HTTP = "http://";

    public final static String URL_SEPARATOR = "/";

    /**
     * Suffix of freemarker template file
     */
    public static final String SUFFIX_FTL = ".ftl";

    /**
     * 临时目录
     */
    public final static String TEMP_DIR = "/tmp/run.eternal.app";

    /**
     * Eternal 备份目录前缀
     */
    public final static String ETERNAL_BACKUP_PREFIX = "eternal-backup-";

    /**
     * Eternal 数据导出目录前缀
     */
    public final static String ETERNAL_DATA_EXPORT_PREFIX = "eternal-data-export-";

    /**
     * 路径分隔符
     */
    public final static String FILE_SEPARATOR = File.separator;

    /**
     * 默认主题名
     */
    public final static String DEFAULT_THEME_ID = "sakura";

    /**
     * 主题属性文件名
     */
    public final static String THEME_PROPERTY_FILE_NAME = "theme.yaml";

    /**
     * 主题属性文件名
     */
    public final static String[] THEME_PROPERTY_FILE_NAMES = {"theme.yaml", "theme.yml"};


    /**
     * 配置文件名
     */
    public final static String[] SETTINGS_NAMES = {"settings.yaml", "settings.yml"};

    /**
     * 主题文件位置
     */
    public final static String THEME_FOLDER = "templates/themes";

    /**
     * 主题缩略图
     */
    public final static String THEME_SCREENSHOTS_NAME = "screenshot";

    /**
     * Default error path.
     */
    public static final String DEFAULT_ERROR_PATH = "common/error/error";

    /**
     * Render template.
     */
    public final static String RENDER_TEMPLATE = "themes/%s/%s";


    /**
     * Admin token header name.
     */
    public final static String ADMIN_TOKEN_HEADER_NAME = "ADMIN-" + HttpHeaders.AUTHORIZATION;

    /**
     * Version constant. (Available in production environment)
     */
    public static final String ETERNAL_VERSION = "1.0";

}
