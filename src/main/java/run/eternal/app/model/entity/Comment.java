package run.eternal.app.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author : lwj
 * @since : 2020-08-11
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@TableName("blog_comments")
@ApiModel(value = "Comments对象", description = "评论对象")
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "评论id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "评论作者")
    private String author;

    @ApiModelProperty(value = "电子邮件")
    private String email;

    @ApiModelProperty(value = "作者地址")
    private String authorUrl;

    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "头像MD5加密")
    private String gravatarMd5;

    @ApiModelProperty(value = "评论者IP地址")
    private String ipAddress;

    @ApiModelProperty(value = "是否是管理员（0-表示不是管理员，1-表示是管理员）")
    private Boolean isAdmin;

    @ApiModelProperty(value = "评论文章id")
    private String postId;

    @ApiModelProperty(value = "父评论id")
    private String parentId;

    @ApiModelProperty(value = "评论状态（0-表示待审核，1-表示已通过，2-表示回收站）")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;
}
