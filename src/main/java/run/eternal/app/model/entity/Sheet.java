package run.eternal.app.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <pre>
 *      自定义页面
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-11
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@TableName("blog_sheets")
@ApiModel(value = "sheet对象", description = "自定义页面")
public class Sheet implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "页面id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "页面标题")
    private String title;

    @ApiModelProperty(value = "页面内容的html格式")
    private String formatContent;

    @ApiModelProperty(value = "页面内容的markdown格式")
    private String originalContent;

    @ApiModelProperty(value = "自定义页面缩略图")
    private String thumbnail;

    @ApiModelProperty(value = "是否允许评论（0-允许，1-不允许）")
    private Integer allowComment;

    @ApiModelProperty(value = "页面状态（0-表示已发布，1-表示草稿，2-表示回收站）")
    private Integer status;

    @ApiModelProperty(value = "页面地址")
    private String url;

    @ApiModelProperty(value = "页面访问人数")
    private Long visit;

    @ApiModelProperty(value = "页面点赞人数")
    private Long likes;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;
}
