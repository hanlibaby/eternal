package run.eternal.app.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * <pre>
 *      文章
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("blog_posts")
@ApiModel(value = "Posts对象", description = "文章")
public class Post implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文章id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "文章标题")
    private String title;

    @ApiModelProperty(value = "文章摘要")
    private String summary;

    @ApiModelProperty(value = "文章内容的html格式")
    private String formatContent;

    @ApiModelProperty(value = "文章内容的markdown格式")
    private String originalContent;

    @ApiModelProperty(value = "是否允许评论（0-允许，1-不允许）")
    private Integer allowComment;

    @ApiModelProperty(value = "文章状态（0-表示已发布，1-表示草稿，2-表示回收站）")
    private Integer status;

    @ApiModelProperty(value = "文章缩略图")
    private String thumbnail;

    @ApiModelProperty(value = "文章地址")
    private String url;

    @ApiModelProperty(value = "是否置顶")
    private Integer isTop;

    @ApiModelProperty(value = "排序（值越大越靠前）")
    private Integer sort;

    @ApiModelProperty(value = "文章访问人数")
    private Long visit;

    @ApiModelProperty(value = "文章点赞人数")
    private Long likes;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;
}
