package run.eternal.app.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <pre>
 *      友链
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@TableName("blog_links")
@ApiModel(value = "Links对象", description = "友链")
public class Link implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "友链id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "友链名称")
    private String name;

    @ApiModelProperty(value = "友链地址")
    private String url;

    @ApiModelProperty(value = "友链logo")
    private String logo;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "友链描述")
    private String description;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;
}
