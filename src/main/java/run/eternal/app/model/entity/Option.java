package run.eternal.app.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <pre>
 *      博客设置
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("blog_options")
@ApiModel(value = "Options对象", description = "博客设置")
public class Option implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "设置内容项id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "设置内容key")
    private String optionKey;

    @ApiModelProperty(value = "设置内容value")
    private String optionValue;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;
}
