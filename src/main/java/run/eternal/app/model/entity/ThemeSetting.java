package run.eternal.app.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <pre>
 * 主题设置
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("blog_theme_settings")
@ApiModel(value = "ThemeSettings对象", description = "主题设置")
public class ThemeSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主题设置id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "主题id")
    private String themeId;

    @ApiModelProperty(value = "主题设置字段key")
    private String settingKey;

    @ApiModelProperty(value = "主题设置字段value")
    private String settingValue;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;
}
