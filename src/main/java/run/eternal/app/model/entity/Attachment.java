package run.eternal.app.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <pre>
 *      附件
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@TableName("blog_attachments")
@ApiModel(value = "Attachments对象", description = "附件")
public class Attachment implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "附件id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "附件名")
    private String name;

    @ApiModelProperty(value = "文件 key")
    private String fileKey;

    @ApiModelProperty(value = "附件存储位置类型")
    private String storageType;

    @ApiModelProperty(value = "附件类型")
    private String mediaType;

    @ApiModelProperty(value = "宽（附件是图片时才有效）")
    private Integer width;

    @ApiModelProperty(value = "高（附件是图片时才有效）")
    private Integer height;

    @ApiModelProperty(value = "大小")
    private Long size;

    @ApiModelProperty(value = "文件后缀（jpg、txt...）")
    private String suffix;

    @ApiModelProperty(value = "地址")
    private String path;

    @ApiModelProperty("缩略图地址")
    private String thumbPath;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;
}
