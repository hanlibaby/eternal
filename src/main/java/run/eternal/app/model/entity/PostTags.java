package run.eternal.app.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <pre>
 *      文章-标签
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@TableName("blog_post_tags")
@ApiModel(value = "PostTags对象", description = "文章-标签")
public class PostTags implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "标签id")
    private String tagId;

    @ApiModelProperty(value = "文章id")
    private String postId;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;
}
