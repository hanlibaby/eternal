package run.eternal.app.model.properties;

import run.eternal.app.model.enums.ValueEnum;

/**
 * @author : lwj
 * @since : 2020-08-20
 */
public enum PostEditorType implements ValueEnum<Integer> {

    /**
     * Markdown editor.
     */
    MARKDOWN(0),

    /**
     * Rich text editor.
     */
    RICHTEXT(1);

    private final Integer value;

    PostEditorType(Integer value) {
        this.value = value;
    }

    /**
     * Get enum value.
     *
     * @return enum value
     */
    @Override
    public Integer getValue() {
        return value;
    }

}

