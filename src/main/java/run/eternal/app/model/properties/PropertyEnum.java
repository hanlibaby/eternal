package run.eternal.app.model.properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import run.eternal.app.model.enums.ValueEnum;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 *     Property
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-08
 */
public interface PropertyEnum extends ValueEnum<String> {

    /**
     * 转换为具有相应类型的值
     *
     * @param value 字符串值不能为空
     * @param type  属性值类型不能为null
     * @param <T>   属性值类型
     * @return property value
     */
    @SuppressWarnings("unchecked")
    static <T> T convertTo(@NonNull String value, @NonNull Class<T> type) {
        Assert.notNull(type, "Type must not be null");

        if ("".equals(value)) {
            return null;
        }

        if (type.isAssignableFrom(String.class)) {
            return (T) value;
        }

        if (type.isAssignableFrom(Integer.class)) {
            return (T) Integer.valueOf(value);
        }

        if (type.isAssignableFrom(Long.class)) {
            return (T) Long.valueOf(value);
        }

        if (type.isAssignableFrom(Boolean.class)) {
            return (T) Boolean.valueOf(value);
        }

        if (type.isAssignableFrom(Short.class)) {
            return (T) Short.valueOf(value);
        }

        if (type.isAssignableFrom(Byte.class)) {
            return (T) Byte.valueOf(value);
        }

        if (type.isAssignableFrom(Double.class)) {
            return (T) Double.valueOf(value);
        }

        if (type.isAssignableFrom(Float.class)) {
            return (T) Float.valueOf(value);
        }

        // Should never happen
        throw new UnsupportedOperationException("Unsupported convention for blog property type:" + type.getName() + " provided");
    }

    /**
     * 转换为枚举
     *
     * @param value        值
     * @param propertyEnum 枚举属性
     * @return 属性值
     */
    @SuppressWarnings("unchecked")
    static Object convertTo(@Nullable String value, @NonNull PropertyEnum propertyEnum) {
        Assert.notNull(propertyEnum, "枚举属性不能为空！");

        if (StringUtils.isBlank(value)) {
            // 设置默认值
            value = propertyEnum.defaultValue();
        }

        try {
            if (propertyEnum.getType().isAssignableFrom(Enum.class)) {
                Class<Enum> type = (Class<Enum>) propertyEnum.getType();
                Enum result = convertToEnum(value, type);
                return result != null ? result : value;
            }

            return convertTo(value, propertyEnum.getType());
        } catch (Exception e) {
            // 返回
            return value;
        }
    }

    /**
     * 转换为枚举
     *
     * @param value 值
     * @param type  枚举属性类型
     * @param <T>   属性类型
     * @return 对应枚举
     */
    @Nullable
    static <T extends Enum<T>> T convertToEnum(@NonNull String value, @NonNull Class<T> type) {
        Assert.hasText(value, "属性值不能为空！");

        try {
            return Enum.valueOf(type, value.toUpperCase());
        } catch (Exception e) {
            // Ignore this exception
            return null;
        }
    }

    /**
     * 获取博客所有属性设置
     *
     * @return 属性设置映射集合
     */
    static Map<String, PropertyEnum> getValuePropertyEnumMap() {
        // 获取所有属性设置
        List<Class<? extends PropertyEnum>> propertyEnumClasses = new LinkedList<>();
        propertyEnumClasses.add(AttachmentProperties.class);
        propertyEnumClasses.add(BlogProperties.class);
        propertyEnumClasses.add(CommentProperties.class);
        propertyEnumClasses.add(EmailProperties.class);
        propertyEnumClasses.add(OtherProperties.class);
        propertyEnumClasses.add(PostProperties.class);
        propertyEnumClasses.add(PrimaryProperties.class);
        propertyEnumClasses.add(SeoProperties.class);

        Map<String, PropertyEnum> result = new HashMap<>(30);

        propertyEnumClasses.forEach(propertyEnumClass -> {
            PropertyEnum[] propertyEnums = propertyEnumClass.getEnumConstants();

            for (PropertyEnum propertyEnum : propertyEnums) {
                result.put(propertyEnum.getValue(), propertyEnum);
            }
        });

        return result;
    }

    /**
     * 检查类型是否受博客属性支持
     *
     * @param type type to check
     * @return true if supports; false else
     */
    static boolean isSupportedType(Class<?> type) {
        if (null == type) {
            return false;
        }
        return type.isAssignableFrom(String.class)
                || type.isAssignableFrom(Number.class)
                || type.isAssignableFrom(Integer.class)
                || type.isAssignableFrom(Long.class)
                || type.isAssignableFrom(Boolean.class)
                || type.isAssignableFrom(Short.class)
                || type.isAssignableFrom(Byte.class)
                || type.isAssignableFrom(Double.class)
                || type.isAssignableFrom(Float.class)
                || type.isAssignableFrom(Enum.class)
                || type.isAssignableFrom(ValueEnum.class);
    }

    /**
     * 获取属性类型
     *
     * @return property type
     */
    Class<?> getType();

    /**
     * 默认值
     *
     * @return default value
     */
    @Nullable
    String defaultValue();

    /**
     * 给定类型的默认值
     *
     * @param propertyType 属性类型不能为null
     * @param <T>          property type
     * @return 给定类型的默认值
     */
    @Nullable
    default <T> T defaultValue(Class<T> propertyType) {
        // Get default value
        String defaultValue = defaultValue();
        if (defaultValue == null) {
            return null;
        }

        // Convert to the given type
        return PropertyEnum.convertTo(defaultValue, propertyType);
    }

}
