package run.eternal.app.model.properties;

/**
 * @author : lwj
 * @since : 2020-08-20
 */
public enum SeoProperties implements PropertyEnum {

    /**
     * 网站关键词
     */
    KEYWORDS("seo_keywords", String.class, ""),

    /**
     * 网站描述信息
     */
    DESCRIPTION("seo_description", String.class, ""),

    /**
     * 是否屏蔽搜索引擎
     */
    SPIDER_DISABLED("seo_spider_disabled", Boolean.class, "false");

    private final String value;

    private final Class<?> type;

    private final String defaultValue;

    SeoProperties(String value, Class<?> type, String defaultValue) {
        this.value = value;
        this.type = type;
        this.defaultValue = defaultValue;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String defaultValue() {
        return defaultValue;
    }

    @Override
    public String getValue() {
        return value;
    }
}
