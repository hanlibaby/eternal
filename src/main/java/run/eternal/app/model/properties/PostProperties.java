package run.eternal.app.model.properties;

/**
 * @author : lwj
 * @since : 2020-08-20
 */
public enum PostProperties implements PropertyEnum {

    /**
     * 文章摘要字数
     */
    SUMMARY_LENGTH("post_summary_length", Integer.class, "150"),

    /**
     * Rss page size.
     */
    RSS_PAGE_SIZE("rss_page_size", Integer.class, "20"),

    /**
     * Rss content type,full or summary.
     */
    RSS_CONTENT_TYPE("rss_content_type", Integer.class, "full"),

    /**
     * 首页文章显示条数
     */
    INDEX_PAGE_SIZE("post_index_page_size", Integer.class, "10"),

    /**
     * 归档页显示条数
     */
    ARCHIVES_PAGE_SIZE("post_archives_page_size", Integer.class, "10"),

    /**
     * 文章首页排序
     */
    INDEX_SORT("post_index_sort", String.class, "gmtCreate");

    private final String value;

    private final Class<?> type;

    private final String defaultValue;

    PostProperties(String value, Class<?> type, String defaultValue) {
        this.value = value;
        this.type = type;
        this.defaultValue = defaultValue;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String defaultValue() {
        return defaultValue;
    }

    @Override
    public String getValue() {
        return value;
    }

}
