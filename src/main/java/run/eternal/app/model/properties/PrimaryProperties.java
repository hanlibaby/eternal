package run.eternal.app.model.properties;

import run.eternal.app.model.enums.StorageType;

import static run.eternal.app.model.support.EternalConst.DEFAULT_THEME_ID;

/**
 * <pre>
 *     主配置
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-08
 */
public enum PrimaryProperties implements PropertyEnum {

    /**
     * 博客是否已经初始化
     */
    IS_INSTALLED("is_installed", Boolean.class, "false"),

    /**
     * 当前主题
     */
    THEME("theme", String.class, DEFAULT_THEME_ID),

    BIRTHDAY("birthday", Long.class, "");

    private final String value;

    private final Class<?> type;

    private final String defaultValue;

    PrimaryProperties(String value, Class<?> type, String defaultValue) {
        this.value = value;
        this.type = type;
        this.defaultValue = defaultValue;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String defaultValue() {
        return defaultValue;
    }

    @Override
    public String getValue() {
        return value;
    }
}
