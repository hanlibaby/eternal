package run.eternal.app.config.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.time.Duration;

import static run.eternal.app.model.support.EternalConst.FILE_SEPARATOR;
import static run.eternal.app.model.support.EternalConst.USER_HOME;

/**
 * Eternal configurations (not recommended to modify this configuration)
 *
 * @author : lwj
 * @since : 2020-08-05
 */
@Data
@Component
@Configuration
@ConfigurationProperties(prefix = "eternal")
public class EternalProperties {

    /**
     * whether open record log
     */
    private boolean openAopLog = true;

    /**
     * word dir
     */
    private String workDir = USER_HOME + FILE_SEPARATOR + ".eternal" + FILE_SEPARATOR;

    /**
     * Eternal data backup dir
     */
    private String backupDir = workDir + "backup" + FILE_SEPARATOR;

    /**
     * Eternal data export dir
     */
    private String dataExportDir = workDir + "data-export" + FILE_SEPARATOR;

    /**
     * Eternal upload dir
     */
    private String uploadDir = workDir + "upload" + FILE_SEPARATOR;

    /**
     * upload dir prefix
     */
    private String uploadUrlPrefix = "upload";

    /**
     * Download Timeout.
     */
    private Duration downloadTimeout = Duration.ofSeconds(30);
}
