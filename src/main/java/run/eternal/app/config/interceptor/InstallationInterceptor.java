package run.eternal.app.config.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import run.eternal.app.model.properties.PrimaryProperties;
import run.eternal.app.service.OptionsService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Verify install
 *
 * @author : lwj
 * @since : 2020-08-12
 */
public class InstallationInterceptor implements HandlerInterceptor {

    @Autowired
    private OptionsService optionsService;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {

        // judge whether the blog is installed
        Boolean isInstall = optionsService.getByPropertyOrDefault(PrimaryProperties.IS_INSTALLED, Boolean.class, false);
        if (isInstall) {
            return true;
        }
        response.sendRedirect("/install");
        return false;
    }
}
