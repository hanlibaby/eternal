package run.eternal.app.config.annotation;

import java.lang.annotation.*;

/**
 * Indicates that the current api required to be logged in to access
 *
 * @author : lwj
 * @since : 2020-08-08
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Authorize {

    /**
     * Whether required login，default true
     */
    boolean required() default true;

}
