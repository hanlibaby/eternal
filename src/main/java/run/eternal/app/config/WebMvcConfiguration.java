package run.eternal.app.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import run.eternal.app.config.properties.EternalProperties;
import run.eternal.app.config.interceptor.AuthenticationInterceptor;
import run.eternal.app.config.interceptor.InstallationInterceptor;

import static run.eternal.app.model.support.EternalConst.FILE_SEPARATOR;
import static run.eternal.app.utils.EternalUtils.*;

/**
 * Web MVC configuration
 *
 * @author : lwj
 * @since : 2020-08-08
 */
@Slf4j
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    private static final String FILE_PROTOCOL = "file:";

    @Autowired
    private EternalProperties eternalProperties;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // add installed interceptor
        registry.addInterceptor(installationInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/api/admin/isInstall")
                .excludePathPatterns("/api/admin/installations")
                .excludePathPatterns("/admin")
                .excludePathPatterns("/install");

        // add api interceptor
        registry.addInterceptor(authenticationInterceptor())
                .addPathPatterns("/api/admin/**")
                .addPathPatterns("/api/content/**");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // get work dir
        String workDir = FILE_PROTOCOL + ensureSuffix(eternalProperties.getWorkDir(), FILE_SEPARATOR);

        // splice upload url pattern
        String uploadUrlPattern = ensureBoth(eternalProperties.getUploadUrlPrefix(), URL_SEPARATOR) + "**";

        // registry admin resource handler
        registry.addResourceHandler("/admin/**")
                .addResourceLocations("classpath:/templates/admin/");

        // register /themes/** resource handler.
        registry.addResourceHandler("/themes/**")
                .addResourceLocations(workDir + "templates/themes/");

        // registry upload source handler
        registry.addResourceHandler(uploadUrlPattern)
                .addResourceLocations(workDir + "upload/");

        // registry swagger-ui api doc source handler
        registry.addResourceHandler("doc.html")
                .addResourceLocations("classpath:/META-INF/resources/");
    }

    @Bean
    public InstallationInterceptor installationInterceptor() {
        return new InstallationInterceptor();
    }

    @Bean
    public AuthenticationInterceptor authenticationInterceptor() {
        return new AuthenticationInterceptor();
    }
}
