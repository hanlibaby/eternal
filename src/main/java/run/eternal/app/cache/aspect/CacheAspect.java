package run.eternal.app.cache.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import run.eternal.app.cache.AbstractStringCacheStore;
import run.eternal.app.cache.annotation.CacheWrite;

/**
 * 自定义缓存注解拦截实现
 *
 * @author : lwj
 * @since : 2020-09-10
 */
@Aspect
@Component
@Slf4j
public class CacheAspect {

    private final static String CACHE_KEY_PREFIX = "cache_eternal_";

    @Autowired
    private AbstractStringCacheStore cacheStore;

    /**
     * 写入缓存
     */
    @Around("@annotation(run.eternal.app.cache.annotation.CacheWrite)")
    public Object cacheWriteAspect(ProceedingJoinPoint joinPoint) throws Throwable {

        // 获取方法签名
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();

        log.debug("Starting caching: [{}]", methodSignature.toString());

        // 获取写入缓存注解
        CacheWrite cacheWrite = methodSignature.getMethod().getAnnotation(CacheWrite.class);

        // 获取自定义缓存key
        String key = cacheWrite.key();

        // TODO
        return null;
    }

}
