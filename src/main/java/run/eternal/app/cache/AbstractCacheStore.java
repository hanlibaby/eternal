package run.eternal.app.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import run.eternal.app.utils.DateUtils;

import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * 抽象缓存仓库
 *
 * @author : lwj
 * @since : 2020-08-29
 */
@Slf4j
public abstract class AbstractCacheStore<K, V> implements CacheStore<K, V> {

    /**
     * 获取缓存包装
     *
     * @param key 缓存key
     * @return 可选的缓存包装
     */
    @NonNull
    abstract Optional<CacheWrapper<V>> getInternal(@NonNull K key);

    /**
     * 写入缓存（缓存内容包装后）
     *
     * @param key          缓存key
     * @param cacheWrapper 缓存包装内容
     */
    abstract void putInternal(@NonNull K key, @NonNull CacheWrapper<V> cacheWrapper);

    /**
     * 写入缓存（若key不存在），不同于 {@link AbstractCacheStore#putInternal(Object, CacheWrapper)}
     *
     * @param key          缓存key
     * @param cacheWrapper 缓存包装内容
     * @return 如果不存在当前key并且写入缓存，则为true；如果key已存在，则为false；否则，则为null
     */
    abstract boolean putInternalIfAbsent(@NonNull K key, @NonNull CacheWrapper<V> cacheWrapper);

    @Override
    public void put(K key, V value) {
        putInternal(key, buildCacheWrapper(value, 0, null));
    }

    @Override
    public void put(K key, V value, long timeout, TimeUnit timeUnit) {
        putInternal(key, buildCacheWrapper(value, timeout, timeUnit));
    }

    @Override
    public Boolean putIfAbsent(K key, V value, long timeout, TimeUnit timeUnit) {
        return putInternalIfAbsent(key, buildCacheWrapper(value, timeout, timeUnit));
    }

    @NonNull
    @Override
    public Optional<V> get(@NonNull K key) {
        Assert.notNull(key, "缓存key不能为空");

        return getInternal(key).map(cacheWrapper -> {
            // 检查缓存到期时间
            if (cacheWrapper.getExpireAt() != null && cacheWrapper.getExpireAt().before(DateUtils.now())) {
                // 已过期将其删除
                log.warn("缓存key: [{}] 已过期", key);

                // 删除
                delete(key);

                // 返回null
                return null;
            }

            return cacheWrapper.getData();
        });
    }

    @NonNull
    private CacheWrapper<V> buildCacheWrapper(@NonNull V value, long timeout, @Nullable TimeUnit timeUnit) {
        Assert.notNull(value, "缓存内容不能为空！");
        Assert.isTrue(timeout >= 0, "缓存过期时间不得小于1");

        Date now = DateUtils.now();

        Date expireAt = null;

        if (timeout > 0 && timeUnit != null) {
            expireAt = DateUtils.add(now, timeout, timeUnit);
        }

        // 包装缓存内容
        CacheWrapper<V> cacheWrapper = new CacheWrapper<>();
        cacheWrapper.setCreateAt(now);
        cacheWrapper.setExpireAt(expireAt);
        cacheWrapper.setData(value);

        return cacheWrapper;
    }

}
