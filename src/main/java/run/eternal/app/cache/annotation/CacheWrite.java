package run.eternal.app.cache.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 自定义写入缓存注解
 *
 * @author : lwj
 * @since : 2020-08-28
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CacheWrite {

    /**
     * 缓存key值
     */
    @AliasFor("alias")
    String key() default "";

    /**
     * key值别名 {@link #key}
     */
    @AliasFor("key")
    String alias() default "";

    /**
     * 缓存过期时间
     */
    long expired() default 10;

    /**
     * 过期时间单位
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;
}
