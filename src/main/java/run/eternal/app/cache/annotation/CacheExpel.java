package run.eternal.app.cache.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 自定义缓存清理注解
 *
 * @author : lwj
 * @since : 2020-08-28
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CacheExpel {

    /**
     * 缓存key值
     */
    @AliasFor("alias")
    String key() default "";

    /**
     * key值别名 {@link #key}
     */
    @AliasFor("key")
    String alias() default "";

}
