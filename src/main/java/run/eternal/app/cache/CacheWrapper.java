package run.eternal.app.cache;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 缓存数据包装
 *
 * @author : lwj
 * @since : 2020-08-28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class CacheWrapper<V> implements Serializable {

    /**
     * 缓存数据
     */
    private V data;

    /**
     * 缓存过期时间
     */
    private Date expireAt;

    /**
     * 缓存创建时间
     */
    private Date createAt;

}
