package run.eternal.app.cache;

import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * 缓存仓库接口
 *
 * @param <K> 缓存key值类型
 * @param <V> 缓存value值类型
 * @author : lwj
 * @since : 2020-08-28
 */
public interface CacheStore<K, V> {

    /**
     * 获取key值获取缓存
     *
     * @param key 缓存key
     * @return 缓存内容
     */
    @NonNull
    Optional<V> get(@NonNull K key);

    /**
     * 写入缓存（key若存在则覆盖其原有的缓存内容），不同于 {@link CacheStore#putIfAbsent(Object, Object, long, TimeUnit)}
     *
     * @param key      缓存key
     * @param value    缓存内容
     * @param timeout  过期时间
     * @param timeUnit 时间单位
     */
    void put(@NonNull K key, @NonNull V value, long timeout, @NonNull TimeUnit timeUnit);

    /**
     * 写入缓存（key若不存在则将其写入缓存）
     *
     * @param key      缓存key
     * @param value    缓存内容
     * @param timeout  过期时间
     * @param timeUnit 时间单位
     * @return 如果不存在当前key并且写入缓存，则为true；如果key已存在，则为false；否则，则为null
     */
    Boolean putIfAbsent(@NonNull K key, @NonNull V value, long timeout, @NonNull TimeUnit timeUnit);

    /**
     * 写入一个不过期的缓存
     *
     * @param key   缓存key
     * @param value 缓存内容
     */
    void put(@NonNull K key, @NonNull V value);

    /**
     * 删除缓存
     *
     * @param key 缓存key
     */
    void delete(@NonNull K key);
}
