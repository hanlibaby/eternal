package run.eternal.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.lang.NonNull;
import run.eternal.app.model.entity.PostTags;
import run.eternal.app.model.entity.Tag;

import java.util.List;

/**
 * <pre>
 *      文章-标签 服务类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
public interface PostTagsService extends IService<PostTags> {

    /**
     * 根据文章id删除 文章-分类 对于字段信息
     *
     * @param id 文章id
     */
    void removeByPostId(@NonNull String id);

    /**
     * 创建 文章-标签 对应信息
     *
     * @param id   文章id
     * @param tags 标签集合
     */
    void createBy(@NonNull String id, @NonNull List<Tag> tags);

    /**
     * 根据文章id获取相关联标签集合
     *
     * @param postId 文章id
     * @return 集合
     */
    @NonNull
    List<PostTags> listTagsByPostId(@NonNull String postId);

    /**
     * 根据标签id获取文章数量
     *
     * @param tagId 标签id
     * @return 数量
     */
    int countByTagId(@NonNull String tagId);

    /**
     * Get post ids by tag id
     *
     * @param id tag id
     * @return post ids
     */
    List<String> listPostIdsByTagId(@NonNull String id);
}
