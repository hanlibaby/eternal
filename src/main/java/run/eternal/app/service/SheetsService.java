package run.eternal.app.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.lang.NonNull;
import run.eternal.app.model.dto.sheet.SheetDTO;
import run.eternal.app.model.entity.Sheet;
import run.eternal.app.model.enums.PostStatus;
import run.eternal.app.model.vo.SheetVO;

/**
 * <pre>
 *     自定义页面 服务类
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-11
 */
public interface SheetsService extends IService<Sheet>, BasePostService {

    /**
     * 通过页面状态查询数量
     *
     * @param sheetStatus 页面状态
     * @return 数量
     */
    Integer countByStatus(@NonNull PostStatus sheetStatus);

    /**
     * 创建自定义页面
     *
     * @param sheetVO 自定义页面数据
     * @return sheet
     */
    Sheet createBy(@NonNull SheetVO sheetVO);

    /**
     * 获取所有自定义页面访问总次数
     *
     * @return 总访问次数
     */
    Long countVisit();

    /**
     * 统计点赞人数
     *
     * @return 点赞人数
     */
    Long countLike();

    /**
     * 分页查询页面
     *
     * @param page 当前页
     * @param size 每页条数
     * @return 集合
     */
    @NonNull
    IPage<SheetDTO> pageBy(int page, int size);

    /**
     * 转化为 SheetDTO
     *
     * @param sheet sheet
     * @return SheetDTO
     */
    @NonNull
    SheetDTO convertTo(@NonNull Sheet sheet);

    /**
     * 更新页面状态
     *
     * @param sheetId 页面id
     * @param status  状态
     */
    void updateStatus(@NonNull String sheetId, int status);

    /**
     * Get sheet count
     *
     * @return sheet count
     */
    @Override
    default int statisticsCount() {
        return count();
    }

    /**
     * Get sheet by slug
     *
     * @param slug sheet slug
     * @return Sheet
     */
    @NonNull
    Sheet getBySlug(@NonNull String slug);
}
