package run.eternal.app.service;

import org.springframework.lang.NonNull;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;

/**
 * @author : lwj
 * @since : 2020-08-23
 */
public interface ToolsService {

    /**
     * Import a post by a markdown file
     *
     * @param file a markdown file
     * @throws IOException maybe throw a IOException
     */
    void importMarkdown(@NonNull MultipartFile file) throws IOException, ParseException;

}
