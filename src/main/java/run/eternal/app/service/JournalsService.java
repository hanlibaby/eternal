package run.eternal.app.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.lang.NonNull;
import run.eternal.app.model.dto.journal.JournalDTO;
import run.eternal.app.model.dto.journal.JournalDetailDTO;
import run.eternal.app.model.entity.Journal;
import run.eternal.app.model.vo.JournalVO;
import run.eternal.app.model.vo.query.JournalQueryVO;

import java.util.List;

/**
 * Journals service
 *
 * @author lwjppz
 * @since 2020-08-08
 */
public interface JournalsService extends IService<Journal> {

    /**
     * Pagination journals
     *
     * @param page current page
     * @param size page size
     * @return records
     */
    @NonNull
    IPage<JournalDTO> pageBy(int page, int size);

    /**
     * Converts to JournalDTO
     *
     * @param journal journal info
     * @return JournalDTO
     */
    @NonNull
    JournalDTO convertTo(@NonNull Journal journal);

    /**
     * Converts to JournalDetailDTO
     *
     * @param journal journal info
     * @return JournalDetailDTO
     */
    @NonNull
    JournalDetailDTO convertToDetail(@NonNull Journal journal);

    /**
     * Pagination conditional query journals
     *
     * @param page           current page
     * @param size           page size
     * @param journalQueryVO condition
     * @return records
     */
    @NonNull
    IPage<JournalDTO> pageByQuery(int page, int size, JournalQueryVO journalQueryVO);

    /**
     * Add journal
     *
     * @param journalVO journal info
     */
    void createBy(@NonNull JournalVO journalVO);

    /**
     * Conditional query journals
     *
     * @param journalQueryVO condition
     * @return journal list
     */
    @NonNull
    List<JournalDTO> queryVO(@NonNull JournalQueryVO journalQueryVO);

    /**
     * Get public journals
     *
     * @return journals list
     */
    List<Journal> listPublic();
}
