package run.eternal.app.service;

import org.springframework.lang.NonNull;
import run.eternal.app.model.dto.StatisticDTO;

/**
 * @author : lwj
 * @since : 2020-08-12
 */
public interface StatisticService {

    /**
     * 获取统计信息
     *
     * @return 统计信息
     */
    @NonNull
    StatisticDTO getStatistic();
}
