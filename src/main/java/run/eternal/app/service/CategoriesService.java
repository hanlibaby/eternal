package run.eternal.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.lang.NonNull;
import run.eternal.app.model.dto.category.CategoryMinimalDTO;
import run.eternal.app.model.dto.category.CategoryDetailDTO;
import run.eternal.app.model.entity.Category;
import run.eternal.app.model.vo.CategoryVO;

import java.util.List;

/**
 * Categories service
 *
 * @author lwjppz
 * @since 2020-08-08
 */
public interface CategoriesService extends IService<Category> {

    /**
     * Add category
     *
     * @param categoryVO category info
     * @return category info
     */
    @NonNull
    Category createBy(@NonNull CategoryVO categoryVO);

    /**
     * Converts to CategoryDTO
     *
     * @param categories category list
     * @return records
     */
    @NonNull
    List<CategoryMinimalDTO> convertTo(List<Category> categories);

    /**
     * Converts to CategoryDetailDTO
     *
     * @param category category info
     * @return CategoryDetailDTO
     */
    @NonNull
    CategoryDetailDTO convertTo(@NonNull Category category);

    /**
     * Update category
     *
     * @param categoryVO category info
     */
    void updateBy(@NonNull CategoryVO categoryVO);

    /**
     * Get category by name
     *
     * @param name category name
     * @return Category
     */
    @NonNull
    Category getByName(@NonNull String name);
}
