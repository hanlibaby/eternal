package run.eternal.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import run.eternal.app.model.dto.menu.MenuDTO;
import run.eternal.app.model.dto.menu.MenuDetailDTO;
import run.eternal.app.model.dto.menu.MenuMinimalDTO;
import run.eternal.app.model.entity.Menu;
import run.eternal.app.model.vo.MenuVO;

import java.util.List;

/**
 * <pre>
 *      菜单 服务类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
public interface MenusService extends IService<Menu> {

    /**
     * 创建新菜单
     *
     * @param menuIndex 菜单信息
     */
    void createBy(@NonNull MenuVO menuIndex);

    /**
     * 转化为 MenuDTO
     *
     * @param menu Menu
     * @return MenuDTO
     */
    @NonNull
    default MenuDTO convertTo(@NonNull Menu menu) {
        Assert.notNull(menu, "菜单信息不能为空！");

        MenuDTO menuDTO = MenuDTO.builder().build();
        BeanUtils.copyProperties(menu, menuDTO);
        return menuDTO;
    }

    /**
     * 转化为 MenuDTO
     *
     * @param list Menu集合
     * @return MenuDTO集合
     */
    @NonNull
    List<MenuDTO> convertTo(@NonNull List<Menu> list);

    /**
     * 转化为MenuDetailDTO
     *
     * @param menu menu
     * @return MenuDetailDTO
     */
    @NonNull
    default MenuDetailDTO convertToDetail(@NonNull Menu menu) {
        Assert.notNull(menu, "菜单信息不能为空！");

        MenuDetailDTO menuDetailDTO = MenuDetailDTO.builder().build();
        BeanUtils.copyProperties(menu, menuDetailDTO);
        return menuDetailDTO;
    }

    /**
     * 更新菜单信息
     *
     * @param menuVO 菜单信息
     */
    void updateBy(@NonNull MenuVO menuVO);

    /**
     * 转化为 MenuMinimalDTO
     *
     * @param menu menu
     * @return MenuMinimalDTO
     */
    @NonNull
    default MenuMinimalDTO convertToMinimal(@NonNull Menu menu) {
        MenuMinimalDTO menuMinimalDTO = MenuMinimalDTO.builder().build();
        BeanUtils.copyProperties(menu, menuMinimalDTO);
        return menuMinimalDTO;
    }

    /**
     * 转化为 MenuMinimalDTO
     *
     * @param list Menu集合
     * @return MenuMinimalDTO集合
     */
    @NonNull
    List<MenuMinimalDTO> convertMinimal(@NonNull List<Menu> list);

    /**
     * 筛选没有子菜单的菜单
     *
     * @param menus 菜单信息集合
     * @return 没有子菜单的菜单集合
     */
    @NonNull
    List<MenuMinimalDTO> selectNoChild(@NonNull List<Menu> menus);
}
