package run.eternal.app.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import run.eternal.app.model.dto.log.LogDTO;
import run.eternal.app.model.dto.log.LogMinimalDTO;
import run.eternal.app.model.entity.Logs;
import run.eternal.app.model.vo.query.LogQueryVO;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Logs service
 *
 * @author lwjppz
 * @since 2020-08-08
 */
public interface LogsService extends IService<Logs> {

    /**
     * Get top logs
     *
     * @param top top log
     * @return log list
     */
    @NonNull
    List<Logs> pageLatest(int top);

    /**
     * Converts to LogMinimalDTO
     *
     * @param logsList log list
     * @return LogMinimalDTO list
     */
    @NonNull
    List<LogMinimalDTO> convertToMinimal(List<Logs> logsList);

    /**
     * query logs by condition
     *
     * @param page       current page
     * @param size       the page of size
     * @param logQueryVO query condition
     * @return an IPage object for logs
     */
    @NonNull
    IPage<LogDTO> getByQuery(int page, int size, LogQueryVO logQueryVO);

    /**
     * Converts to LogDTO object
     *
     * @param logs Log object
     * @return a LogDTO object
     */
    @NonNull
    default LogDTO convertTo(@NonNull Logs logs) {
        Assert.notNull(logs, "logs must not be null!");

        LogDTO logDTO = new LogDTO();
        BeanUtils.copyProperties(logs, logDTO);

        return logDTO;
    }

    /**
     * Converts to LogDTO list
     *
     * @param logs Logs list
     * @return LogDTO lost
     */
    default List<LogDTO> convertTo(List<Logs> logs) {
        return logs.stream().map(this::convertTo).collect(Collectors.toList());
    }

    /**
     * list all logType
     *
     * @return a logType list
     */
    List<String> listLogTypes();
}
