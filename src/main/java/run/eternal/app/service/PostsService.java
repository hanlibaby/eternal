package run.eternal.app.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.multipart.MultipartFile;
import run.eternal.app.model.dto.post.PostDTO;
import run.eternal.app.model.dto.post.PostMinimalDTO;
import run.eternal.app.model.entity.Category;
import run.eternal.app.model.entity.Post;
import run.eternal.app.model.entity.Tag;
import run.eternal.app.model.enums.PostStatus;
import run.eternal.app.model.vo.PostListVO;
import run.eternal.app.model.vo.query.PostQueryVO;
import run.eternal.app.model.vo.PostVO;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 *      文章 服务类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
public interface PostsService extends IService<Post>, BasePostService {

    /**
     * 通过文章状态查询数量
     *
     * @param postStatus 文章状态
     * @return 数量
     */
    Integer countByStatus(@NonNull PostStatus postStatus);

    /**
     * 新增文章
     *
     * @param postVO 文章信息
     */
    default void createBy(@NonNull PostVO postVO) {
        createBy(postVO, postVO.getTagIds(), postVO.getCategoryIds());
    }

    /**
     * 新增文章
     *
     * @param postVO      文章信息
     * @param tagIds      标签集合
     * @param categoryIds 分类集合
     * @return Post
     */
    @NonNull
    PostDTO createBy(@NonNull PostVO postVO, @NonNull List<String> tagIds, @NonNull List<String> categoryIds);

    /**
     * 获取所有文章访问总次数
     *
     * @return 总访问次数
     */
    Long countVisit();

    /**
     * 统计点赞人数
     *
     * @return 点赞人数
     */
    Long countLike();

    /**
     * 获取前top篇文章
     *
     * @param top 前top篇
     * @return 文集合
     */
    @NonNull
    List<Post> pageLatest(int top);

    /**
     * 将文章集合转化为 PostMinimalDTO 类型
     *
     * @param pageLatest 文章集合
     * @return 集合
     */
    @NonNull
    List<PostMinimalDTO> convertToMinimal(List<Post> pageLatest);

    /**
     * 文章分页
     *
     * @param page 当前页
     * @param size 每页条数
     * @return 文章集合
     */
    @NonNull
    IPage<PostDTO> pageBy(int page, int size);

    /**
     * 转化为PostDATO
     *
     * @param post 文章
     * @return PostDTO
     */
    @NonNull
    PostDTO convertTo(@NonNull Post post);

    /**
     * Converts to PostListVO
     *
     * @param postDTO PostDTO
     * @return a PostListVO object
     */
    @NonNull
    PostListVO convertTo(@NonNull PostDTO postDTO);

    /**
     * 转化为PostDATO
     *
     * @param post       文章
     * @param tags       标签集合
     * @param categories 分类集合
     * @return PostDTO
     */
    @NonNull
    PostDTO convertTo(@NonNull Post post, @Nullable List<Tag> tags, @Nullable List<Category> categories);

    /**
     * 文章条件分页
     *
     * @param page        当前页
     * @param size        每页条数
     * @param postQueryVO 查询条件
     * @return 集合
     */
    @NonNull
    IPage<PostDTO> pageByQuery(int page, int size, PostQueryVO postQueryVO);

    /**
     * 更新文章状态
     *
     * @param postId 文章id
     * @param status 文章状态
     */
    void updateStatus(String postId, int status);

    /**
     * 设置文章置顶
     *
     * @param postId 文章id
     * @param isTop  是否置顶
     */
    void setIsTop(@NonNull String postId, int isTop);

    /**
     * Converts to PostListVO list
     *
     * @param records a PostDTO list
     * @return List<PostListVO>
     */
    List<PostListVO> convertToListVO(List<PostDTO> records);

    /**
     * Converts to PostListVO
     *
     * @param postDTO a PostDTO
     * @return PostListVO
     */
    PostListVO convertToListVO(PostDTO postDTO);

    /**
     * Get post by postUrl
     *
     * @param postUrl post url
     * @return post
     */
    PostDTO getByPostUrl(@NonNull String postUrl);

    /**
     * Query prev post, current post, next post
     *
     * @param postUrl post url
     * @return three post
     */
    @NonNull
    List<PostListVO> selectPrevAndNext(@NonNull String postUrl);

    /**
     * Get PostDTO list by post ids
     *
     * @param postIds post ids
     * @return PostDTO list
     */
    List<PostDTO> getByIds(List<String> postIds);

    /**
     * Get post count
     *
     * @return post count
     */
    @Override
    default int statisticsCount() {
        return count();
    }

    /**
     * Converts to ArchivesVO
     *
     * @param list post list
     * @return Archives
     */
    Map<String, Map<String, List<PostListVO>>> convertArchivesVO(List<Post> list);

    /**
     * Query post by keyword
     *
     * @param keyword post keyword
     * @return PostDTO list
     */
    List<PostDTO> listSearch(@NonNull String keyword);

    /**
     * Import a post by markdown file
     *
     * @param markdown markdown string
     * @param fileName file name
     */
    void importMarkdown(@NonNull String markdown, String fileName) throws ParseException;
}
