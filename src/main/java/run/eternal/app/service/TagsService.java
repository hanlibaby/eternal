package run.eternal.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.lang.NonNull;
import run.eternal.app.model.dto.tag.TagDTO;
import run.eternal.app.model.dto.tag.TagDetailDTO;
import run.eternal.app.model.entity.Tag;
import run.eternal.app.model.vo.TagVO;

import java.util.List;

/**
 * <pre>
 *      标签 服务类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
public interface TagsService extends IService<Tag> {

    /**
     * 新增标签
     *
     * @param tagVO 标签信息
     * @return 标签信息
     */
    @NonNull
    Tag createBy(@NonNull TagVO tagVO);

    /**
     * 转化为 TagDTO 类型
     *
     * @param tags 标签信息
     * @return 集合
     */
    @NonNull
    List<TagDTO> convertTo(List<Tag> tags);

    /**
     * 转化为 TagDetailDTO 类型
     *
     * @param tag 标签信息
     * @return TagDetailDTO
     */
    @NonNull
    TagDetailDTO convertTo(@NonNull Tag tag);

    /**
     * 更新标签信息
     *
     * @param tagVO 标签信息
     */
    void updateBy(@NonNull TagVO tagVO);

    /**
     * Get tag by name
     *
     * @param tag tag name
     * @return Tag
     */
    @NonNull
    Tag getByName(@NonNull String tag);
}
