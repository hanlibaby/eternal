package run.eternal.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.lang.NonNull;
import run.eternal.app.model.dto.comment.PostCommentMinimalDTO;
import run.eternal.app.model.dto.comment.SheetCommentMinimalDTO;
import run.eternal.app.model.entity.Comment;
import run.eternal.app.model.enums.CommentStatus;
import run.eternal.app.model.vo.CommentVO;

import java.util.List;

/**
 * Comments service
 *
 * @author lwjppz
 * @since 2020-08-11
 */
public interface CommentsService extends IService<Comment> {

    /**
     * Number of query by comment status
     *
     * @param commentStatus comment status
     * @return comment count
     */
    Integer countByStatus(@NonNull CommentStatus commentStatus);

    /**
     * Add comment
     *
     * @param comment comment info
     * @return comment info
     */
    Comment createBy(@NonNull CommentVO comment);

    /**
     * Get top post comment
     *
     * @param top tpp comment
     * @return comment list
     */
    @NonNull
    List<Comment> pageLatestPost(int top);

    /**
     * Get top post comment
     *
     * @param top           tpp comment
     * @param commentStatus comment status
     * @return comment list
     */
    @NonNull
    List<Comment> pageLatestPostByStatus(int top, CommentStatus commentStatus);

    /**
     * Converts to PostCommentMinimalDTO
     *
     * @param comments comment list
     * @return records
     */
    @NonNull
    List<PostCommentMinimalDTO> convertToPostMinimal(List<Comment> comments);

    /**
     * Get top sheet comment
     *
     * @param top top comment
     * @return comment list
     */
    @NonNull
    List<Comment> pageLatestSheet(int top);

    /**
     * Get top sheet comment
     *
     * @param top           top comment
     * @param commentStatus comment status
     * @return comment status
     */
    @NonNull
    List<Comment> pageLatestSheetByStatus(int top, CommentStatus commentStatus);

    /**
     * Converts to SheetCommentMinimalDTO
     *
     * @param comments comment list
     * @return comment list
     */
    @NonNull
    List<SheetCommentMinimalDTO> convertToSheetMinimal(List<Comment> comments);
}
