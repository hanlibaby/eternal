package run.eternal.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;
import run.eternal.app.exception.MissingPropertyException;
import run.eternal.app.model.entity.Option;
import run.eternal.app.model.properties.PropertyEnum;

import java.util.Map;
import java.util.Optional;

/**
 * <pre>
 *      博客设置 服务类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
public interface OptionsService extends IService<Option> {

    /**
     * 获取博客所有属性设置
     *
     * @return 属性设置映射集合
     */
    @NonNull
    Map<String, Object> listOptions();

    /**
     * 获取博客设置属性值
     *
     * @param property 属性
     * @return Optional<Object>
     */
    @NonNull
    Optional<Object> getByProperty(PropertyEnum property);

    /**
     * 获取博客设置属性值
     *
     * @param property     属性
     * @param propertyType 属性类型
     * @param <T>          T
     * @return Optional<T>
     */
    @NonNull
    <T> Optional<T> getByProperty(PropertyEnum property, Class<T> propertyType);

    /**
     * 获取博客设置属性值
     *
     * @param property     属性
     * @param propertyType 属性类型
     * @param defaultValue 默认值
     * @return value
     */
    @NonNull
    <T> T getByPropertyOrDefault(PropertyEnum property, Class<T> propertyType, T defaultValue);

    /**
     * 获取博客设置属性值
     *
     * @param property     属性
     * @param propertyType 属性类型
     * @return value
     */
    <T> T getByPropertyOrDefault(PropertyEnum property, Class<T> propertyType);

    /**
     * 获取非null的博客属性值
     *
     * @param key key
     * @return value
     */
    @NonNull
    Object getByKeyOfNonNull(@NonNull String key);

    /**
     * 获取博客设置属性值
     *
     * @param property 博客属性
     * @return 值
     * @throws MissingPropertyException 属性值未找到时抛出
     */
    @NonNull
    Object getByPropertyOfNonNull(@NonNull PropertyEnum property);

    /**
     * 获取博客地址
     *
     * @return 博客地址
     */
    @NonNull
    String getBlogBaseUrl();

    /**
     * 保存博客设置属性值
     *
     * @param properties 属性值
     */
    void saveProperties(Map<? extends PropertyEnum, String> properties);

    /**
     * Saves a property.
     *
     * @param property must not be null
     * @param value    could be null
     */
    @Transactional(rollbackFor = Exception.class)
    void saveProperty(@NonNull PropertyEnum property, @Nullable String value);

    /**
     * save blog options
     *
     * @param optionsMap an options map
     */
    void saveByMap(Map<String, Object> optionsMap);


    /**
     * Gets blog title.
     *
     * @return blog title.
     */
    @NonNull
    String getBlogTitle();

    /**
     * Gets global seo keywords.
     *
     * @return keywords
     */
    String getSeoKeywords();

    /**
     * Get global seo description.
     *
     * @return description
     */
    String getSeoDescription();

    /**
     * Gets blog birthday.
     *
     * @return birthday timestamp
     */
    long getBirthday();

    /**
     * Get post size in index page
     */
    int getIndexPostPageSize();
}
