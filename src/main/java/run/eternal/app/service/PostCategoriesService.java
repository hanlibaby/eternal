package run.eternal.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.lang.NonNull;
import run.eternal.app.model.entity.Category;
import run.eternal.app.model.entity.PostCategories;

import java.util.List;

/**
 * <pre>
 *      文章-分类 服务类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
public interface PostCategoriesService extends IService<PostCategories> {

    /**
     * 根据文章id删除 文章-分类 对于字段
     *
     * @param id 文章Id
     */
    void removeByPostId(@NonNull String id);

    /**
     * 根据文章id 创建 文章-分类 对应信息
     *
     * @param id         文章id
     * @param categories 分类集合
     */
    void createBy(@NonNull String id, @NonNull List<Category> categories);

    /**
     * 根据文章id获取所有相关联的分类id
     *
     * @param postId 文章id
     * @return 集合
     */
    @NonNull
    List<PostCategories> listCategoriesByPostId(@NonNull String postId);

    /**
     * 根据文章id获取该分类文章数量
     *
     * @param categoryId 文章id
     * @return 数量
     */
    int countByCategoryId(@NonNull String categoryId);

    /**
     * list post ids by category id
     *
     * @param id category id
     * @return post ids
     */
    List<String> listPostIdsByCategoryId(@NonNull String id);

}
