package run.eternal.app.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;
import run.eternal.app.model.dto.attachment.AttachmentDetailDTO;
import run.eternal.app.model.dto.attachment.AttachmentMinimalDTO;
import run.eternal.app.model.entity.Attachment;
import run.eternal.app.model.vo.query.AttachmentQueryVO;

import java.util.List;

/**
 * Attachment service
 *
 * @author lwjppz
 * @since 2020-08-08
 */
public interface AttachmentsService extends IService<Attachment> {

    /**
     * Get all attachment types
     *
     * @return attachment type list
     */
    @NonNull
    List<String> listMedia();

    /**
     * Get all attachment storage location types
     *
     * @return attachment storage location list
     */
    List<String> listStorage();

    /**
     * Pagination query attachment
     *
     * @param page current page
     * @param size page size
     * @return records
     */
    @NonNull
    IPage<AttachmentMinimalDTO> pageBy(int page, int size);

    /**
     * Pagination condition query attachment
     *
     * @param page              current page
     * @param size              page size
     * @param attachmentQueryVO condition
     * @return records
     */
    @NonNull
    IPage<AttachmentMinimalDTO> pageByQuery(int page, int size, AttachmentQueryVO attachmentQueryVO);

    /**
     * Converts to AttachmentMinimalDTO
     *
     * @param attachment Attachment
     * @return AttachmentMinimalDTO
     */
    @NonNull
    default AttachmentMinimalDTO convertToMinimal(@NonNull Attachment attachment) {
        Assert.notNull(attachment, "attachment must not be null.");

        return AttachmentMinimalDTO.builder()
                .id(attachment.getId())
                .name(attachment.getName())
                .mediaType(attachment.getMediaType())
                .thumbPath(attachment.getThumbPath())
                .build();
    }

    /**
     * Upload attachment
     *
     * @param attachment attachment
     * @return attachment
     */
    @NonNull
    Attachment upload(@NonNull MultipartFile attachment);

    /**
     * Converts to AttachmentDetailDTO
     *
     * @param attachment attachment
     * @return AttachmentDetailDTO
     */
    @NonNull
    default AttachmentDetailDTO convertToDetail(@NonNull Attachment attachment) {
        Assert.notNull(attachment, "attachment must not be null.");

        return AttachmentDetailDTO.builder()
                .id(attachment.getId())
                .name(attachment.getName())
                .storageType(attachment.getStorageType())
                .fileKey(attachment.getFileKey())
                .mediaType(attachment.getMediaType())
                .path(attachment.getPath())
                .height(attachment.getHeight())
                .width(attachment.getWidth())
                .size(attachment.getSize())
                .thumbPath(attachment.getThumbPath())
                .gmtCreate(attachment.getGmtCreate())
                .build();
    }

    /**
     * Permanently delete attachment
     *
     * @param attachmentId attachment id
     */
    void removePermanently(@NonNull String attachmentId);

    /**
     * Batch delete attachments
     *
     * @param ids attachment ids
     */
    void removePermanently(List<String> ids);

    /**
     * Pagination query attachment (picture)
     *
     * @param page              current page
     * @param size              page size
     * @param attachmentQueryVO condition
     * @return records
     */
    IPage<AttachmentMinimalDTO> pageByQueryImage(int page, int size, AttachmentQueryVO attachmentQueryVO);
}
