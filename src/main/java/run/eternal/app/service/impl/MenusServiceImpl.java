package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import run.eternal.app.exception.AbstractEternalException;
import run.eternal.app.exception.AlreadyExistsException;
import run.eternal.app.mapper.MenusMapper;
import run.eternal.app.model.dto.menu.MenuDTO;
import run.eternal.app.model.dto.menu.MenuMinimalDTO;
import run.eternal.app.model.entity.Menu;
import run.eternal.app.model.enums.ResultStatus;
import run.eternal.app.model.vo.MenuVO;
import run.eternal.app.service.MenusService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <pre>
 *      菜单 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Slf4j
@Service
public class MenusServiceImpl extends ServiceImpl<MenusMapper, Menu> implements MenusService {

    @Override
    public void createBy(@NonNull MenuVO menuVO) {
        Assert.notNull(menuVO, "菜单信息不能为空！");

        // 判断当前菜单是否已存在
        LambdaQueryWrapper<Menu> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Menu::getName, menuVO.getName());
        Menu selectOne = baseMapper.selectOne(wrapper);
        if (null != selectOne) {
            log.error("此菜单已存在: {}", selectOne.getName());
            throw new AlreadyExistsException(ResultStatus.MENU_EXIST);
        }

        Menu menu = Menu.builder().build();
        BeanUtils.copyProperties(menuVO, menu);

        baseMapper.insert(menu);
    }

    @NonNull
    @Override
    public List<MenuDTO> convertTo(@NonNull List<Menu> menus) {
        Assert.notNull(menus, "菜单集合不难为空！");

        return menus.stream()
                // 过滤出没有父菜单的菜单
                .filter(menu -> "".equals(menu.getParentId()))
                // 转为MenuDTO
                .map(this::convertTo)
                // 为这些没有父菜单的菜单寻找设置子菜单
                .peek(menuDTO -> menuDTO.setChildren(generateChildren(menus, menuDTO)))
                // 若没有子菜单，则将children设置为null，防止前端表格多渲染图标
                .peek(menuDTO -> {
                    if (menuDTO.getChildren().size() == 0) {
                        menuDTO.setChildren(null);
                    }
                })
                // 按照菜单的排序编号进行排序
                .sorted((a, b) -> a.getSort() - b.getSort())
                .collect(Collectors.toList());
    }

    private List<MenuDTO> generateChildren(List<Menu> menus, MenuDTO root) {
        return menus.stream()
                // 找出菜单root的子菜单
                .filter(menu -> root.getId().equals(menu.getParentId()))
                // 转为MenuDTo
                .map(this::convertTo)
                .collect(Collectors.toList());
    }

    @Override
    public void updateBy(@NonNull MenuVO menuVO) {
        Assert.notNull(menuVO, "菜单信息不能为空！");

        Menu menu = Menu.builder().build();
        BeanUtils.copyProperties(menuVO, menu);

        baseMapper.updateById(menu);
    }

    @NonNull
    @Override
    public List<MenuMinimalDTO> convertMinimal(@NonNull List<Menu> menus) {
        Assert.notNull(menus, "菜单集合信息不能为空！");

        return menus.stream()
                .map(this::convertToMinimal)
                .collect(Collectors.toList());
    }

    @NonNull
    @Override
    public List<MenuMinimalDTO> selectNoChild(@NonNull List<Menu> menus) {
        Assert.notNull(menus, "菜单信息集合不难为空！");

        return menus.stream()
                .filter(menu -> "".equals(menu.getParentId()))
                .map(this::convertToMinimal)
                .collect(Collectors.toList());
    }
}
