package run.eternal.app.service.impl;

import cn.hutool.core.io.IoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;
import run.eternal.app.service.PostsService;
import run.eternal.app.service.ToolsService;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;

/**
 * @author : lwj
 * @since : 2020-08-23
 */
@Service
public class ToolsServiceImpl implements ToolsService {

    @Autowired
    private PostsService postsService;

    @Override
    public void importMarkdown(@NonNull MultipartFile file) throws IOException, ParseException {
        Assert.notNull(file, "file must not be null.");

        String markdown = IoUtil.read(file.getInputStream(), StandardCharsets.UTF_8);

        // TODO import a sheet

        postsService.importMarkdown(markdown, file.getOriginalFilename());
    }

}
