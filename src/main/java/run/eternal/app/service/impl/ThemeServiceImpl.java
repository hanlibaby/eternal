package run.eternal.app.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;
import run.eternal.app.config.properties.EternalProperties;
import run.eternal.app.exception.*;
import run.eternal.app.handler.theme.ThemeConfigResolver;
import run.eternal.app.handler.theme.support.Group;
import run.eternal.app.handler.theme.support.ThemeProperty;
import run.eternal.app.model.properties.BlogProperties;
import run.eternal.app.model.properties.PrimaryProperties;
import run.eternal.app.model.support.EternalConst;
import run.eternal.app.model.support.ThemeFile;
import run.eternal.app.service.OptionsService;
import run.eternal.app.service.ThemeService;
import run.eternal.app.theme.ThemeFileScanner;
import run.eternal.app.theme.ThemePropertyScanner;
import run.eternal.app.utils.FileNameUtils;
import run.eternal.app.utils.FileUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipInputStream;

import static run.eternal.app.model.support.EternalConst.DEFAULT_ERROR_PATH;
import static run.eternal.app.model.support.EternalConst.DEFAULT_THEME_ID;

/**
 * @author : lwj
 * @since : 2020-08-21
 */
@Slf4j
@Service
public class ThemeServiceImpl implements ThemeService {

    /**
     * Theme work directory.
     */
    private final Path themeWorkDir;

    private final OptionsService optionsService;

    private final ThemeConfigResolver themeConfigResolver;

    /**
     * Activated theme id.
     */
    private String activatedThemeId;

    /**
     * Activated theme property.
     */
    private ThemeProperty activatedTheme;

    public ThemeServiceImpl(EternalProperties eternalProperties,
                            ThemeConfigResolver themeConfigResolver,
                            OptionsService optionsService) {
        this.themeConfigResolver = themeConfigResolver;
        this.optionsService = optionsService;
        this.themeWorkDir = Paths.get(eternalProperties.getWorkDir(), THEME_FOLDER);
    }

    @Override
    @NonNull
    public Optional<ThemeProperty> fetchThemePropertyBy(String themeId) {
        if (StringUtils.isBlank(themeId)) {
            return Optional.empty();
        }

        // Get all themes
        List<ThemeProperty> themes = getThemes();

        // filter and find first
        return themes.stream()
                .filter(themeProperty -> StringUtils.equals(themeProperty.getId(), themeId))
                .findFirst();
    }

    @Override
    @NonNull
    public ThemeProperty getThemeOfNonNullBy(@NonNull String themeId) {
        return fetchThemePropertyBy(themeId).orElseThrow(() -> new NotFoundException(themeId + " 主题不存在或已删除！"));
    }

    @Override
    @NonNull
    public List<ThemeProperty> getThemes() {
        List<ThemeProperty> properties = ThemePropertyScanner.INSTANCE.scan(getBasePath(), getActivatedThemeId());
        properties.forEach(themeProperty -> themeProperty.setScreenshots(optionsService.getBlogBaseUrl() + themeProperty.getScreenshots()));
        ThemeProperty[] themeProperties = properties.toArray(new ThemeProperty[0]);
        return Arrays.asList(themeProperties);
    }

    @Override
    @NonNull
    public List<ThemeFile> listThemeFolderBy(@NonNull String themeId) {
        return fetchThemePropertyBy(themeId)
                .map(themeProperty -> ThemeFileScanner.INSTANCE.scan(themeProperty.getThemePath()))
                .orElse(Collections.emptyList());
    }

    @NonNull
    public List<String> listCustomTemplates(@NonNull String themeId) {
        return listCustomTemplates(themeId, CUSTOM_SHEET_PREFIX);
    }

    @Override
    @NonNull
    public List<String> listCustomTemplates(@NonNull String themeId, @NonNull String prefix) {
        return fetchThemePropertyBy(themeId).map(themeProperty -> {
            // Get the theme path
            Path themePath = Paths.get(themeProperty.getThemePath());
            try (Stream<Path> pathStream = Files.list(themePath)) {
                return pathStream.filter(path -> StringUtils.startsWithIgnoreCase(path.getFileName().toString(), prefix))
                        .map(path -> {
                            // Remove prefix
                            String customTemplate = StringUtils.removeStartIgnoreCase(path.getFileName().toString(), prefix);
                            // Remove suffix
                            return StringUtils.removeEndIgnoreCase(customTemplate, EternalConst.SUFFIX_FTL);
                        })
                        .distinct()
                        .collect(Collectors.toList());
            } catch (Exception e) {
                throw new ServiceException("Failed to list files of path " + themePath);
            }
        }).orElse(Collections.emptyList());
    }

    @Override
    public boolean templateExists(String template) {
        if (StringUtils.isBlank(template)) {
            return false;
        }

        return fetchActivatedTheme().map(themeProperty -> {
            // Resolve template path
            Path templatePath = Paths.get(themeProperty.getThemePath(), template);
            // Check the directory
            checkDirectory(templatePath.toString());
            // Check existence
            return Files.exists(templatePath);
        }).orElse(false);
    }

    @Override
    public boolean themeExists(String themeId) {
        return fetchThemePropertyBy(themeId).isPresent();
    }

    @Override
    public Path getBasePath() {
        return themeWorkDir;
    }

    @Override
    public String getTemplateContent(@NonNull String absolutePath) {
        // Check the path
        checkDirectory(absolutePath);

        // Read file
        Path path = Paths.get(absolutePath);
        try {
            return new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new ServiceException("读取模板内容失败 " + absolutePath);
        }
    }

    @Override
    @NonNull
    public String getTemplateContent(@NonNull String themeId, @NonNull String absolutePath) {
        checkDirectory(themeId, absolutePath);

        // Read file
        Path path = Paths.get(absolutePath);
        try {
            return new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new ServiceException("读取模板内容失败 " + absolutePath);
        }
    }

    @Override
    public void saveTemplateContent(@NonNull String absolutePath, String content) {
        // Check the path
        checkDirectory(absolutePath);

        // Write file
        Path path = Paths.get(absolutePath);
        try {
            Files.write(path, content.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new ServiceException("保存模板内容失败 " + absolutePath);
        }
    }

    @Override
    public void saveTemplateContent(@NonNull String themeId, @NonNull String absolutePath, String content) {
        // Check the path
        checkDirectory(themeId, absolutePath);

        // Write file
        Path path = Paths.get(absolutePath);
        try {
            Files.write(path, content.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new ServiceException("保存模板内容失败 " + absolutePath);
        }
    }

    @Override
    public void deleteTheme(@NonNull String themeId) {
        // Get the theme property
        ThemeProperty themeProperty = getThemeOfNonNullBy(themeId);

        if (themeId.equals(getActivatedThemeId())) {
            // Prevent to delete the activated theme
            throw new BadRequestException("不能删除正在使用的主题");
        }

        try {
            // Delete the folder
            FileUtils.deleteFolder(Paths.get(themeProperty.getThemePath()));
        } catch (Exception e) {
            throw new ServiceException("主题删除失败");
        }
    }

    @Override
    @NonNull
    public ThemeProperty add(@NonNull Path themeTmpPath) throws IOException {
        Assert.notNull(themeTmpPath, "Theme temporary path must not be null");
        Assert.isTrue(Files.isDirectory(themeTmpPath), "Theme temporary path must be a directory");

        log.debug("Children path of [{}]:", themeTmpPath);

        try (Stream<Path> pathStream = Files.list(themeTmpPath)) {
            pathStream.forEach(path -> log.debug(path.toString()));
        }

        // Check property config
        ThemeProperty tmpThemeProperty = getProperty(themeTmpPath);

        // Check theme existence
        boolean isExist = getThemes().stream()
                .anyMatch(themeProperty -> themeProperty.getId().equalsIgnoreCase(tmpThemeProperty.getId()));

        if (isExist) {
            throw new AlreadyExistsException("当前安装的主题已存在");
        }

        // Copy the temporary path to current theme folder
        Path targetThemePath = themeWorkDir.resolve(tmpThemeProperty.getId());
        FileUtils.copyFolder(themeTmpPath, targetThemePath);

        // Get property again

        return getProperty(targetThemePath);
    }

    @Override
    @NonNull
    public List<Group> fetchConfig(@NonNull String themeId) {
        Assert.hasText(themeId, "Theme id must not be blank");

        // Get theme property
        ThemeProperty themeProperty = getThemeOfNonNullBy(themeId);

        if (!themeProperty.isHasOptions()) {
            // If this theme dose not has an option, then return empty list
            return Collections.emptyList();
        }

        try {
            for (String optionsName : SETTINGS_NAMES) {
                // Resolve the options path
                Path optionsPath = Paths.get(themeProperty.getThemePath(), optionsName);

                log.debug("Finding options in: [{}]", optionsPath.toString());

                // Check existence
                if (!Files.exists(optionsPath)) {
                    continue;
                }

                // Read the yaml file
                String optionContent = new String(Files.readAllBytes(optionsPath), StandardCharsets.UTF_8);

                // Resolve it
                return themeConfigResolver.resolve(optionContent);
            }

            return Collections.emptyList();
        } catch (IOException e) {
            throw new ServiceException("读取主题配置文件失败");
        }
    }

    @NonNull
    @Override
    public String render(@NonNull String pageName) {
        return fetchActivatedTheme()
                .map(themeProperty -> String.format(RENDER_TEMPLATE, themeProperty.getFolderName(), pageName))
                .orElse(DEFAULT_ERROR_PATH);
    }

    @NonNull
    @Override
    public String getActivatedThemePath() {
        return fetchActivatedTheme()
                .map(themeProperty -> "themes/" + themeProperty.getFolderName())
                .orElse(DEFAULT_THEME_ID);
    }

    @NonNull
    @Override
    public String renderWithSuffix(@NonNull String pageName) {
        // Get activated theme
        ThemeProperty activatedTheme = getActivatedTheme();
        // Build render url
        return String.format(RENDER_TEMPLATE_SUFFIX, activatedTheme.getFolderName(), pageName);
    }

    @Override
    @NonNull
    public String getActivatedThemeId() {
        if (activatedThemeId == null) {
            activatedThemeId = optionsService.getByPropertyOrDefault(PrimaryProperties.THEME, String.class, DEFAULT_THEME_ID);
        }
        return activatedThemeId;
    }

    /**
     * Sets activated theme.
     *
     * @param activatedTheme activated theme
     */
    private void setActivatedTheme(@Nullable ThemeProperty activatedTheme) {
        this.activatedTheme = activatedTheme;
        this.activatedThemeId = Optional.ofNullable(activatedTheme).map(ThemeProperty::getId).orElse(null);
    }

    @Override
    @NonNull
    public ThemeProperty getActivatedTheme() {
        if (activatedTheme == null) {
            synchronized (this) {
                if (activatedTheme == null) {
                    // Get theme property
                    activatedTheme = getThemeOfNonNullBy(getActivatedThemeId());
                }
            }
        }
        return activatedTheme;
    }

    @Override
    @NonNull
    public Optional<ThemeProperty> fetchActivatedTheme() {
        return fetchThemePropertyBy(getActivatedThemeId());
    }

    @Override
    @NonNull
    public ThemeProperty activateTheme(@NonNull String themeId) {
        // Check existence of the theme
        ThemeProperty themeProperty = getThemeOfNonNullBy(themeId);

        // Save the theme to database
        optionsService.saveProperty(PrimaryProperties.THEME, themeId);

        // Set activated theme
        setActivatedTheme(themeProperty);

        return themeProperty;
    }

    @Override
    @NonNull
    public ThemeProperty upload(@NonNull MultipartFile file) {
        Assert.notNull(file, "Multipart file must not be null");

        if (!StringUtils.endsWithIgnoreCase(file.getOriginalFilename(), ".zip")) {
            throw new UnsupportedMediaTypeException("不支持的文件类型: " + file.getContentType());
        }

        ZipInputStream zis = null;
        Path tempPath = null;

        try {
            // Create temp directory
            tempPath = FileUtils.createTempDirectory();
            String basename = FileNameUtils.getBasename(Objects.requireNonNull(file.getOriginalFilename()));
            Path themeTempPath = tempPath.resolve(basename);

            // Check directory traversal
            FileUtils.checkDirectoryTraversal(tempPath, themeTempPath);

            // New zip input stream
            zis = new ZipInputStream(file.getInputStream());

            // Unzip to temp path
            FileUtils.unzip(zis, themeTempPath);

            Path themePath = FileUtils.tryToSkipZipParentFolder(themeTempPath);

            // Go to the base folder and add the theme into system
            return add(themePath);
        } catch (IOException e) {
            throw new ServiceException("主题上传失败: " + file.getOriginalFilename());
        } finally {
            // Close zip input stream
            FileUtils.closeQuietly(zis);
            // Delete folder after testing
            FileUtils.deleteFolderQuietly(tempPath);
        }
    }

    /**
     * Check if directory is valid or not.
     *
     * @param absoluteName must not be blank
     * @throws ForbiddenException throws when the given absolute directory name is invalid
     */
    private void checkDirectory(@NonNull String absoluteName) {
        ThemeProperty activeThemeProperty = getThemeOfNonNullBy(getActivatedThemeId());
        FileUtils.checkDirectoryTraversal(activeThemeProperty.getThemePath(), absoluteName);
    }

    /**
     * Check if directory is valid or not.
     *
     * @param themeId      themeId must not be blank
     * @param absoluteName throws when the given absolute directory name is invalid
     */
    private void checkDirectory(@NonNull String themeId, @NonNull String absoluteName) {
        ThemeProperty themeProperty = getThemeOfNonNullBy(themeId);
        FileUtils.checkDirectoryTraversal(themeProperty.getThemePath(), absoluteName);
    }


    @NonNull
    @Override
    public ThemeProperty update(@NonNull String themeId, @NonNull MultipartFile file) {
        Assert.hasText(themeId, "Theme id must not be blank");
        Assert.notNull(file, "Theme file must not be null");

        if (!StringUtils.endsWithIgnoreCase(file.getOriginalFilename(), ".zip")) {
            throw new UnsupportedMediaTypeException("不支持的文件类型: " + file.getContentType());
        }

        ThemeProperty updatingTheme = getThemeOfNonNullBy(themeId);

        ZipInputStream zis = null;
        Path tempPath = null;

        try {
            // Create temp directory
            tempPath = FileUtils.createTempDirectory();

            String basename = FileNameUtils.getBasename(file.getOriginalFilename());
            Path themeTempPath = tempPath.resolve(basename);

            // Check directory traversal
            FileUtils.checkDirectoryTraversal(tempPath, themeTempPath);

            // New zip input stream
            zis = new ZipInputStream(file.getInputStream());

            // Unzip to temp path
            FileUtils.unzip(zis, themeTempPath);

            Path preparePath = FileUtils.tryToSkipZipParentFolder(themeTempPath);

            ThemeProperty prepareThemeProperty = getProperty(preparePath);

            if (!prepareThemeProperty.getId().equals(updatingTheme.getId())) {
                throw new ServiceException("上传的主题包不是该主题的更新包: " + file.getOriginalFilename());
            }

            // Coping new theme files to old theme folder.
            FileUtils.copyFolder(preparePath, Paths.get(updatingTheme.getThemePath()));

            // Gets theme property again.
            return getProperty(Paths.get(updatingTheme.getThemePath()));
        } catch (IOException e) {
            throw new ServiceException("更新主题失败: " + file.getOriginalFilename());
        } finally {
            // Close zip input stream
            FileUtils.closeQuietly(zis);
            // Delete folder after testing
            FileUtils.deleteFolderQuietly(tempPath);
        }
    }

    /**
     * Gets theme property.
     *
     * @param themePath must not be null
     * @return theme property
     */
    @NonNull
    private ThemeProperty getProperty(@NonNull Path themePath) {
        return ThemePropertyScanner.INSTANCE.fetchThemeProperty(themePath)
                .orElseThrow(() -> new ThemePropertyMissingException(themePath + " 没有说明文件"));
    }

}
