package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import run.eternal.app.event.post.PostVisitEvent;
import run.eternal.app.event.post.SheetVisitEvent;
import run.eternal.app.mapper.SheetsMapper;
import run.eternal.app.model.dto.sheet.SheetDTO;
import run.eternal.app.model.entity.Sheet;
import run.eternal.app.model.enums.PostStatus;
import run.eternal.app.model.vo.SheetVO;
import run.eternal.app.service.SheetsService;
import run.eternal.app.utils.MarkDownUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * <pre>
 *     自定义页面 服务实现类
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-11
 */
@Service
public class SheetsServiceImpl extends ServiceImpl<SheetsMapper, Sheet> implements SheetsService {

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Override
    public Integer countByStatus(@NonNull PostStatus sheetStatus) {
        Assert.notNull(sheetStatus, "页面状态不能为空！");

        LambdaQueryWrapper<Sheet> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Sheet::getStatus, sheetStatus.getValue());

        return baseMapper.selectCount(wrapper);
    }

    @Override
    public Sheet createBy(@NonNull SheetVO sheetVO) {
        Assert.notNull(sheetVO, "自定义页面数据不能为空！");

        Sheet sheet = Sheet.builder().build();
        BeanUtils.copyProperties(sheetVO, sheet);
        sheet.setFormatContent(MarkDownUtils.convertToHtml(sheet.getOriginalContent()));
        if ("".equals(sheetVO.getThumbnail())) {
            sheet.setThumbnail("https://uploadbeta.com/api/pictures/random/?key=BingEverydayWallpaperPicture");
        }
        super.saveOrUpdate(sheet);
        return sheet;
    }

    @Override
    public Long countVisit() {
        return Optional.ofNullable(baseMapper.countVisit()).orElse(0L);
    }

    @Override
    public Long countLike() {
        return Optional.ofNullable(baseMapper.countLike()).orElse(0L);
    }

    @NonNull
    @Override
    public IPage<SheetDTO> pageBy(int page, int size) {
        // 封装分页信息
        IPage<Sheet> sheetPage = new Page<>(page, size);
        // 按照页面发布时间降序排序
        LambdaQueryWrapper<Sheet> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(Sheet::getGmtCreate);

        return getPage(sheetPage, wrapper);
    }


    private IPage<SheetDTO> getPage(IPage<Sheet> sheetPage, LambdaQueryWrapper<Sheet> wrapper) {
        baseMapper.selectPage(sheetPage, wrapper);

        List<Sheet> sheets = sheetPage.getRecords();

        List<SheetDTO> sheetDTOList = sheets.stream()
                .map(this::convertTo)
                .collect(Collectors.toList());

        IPage<SheetDTO> pageable = new Page<>();
        BeanUtils.copyProperties(sheetPage, pageable);
        pageable.setRecords(sheetDTOList);

        return pageable;
    }

    @NonNull
    @Override
    public SheetDTO convertTo(@NonNull Sheet sheet) {
        SheetDTO sheetDTO = SheetDTO.builder().build();
        BeanUtils.copyProperties(sheet, sheetDTO);
        return sheetDTO;
    }

    @Override
    public void updateStatus(@NonNull String sheetId, int status) {
        Assert.notNull(sheetId, "页面id不能为空！");

        Sheet sheet = baseMapper.selectById(sheetId);
        sheet.setStatus(status);
        baseMapper.updateById(sheet);
    }

    @NonNull
    @Override
    public Sheet getBySlug(@NonNull String slug) {
        Assert.notNull(slug, "sheet slug must no be null.");

        LambdaQueryWrapper<Sheet> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(Sheet::getUrl, slug);

        return baseMapper.selectOne(wrapper);
    }

    @Override
    public void increaseVisit(String sheetId) {
        baseMapper.increaseVisit(sheetId);
    }

    @Override
    public void publishVisitEvent(String sheetId) {
        eventPublisher.publishEvent(new SheetVisitEvent(this, sheetId));
    }
}
