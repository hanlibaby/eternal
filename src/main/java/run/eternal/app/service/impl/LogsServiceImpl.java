package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import run.eternal.app.mapper.LogsMapper;
import run.eternal.app.model.dto.log.LogDTO;
import run.eternal.app.model.dto.log.LogMinimalDTO;
import run.eternal.app.model.entity.Logs;
import run.eternal.app.model.vo.query.LogQueryVO;
import run.eternal.app.service.LogsService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <pre>
 *      日志 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Service
public class LogsServiceImpl extends ServiceImpl<LogsMapper, Logs> implements LogsService {

    @NonNull
    @Override
    public List<Logs> pageLatest(int top) {
        // 获取第1页前top条数据
        IPage<Logs> page = new Page<>(1, top);
        // 按照发布时间降序排序
        LambdaQueryWrapper<Logs> wrapper = Wrappers.lambdaQuery();
        wrapper.orderByDesc(Logs::getGmtCreate);
        baseMapper.selectPage(page, wrapper);
        // 返回
        return page.getRecords();
    }

    @NonNull
    @Override
    public List<LogMinimalDTO> convertToMinimal(List<Logs> logsList) {
        if (null == logsList) {
            return Collections.emptyList();
        }

        return logsList.stream()
                .map(logs -> LogMinimalDTO.builder()
                        .logType(logs.getLogType())
                        .status(logs.getStatus()).requestMethod(logs.getRequestMethod()).gmtCreate(logs.getGmtCreate()).build())
                .collect(Collectors.toList());
    }

    @NonNull
    @Override
    public IPage<LogDTO> getByQuery(int page, int size, LogQueryVO logQueryVO) {
        LambdaQueryWrapper<Logs> wrapper = Wrappers.lambdaQuery();

        if (!"".equals(logQueryVO.getLogType())) {
            wrapper.eq(Logs::getLogType, logQueryVO.getLogType());
        }

        if (!"".equals(logQueryVO.getStatus())) {
            wrapper.eq(Logs::getStatus, logQueryVO.getStatus());
        }

        if (!"".equals(logQueryVO.getStartTime()) && !"".equals(logQueryVO.getEndTime())) {
            wrapper.between(Logs::getGmtCreate, logQueryVO.getStartTime(), logQueryVO.getEndTime());
        }

        wrapper.orderByDesc(Logs::getGmtCreate);

        IPage<Logs> logsPage = new Page<>(page, size);

        baseMapper.selectPage(logsPage, wrapper);

        List<Logs> records = logsPage.getRecords();

        List<LogDTO> logs = records.stream()
                .map(this::convertTo)
                .collect(Collectors.toList());

        IPage<LogDTO> pageable = new Page<>();
        BeanUtils.copyProperties(logsPage, pageable);
        pageable.setRecords(logs);

        return pageable;
    }

    @Override
    public List<String> listLogTypes() {
        return baseMapper.listLogTypes();
    }

}
