package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import run.eternal.app.mapper.JournalsMapper;
import run.eternal.app.model.dto.journal.JournalDTO;
import run.eternal.app.model.dto.journal.JournalDetailDTO;
import run.eternal.app.model.entity.Journal;
import run.eternal.app.model.entity.User;
import run.eternal.app.model.enums.JournalStatus;
import run.eternal.app.model.vo.JournalVO;
import run.eternal.app.model.vo.query.JournalQueryVO;
import run.eternal.app.service.JournalsService;
import run.eternal.app.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <pre>
 *       随记 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Service
public class JournalsServiceImpl extends ServiceImpl<JournalsMapper, Journal> implements JournalsService {

    @Autowired
    private UserService userService;

    @NonNull
    @Override
    public IPage<JournalDTO> pageBy(int page, int size) {
        // 封装分页信息
        IPage<Journal> journalPage = new Page<>(page, size);
        // 按照发布时间降序排序
        LambdaQueryWrapper<Journal> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(Journal::getGmtCreate);

        return getPage(journalPage, wrapper);
    }

    @NonNull
    private IPage<JournalDetailDTO> pageDetailBy(int page, int size) {
        IPage<Journal> journalPage = new Page<>();
        baseMapper.selectPage(journalPage, null);

        List<Journal> journals = journalPage.getRecords();
        List<JournalDetailDTO> journalDetailDTOList = journals.stream()
                .map(this::convertToDetail)
                .collect(Collectors.toList());

        IPage<JournalDetailDTO> pageable = new Page<>();
        BeanUtils.copyProperties(journalPage, pageable);
        pageable.setRecords(journalDetailDTOList);

        return pageable;
    }

    private IPage<JournalDTO> getPage(IPage<Journal> journalPage, LambdaQueryWrapper<Journal> wrapper) {
        baseMapper.selectPage(journalPage, wrapper);

        List<Journal> journals = journalPage.getRecords();
        List<JournalDTO> journalDTOList = journals.stream()
                .map(this::convertTo)
                .collect(Collectors.toList());

        IPage<JournalDTO> pageable = new Page<>();
        BeanUtils.copyProperties(journalPage, pageable);
        pageable.setRecords(journalDTOList);

        return pageable;
    }

    @NonNull
    @Override
    public JournalDTO convertTo(@NonNull Journal journal) {
        Assert.notNull(journal, "随记信息不能为空！");

        JournalDTO journalDTO = JournalDTO.builder().build();
        BeanUtils.copyProperties(journal, journalDTO);

        User user = userService.getCurrentUser().orElse(null);
        assert user != null;
        journalDTO.setAvatar(user.getAvatar());
        return journalDTO;
    }

    @NonNull
    @Override
    public JournalDetailDTO convertToDetail(@NonNull Journal journal) {
        JournalDetailDTO journalDetailDTO = JournalDetailDTO.builder().build();
        BeanUtils.copyProperties(journal, journalDetailDTO);

        return journalDetailDTO;
    }

    @NonNull
    @Override
    public IPage<JournalDTO> pageByQuery(int page, int size, JournalQueryVO journalQueryVO) {
        if ("".equals(journalQueryVO.getKeyword()) && "".equals(journalQueryVO.getIsPrivate())) {
            return pageBy(page, size);
        }

        IPage<Journal> iPage = new Page<>(page, size);
        LambdaQueryWrapper<Journal> wrapper = getWrapper(journalQueryVO);

        return getPage(iPage, wrapper);
    }

    @Override
    public void createBy(@NonNull JournalVO journalVO) {
        Assert.notNull(journalVO, "随记信息不能为空！");

        Journal journal = Journal.builder().build();
        BeanUtils.copyProperties(journalVO, journal);

        super.saveOrUpdate(journal);
    }

    @NonNull
    @Override
    public List<JournalDTO> queryVO(@NonNull JournalQueryVO journalQueryVO) {
        if ("".equals(journalQueryVO.getKeyword()) && "".equals(journalQueryVO.getIsPrivate())) {
            LambdaQueryWrapper<Journal> wrapper = new LambdaQueryWrapper<>();
            wrapper.orderByDesc(Journal::getGmtCreate);
            return convertToList(wrapper);
        }

        return convertToList(getWrapper(journalQueryVO));
    }

    @Override
    public List<Journal> listPublic() {
        LambdaQueryWrapper<Journal> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(Journal::getIsPrivate, JournalStatus.PUBLIC.getValue());

        return baseMapper.selectList(wrapper);
    }

    private LambdaQueryWrapper<Journal> getWrapper(JournalQueryVO journalQueryVO) {
        LambdaQueryWrapper<Journal> wrapper = new LambdaQueryWrapper<>();
        if ("".equals(journalQueryVO.getIsPrivate())) {
            wrapper.like(Journal::getContent, journalQueryVO.getKeyword());
        } else {
            wrapper.like(Journal::getContent, journalQueryVO.getKeyword())
                    .eq(Journal::getIsPrivate, Integer.parseInt(journalQueryVO.getIsPrivate()));
        }
        wrapper.orderByDesc(Journal::getGmtCreate);
        return wrapper;
    }

    private List<JournalDTO> convertToList(LambdaQueryWrapper<Journal> wrapper) {
        List<Journal> journals = baseMapper.selectList(wrapper);
        return journals.stream()
                .map(this::convertTo)
                .collect(Collectors.toList());
    }
}
