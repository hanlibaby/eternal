package run.eternal.app.service.impl;

import cn.hutool.core.io.IoUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;
import run.eternal.app.event.post.PostVisitEvent;
import run.eternal.app.exception.AbstractEternalException;
import run.eternal.app.mapper.PostsMapper;
import run.eternal.app.model.dto.post.PostDTO;
import run.eternal.app.model.dto.post.PostMinimalDTO;
import run.eternal.app.model.entity.*;
import run.eternal.app.model.enums.PostStatus;
import run.eternal.app.model.properties.PostProperties;
import run.eternal.app.model.vo.PostListVO;
import run.eternal.app.model.vo.query.PostQueryVO;
import run.eternal.app.model.vo.PostVO;
import run.eternal.app.service.*;
import run.eternal.app.utils.MarkDownUtils;
import run.eternal.app.utils.ServiceUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <pre>
 *      文章 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Slf4j
@Service
public class PostsServiceImpl extends ServiceImpl<PostsMapper, Post> implements PostsService {

    @Autowired
    private TagsService tagsService;

    @Autowired
    private CategoriesService categoriesService;

    @Autowired
    private PostCategoriesService postCategoriesService;

    @Autowired
    private PostTagsService postTagsService;

    @Autowired
    private OptionsService optionsService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Override
    public Integer countByStatus(@NonNull PostStatus postStatus) {
        Assert.notNull(postStatus, "文章状态不能为空！");

        LambdaQueryWrapper<Post> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Post::getStatus, postStatus.getValue());

        return baseMapper.selectCount(wrapper);
    }

    @Transactional(rollbackFor = AbstractEternalException.class)
    @NonNull
    @Override
    public PostDTO createBy(@NonNull PostVO postVO,
                            @NonNull List<String> tagIds,
                            @NonNull List<String> categoryIds) {
        Assert.notNull(postVO, "文章信息不能为空！");
        Assert.notNull(tagIds, "标签集合不能为空！");
        Assert.notNull(categoryIds, "分类集合不能为空！");

        Post post = new Post();
        BeanUtils.copyProperties(postVO, post);
        if ("".equals(post.getThumbnail())) {
            post.setThumbnail("https://uploadbeta.com/api/pictures/random/?key=BingEverydayWallpaperPicture");
        }

        // get post summary count
        int length = optionsService.getByPropertyOrDefault(PostProperties.SUMMARY_LENGTH, Integer.class, 150);

        // converts to html
        String html = MarkDownUtils.convertToHtml(post.getOriginalContent());

        // converts to text
        String text = MarkDownUtils.convertToText(html);

        // set post summary
        post.setSummary(text.length() >= length ? text.substring(0, length) : text);

        // set post formatContent
        post.setFormatContent(MarkDownUtils.convertToHtml(post.getOriginalContent()));

        // 新增或更新文章
        super.saveOrUpdate(post);

        // 根据文章id删除 文章-分类 对应信息
        postCategoriesService.removeByPostId(post.getId());

        // 根据文章id删除 文章-标签 对应信息
        postTagsService.removeByPostId(post.getId());

        // 根据 tagIds 获取所有tag
        List<Tag> tags = tagsService.listByIds(tagIds);

        // 根据 categoryIds 获取所有category
        List<Category> categories = categoriesService.listByIds(categoryIds);

        // 根据tags，创建 文章-标签 对应关系
        postTagsService.createBy(post.getId(), tags);

        // 根据categories，创建 文章-分类 对应关系
        postCategoriesService.createBy(post.getId(), categories);

        return convertTo(post, tags, categories);
    }

    @Override
    public Long countVisit() {
        return Optional.of(baseMapper.countVisit()).orElse(0L);
    }

    @Override
    public Long countLike() {
        return Optional.of(baseMapper.countLike()).orElse(0L);
    }

    @NonNull
    @Override
    public List<Post> pageLatest(int top) {
        // 获取第1页前top条数据
        IPage<Post> postPage = new Page<>(1, top);
        // 按照发布时间降序排序
        LambdaQueryWrapper<Post> wrapper = Wrappers.lambdaQuery();
        wrapper.orderByDesc(Post::getGmtCreate);

        baseMapper.selectPage(postPage, wrapper);
        // 返回
        return postPage.getRecords();
    }

    @NonNull
    @Override
    public List<PostMinimalDTO> convertToMinimal(List<Post> postList) {
        if (null == postList) {
            return Collections.emptyList();
        }

        return postList.stream()
                .map(post -> PostMinimalDTO.builder().title(post.getTitle()).url(post.getUrl()).gmtCreate(post.getGmtCreate()).build())
                .collect(Collectors.toList());
    }

    @NonNull
    @Override
    public IPage<PostDTO> pageBy(int page, int size) {
        // 封装分页信息
        IPage<Post> postPage = new Page<>(page, size);
        // 按照文章是否置顶、排序编号、发布时间降序排序
        LambdaQueryWrapper<Post> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(Post::getIsTop).orderByDesc(Post::getSort).orderByDesc(Post::getGmtCreate);

        return getPage(postPage, wrapper);
    }

    @NonNull
    @Override
    public IPage<PostDTO> pageByQuery(int page, int size, PostQueryVO postQueryVO) {
        if ("".equals(postQueryVO.getKeyword()) && "".equals(postQueryVO.getPostStatus()) && "".equals(postQueryVO.getCategoryId())) {
            return pageBy(page, size);
        }

        // 构造条件通过分类id获取所有在该分类下的文章id
        LambdaQueryWrapper<PostCategories> categoryWrapper = new LambdaQueryWrapper<>();
        if (!"".equals(postQueryVO.getCategoryId())) {
            categoryWrapper.eq(PostCategories::getCategoryId, postQueryVO.getCategoryId());
        }
        List<PostCategories> postCategories = postCategoriesService.list(categoryWrapper);
        // 获取该分类下所有文章id
        List<String> postIds = ServiceUtils.fetchProperty(postCategories, PostCategories::getPostId);

        // 构造分页查询条件
        IPage<Post> postPage = new Page<>(page, size);
        LambdaQueryWrapper<Post> postWrapper = new LambdaQueryWrapper<>();
        postWrapper.like(Post::getTitle, postQueryVO.getKeyword())
                .in(Post::getStatus, "".equals(postQueryVO.getPostStatus()) ? Arrays.asList(0, 1, 2) : postQueryVO.getPostStatus())
                .in(Post::getId, postIds);

        return getPage(postPage, postWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateStatus(String postId, int status) {
        Assert.notNull(postId, "文章id不能为空！");

        Post post = baseMapper.selectById(postId);
        post.setStatus(status);
        baseMapper.updateById(post);
    }

    @Override
    public void setIsTop(@NonNull String postId, int isTop) {
        Assert.notNull(postId, "文章id不能为空！");

        Post post = baseMapper.selectById(postId);
        post.setIsTop(isTop);

        baseMapper.updateById(post);
    }

    @Override
    public List<PostListVO> convertToListVO(List<PostDTO> records) {
        return records.stream()
                .filter(postDTO -> postDTO.getStatus().equals(PostStatus.PUBLISHED.getValue()))
                .map(this::convertTo)
                .collect(Collectors.toList());
    }

    @Override
    public PostListVO convertToListVO(PostDTO postDTO) {
        return convertTo(postDTO);
    }

    @NonNull
    @Override
    public PostDTO getByPostUrl(@NonNull String postUrl) {
        Assert.notNull(postUrl, "post url must not be null.");

        LambdaQueryWrapper<Post> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(Post::getUrl, postUrl);

        return convertTo(baseMapper.selectOne(wrapper));
    }

    @NonNull
    @Override
    public List<PostListVO> selectPrevAndNext(@NonNull String postUrl) {
        Assert.notNull(postUrl, "post url must not be null.");
        LambdaQueryWrapper<Post> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(Post::getUrl, postUrl);
        Post post = baseMapper.selectOne(wrapper);

        List<Post> posts = baseMapper.selectPrevAndNext(post.getId());
        List<PostDTO> result = posts.stream()
                .map(this::convertTo)
                .collect(Collectors.toList());

        return convertToListVO(result);
    }

    @Override
    public List<PostDTO> getByIds(List<String> postIds) {
        List<Post> posts = baseMapper.selectBatchIds(postIds);
        return posts.stream().filter(post -> post.getStatus().equals(PostStatus.PUBLISHED.getValue())).map(this::convertTo).collect(Collectors.toList());
    }

    @Override
    public Map<String, Map<String, List<PostListVO>>> convertArchivesVO(List<Post> list) {
        DateFormat format = DateFormat.getDateInstance();

        // Get post years
        Set<String> years = list.stream().map(post -> format.format(post.getGmtCreate()).substring(0, 4)).collect(Collectors.toSet());

        Map<String, Map<String, List<PostListVO>>> map = new HashMap<>(5);

        years.forEach(year -> {
            List<Post> postsMonth = list.stream()
                    .filter(post -> format.format(post.getGmtCreate()).substring(0, 4).equals(year))
                    .collect(Collectors.toList());

            Set<String> months = postsMonth.stream().map(post -> format.format(post.getGmtCreate()).substring(5, 6)).collect(Collectors.toSet());

            Map<String, List<PostListVO>> monthMap = new HashMap<>(12);
            months.forEach(month -> {
                List<PostListVO> postList =
                        postsMonth.stream()
                                .filter(post -> format.format(post.getGmtCreate()).substring(5, 6).equals(month))
                                .filter(post -> post.getStatus().equals(PostStatus.PUBLISHED.getValue()))
                                .map(this::convertTo)
                                .map(this::convertToListVO)
                                .sorted((a, b) -> b.getGmtCreate().compareTo(a.getGmtCreate()))
                                .collect(Collectors.toList());
                monthMap.put(month, postList);
            });

            map.put(year, monthMap);
        });
        return map;
    }

    @Override
    public List<PostDTO> listSearch(@NonNull String keyword) {
        Assert.notNull(keyword, "keyword must not be null.");

        LambdaQueryWrapper<Post> wrapper = Wrappers.lambdaQuery();
        wrapper.like(Post::getTitle, keyword);

        List<Post> posts = baseMapper.selectList(wrapper);

        return posts.stream()
                .map(this::convertTo)
                .collect(Collectors.toList());
    }

    @Override
    public void increaseVisit(String postId) {
        baseMapper.increaseVisit(postId);
    }

    @Override
    public void publishVisitEvent(String postId) {
        eventPublisher.publishEvent(new PostVisitEvent(this, postId));
    }

    private IPage<PostDTO> getPage(IPage<Post> postPage, LambdaQueryWrapper<Post> wrapper) {
        baseMapper.selectPage(postPage, wrapper);
        List<Post> posts = postPage.getRecords();

        List<PostDTO> postDTOList = posts.stream()
                .map(this::convertTo)
                .collect(Collectors.toList());

        IPage<PostDTO> pageable = new Page<>();
        BeanUtils.copyProperties(postPage, pageable);
        pageable.setRecords(postDTOList);
        return pageable;
    }

    @NonNull
    @Override
    public PostDTO convertTo(@NonNull Post post) {
        Assert.notNull(post, "文章不能为空！");

        // 获取文章-分类集合
        List<PostCategories> postCategories = postCategoriesService.listCategoriesByPostId(post.getId());

        // 获取分类ids
        List<String> categoryIds = ServiceUtils.fetchProperty(postCategories, PostCategories::getCategoryId);

        // 获取文章-标签集合
        List<PostTags> postTags = postTagsService.listTagsByPostId(post.getId());

        // 获取标签ids
        List<String> tagIds = ServiceUtils.fetchProperty(postTags, PostTags::getTagId);

        // 获取分类集合
        List<Category> categories = categoriesService.listByIds(categoryIds);

        // 获取标签集合
        List<Tag> tags = tagsService.listByIds(tagIds);

        return convertTo(post, tags, categories);
    }

    @NonNull
    @Override
    public PostListVO convertTo(@NonNull PostDTO postDTO) {
        PostListVO postListVO = new PostListVO();
        BeanUtils.copyProperties(postDTO, postListVO);
        return postListVO;
    }

    @NonNull
    @Override
    public PostDTO convertTo(@NonNull Post post,
                             @Nullable List<Tag> tags,
                             @Nullable List<Category> categories) {
        Assert.notNull(post, "文章不能为空！");

        PostDTO postDTO = PostDTO.builder().build();
        BeanUtils.copyProperties(post, postDTO);

        // 提取ID
        List<String> tagIds = ServiceUtils.fetchProperty(tags, Tag::getId);
        List<String> categoryIds = ServiceUtils.fetchProperty(categories, Category::getId);

        // 设置文章 tag tagIds
        postDTO.setTagIds(tagIds);
        postDTO.setTags(tags);

        // 设置文章 category categoryIds
        postDTO.setCategoryIds(categoryIds);
        postDTO.setCategories(categories);

        return postDTO;
    }

    @Override
    public void importMarkdown(@NonNull String markdown, @NonNull String fileName) throws ParseException {
        Assert.notNull(markdown, "Markdown document must not be null");

        Post post = new Post();

        List<String> tags;

        List<String> categories;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        int start = 0, end = -1, endCount = 0;
        String match = "\r\n", endFlag = "---";
        while ((end = markdown.indexOf(match, start)) != -1 && endCount != 2) {
            String str = markdown.substring(start, end);

            if (!endFlag.equals(str)) {
                String key = str.substring(0, str.indexOf(":"));
                String value = str.substring(str.lastIndexOf(": ") + 1).trim();
                switch (key) {
                    case "title":
                        post.setTitle(value);
                        log.info(value);
                        break;
                    case "date":
                        post.setGmtCreate(format.parse(value));
                        log.info(String.valueOf(format.parse(value)));
                        break;
                    case "tags":
                        tags = parseArray(value);
                        log.info(String.valueOf(tags));
                        break;
                    case "categories":
                        categories = parseArray(value);
                        log.info(String.valueOf(categories));
                        break;
                    case "index_img":
                        post.setThumbnail(value);
                        log.info(value);
                        break;
                    default:
                }
            } else {
                endCount++;
            }
            start = end + match.length();
        }
    }

    private List<String> parseArray(String value) {
        List<String> list = new ArrayList<>();

        int matchIndex = 0, matchCount = 0;
        char match = '\'';
        for (int i = 0; i < value.length(); i++) {
            if (match == value.charAt(i)) {
                matchCount++;
                if (matchCount == 2) {
                    list.add(value.substring(matchIndex + 1, i));
                    matchIndex = i + 1;
                    matchCount = 0;
                } else {
                    matchIndex = i;
                }
            }
        }
        return list;
    }
}
