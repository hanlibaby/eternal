package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import run.eternal.app.exception.AbstractEternalException;
import run.eternal.app.mapper.PostCategoriesMapper;
import run.eternal.app.model.entity.Category;
import run.eternal.app.model.entity.PostCategories;
import run.eternal.app.service.PostCategoriesService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <pre>
 *      文章-分类 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Slf4j
@Service
public class PostCategoriesServiceImpl extends ServiceImpl<PostCategoriesMapper, PostCategories> implements PostCategoriesService {

    @Transactional(rollbackFor = AbstractEternalException.class)
    @Override
    public void removeByPostId(@NonNull String id) {
        Assert.notNull(id, "文章id不能为空！");

        LambdaQueryWrapper<PostCategories> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(PostCategories::getPostId, id);
        int delete = baseMapper.delete(wrapper);
        log.info("根据文章id：{}，共删除：{} 条数据", id, delete);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void createBy(@NonNull String id, @NonNull List<Category> categories) {
        Assert.notNull(id, "文章id不能为空！");
        Assert.notNull(categories, "分类集合不能为空！");

        categories.forEach(category -> {
            PostCategories postCategories = PostCategories.builder()
                    .postId(id)
                    .categoryId(category.getId())
                    .build();
            baseMapper.insert(postCategories);
        });
    }

    @NonNull
    @Override
    public List<PostCategories> listCategoriesByPostId(@NonNull String postId) {
        Assert.notNull(postId, "文章id不能为空！");

        LambdaQueryWrapper<PostCategories> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(PostCategories::getPostId, postId);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public int countByCategoryId(@NonNull String categoryId) {
        Assert.notNull(categoryId, "分类id不能为空！");

        LambdaQueryWrapper<PostCategories> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(PostCategories::getCategoryId, categoryId);
        return baseMapper.selectCount(wrapper);
    }

    @Override
    public List<String> listPostIdsByCategoryId(@NonNull String id) {
        Assert.notNull(id, "category id must not be null.");

        LambdaQueryWrapper<PostCategories> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(PostCategories::getCategoryId, id);

        List<PostCategories> postCategories = baseMapper.selectList(wrapper);

        return postCategories.stream()
                .map(PostCategories::getPostId)
                .collect(Collectors.toList());
    }
}
