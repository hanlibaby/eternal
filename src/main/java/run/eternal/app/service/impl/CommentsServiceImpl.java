package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import run.eternal.app.mapper.CommentsMapper;
import run.eternal.app.model.dto.comment.PostCommentMinimalDTO;
import run.eternal.app.model.dto.comment.SheetCommentMinimalDTO;
import run.eternal.app.model.dto.post.PostMinimalDTO;
import run.eternal.app.model.entity.Comment;
import run.eternal.app.model.entity.Post;
import run.eternal.app.model.entity.Sheet;
import run.eternal.app.model.enums.CommentStatus;
import run.eternal.app.model.properties.BlogProperties;
import run.eternal.app.model.properties.CommentProperties;
import run.eternal.app.model.vo.CommentVO;
import run.eternal.app.service.*;
import run.eternal.app.utils.HttpContextUtils;
import run.eternal.app.utils.IpUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <pre>
 *     服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-11
 */
@Service
public class CommentsServiceImpl extends ServiceImpl<CommentsMapper, Comment> implements CommentsService {

    @Autowired
    private UserService userService;

    @Autowired
    private PostsService postsService;

    @Autowired
    private SheetsService sheetsService;

    @Autowired
    private OptionsService optionsService;

    @Override
    public Integer countByStatus(@NonNull CommentStatus commentStatus) {
        Assert.notNull(commentStatus, "评论状态不能为空！");

        LambdaQueryWrapper<Comment> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Comment::getStatus, commentStatus.getValue());

        return baseMapper.selectCount(wrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Comment createBy(@NonNull CommentVO commentVO) {
        Assert.notNull(commentVO, "评论信息不能为空！");

        Comment comment = Comment.builder().build();
        BeanUtils.copyProperties(commentVO, comment);

        // 设置评论者ip地址
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        comment.setIpAddress(IpUtils.getIpAddr(request));

        // 设置gravatarMd5
        comment.setGravatarMd5(DigestUtils.md5Hex(comment.getEmail()));

        // 设置当前评论者是否是管理员
        boolean isAdmin = userService.verifyAdmin(commentVO.getAuthor(), commentVO.getEmail());
        if (isAdmin) {
            // 是管理员
            comment.setIsAdmin(true);
            comment.setStatus(CommentStatus.PASS.getValue());
        } else {
            // 不是管理员
            comment.setIsAdmin(false);
            // whether open comment check
            Boolean needCheck = optionsService.getByPropertyOrDefault(CommentProperties.NEW_NEED_CHECK, Boolean.class, true);
            comment.setStatus(needCheck ? CommentStatus.PENDING.getValue() : CommentStatus.PASS.getValue());
        }

        // 新增评论
        baseMapper.insert(comment);

        return comment;
    }

    @NonNull
    @Override
    public List<Comment> pageLatestPost(int top) {
        return baseMapper.listPostComments(top);
    }

    @NonNull
    @Override
    public List<Comment> pageLatestPostByStatus(int top, CommentStatus commentStatus) {
        return baseMapper.listPostCommentsByStatus(top, commentStatus.getValue());
    }

    @NonNull
    @Override
    public List<PostCommentMinimalDTO> convertToPostMinimal(List<Comment> comments) {
        if (null == comments) {
            return Collections.emptyList();
        }

        return comments.stream()
                .map(comment -> {
                    Post post = postsService.getById(comment.getPostId());
                    PostMinimalDTO postMinimalDTO = PostMinimalDTO.builder().build();
                    BeanUtils.copyProperties(post, postMinimalDTO);
                    return PostCommentMinimalDTO.builder()
                            .author(comment.getAuthor())
                            .authorUrl(comment.getAuthorUrl())
                            .content(comment.getContent())
                            .gravatarMd5(comment.getGravatarMd5())
                            .postMinimalDTO(postMinimalDTO)
                            .build();
                }).collect(Collectors.toList());
    }

    @NonNull
    @Override
    public List<Comment> pageLatestSheet(int top) {
        return baseMapper.listSheetComments(top);
    }

    @NonNull
    @Override
    public List<Comment> pageLatestSheetByStatus(int top, CommentStatus commentStatus) {
        return baseMapper.listSheetCommentsByStatus(top, commentStatus.getValue());
    }

    @NonNull
    @Override
    public List<SheetCommentMinimalDTO> convertToSheetMinimal(List<Comment> comments) {
        if (null == comments) {
            return Collections.emptyList();
        }

        return comments.stream()
                .map(comment -> {
                    Sheet post = sheetsService.getById(comment.getPostId());
                    SheetCommentMinimalDTO sheetCommentMinimalDTO = SheetCommentMinimalDTO.builder().build();
                    BeanUtils.copyProperties(post, sheetCommentMinimalDTO);
                    return SheetCommentMinimalDTO.builder()
                            .author(comment.getAuthor())
                            .authorUrl(comment.getAuthorUrl())
                            .content(comment.getContent())
                            .gravatarMd5(comment.getGravatarMd5())
                            .sheetCommentMinimalDTO(sheetCommentMinimalDTO)
                            .build();
                }).collect(Collectors.toList());
    }

}
