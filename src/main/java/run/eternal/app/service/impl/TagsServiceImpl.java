package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import run.eternal.app.exception.AbstractEternalException;
import run.eternal.app.exception.AlreadyExistsException;
import run.eternal.app.mapper.TagsMapper;
import run.eternal.app.model.dto.tag.TagDTO;
import run.eternal.app.model.dto.tag.TagDetailDTO;
import run.eternal.app.model.entity.Tag;
import run.eternal.app.model.enums.ResultStatus;
import run.eternal.app.model.vo.TagVO;
import run.eternal.app.service.PostTagsService;
import run.eternal.app.service.TagsService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <pre>
 *      标签 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Slf4j
@Service
public class TagsServiceImpl extends ServiceImpl<TagsMapper, Tag> implements TagsService {

    @Autowired
    private PostTagsService postTagsService;

    @Transactional(rollbackFor = AbstractEternalException.class)
    @NonNull
    @Override
    public Tag createBy(@NonNull TagVO tagVO) {
        Assert.notNull(tagVO, "标签信息不能为空！");

        // 判断当前是否已有此分类
        LambdaQueryWrapper<Tag> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Tag::getName, tagVO.getName());
        Tag tagWrapper = baseMapper.selectOne(wrapper);
        if (null != tagWrapper) {
            log.error("此标签已存在: {}", tagWrapper.getName());
            throw new AlreadyExistsException(ResultStatus.TAG_EXIST);
        }

        Tag tag = Tag.builder().build();
        BeanUtils.copyProperties(tagVO, tag);

        // 新增
        int insert = baseMapper.insert(tag);
        if (insert > 0) {
            log.info("新增分类：{}", tag.getName());
        }
        return tag;
    }

    @NonNull
    @Override
    public List<TagDTO> convertTo(List<Tag> tags) {
        if (null == tags) {
            return Collections.emptyList();
        }

        return tags.stream()
                .map(tag -> {
                    int tagCount = postTagsService.countByTagId(tag.getId());
                    return TagDTO.builder()
                            .id(tag.getId())
                            .name(tag.getName())
                            .postCount(tagCount)
                            .build();
                }).collect(Collectors.toList());
    }

    @NonNull
    @Override
    public TagDetailDTO convertTo(@NonNull Tag tag) {
        Assert.notNull(tag, "标签信息不能为空！");

        TagDetailDTO tagDetailDTO = TagDetailDTO.builder().build();
        BeanUtils.copyProperties(tag, tagDetailDTO);
        return tagDetailDTO;
    }

    @Override
    public void updateBy(@NonNull TagVO tagVO) {
        Assert.notNull(tagVO, "标签信息不能为空！");

        Tag tag = Tag.builder().build();
        BeanUtils.copyProperties(tagVO, tag);
        baseMapper.updateById(tag);
    }

    @NonNull
    @Override
    public Tag getByName(@NonNull String tag) {
        Assert.notNull(tag, "tag name must not be null.");

        LambdaQueryWrapper<Tag> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(Tag::getName, tag);

        return baseMapper.selectOne(wrapper);
    }

}
