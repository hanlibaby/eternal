package run.eternal.app.service.impl;

import cn.hutool.crypto.digest.BCrypt;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import run.eternal.app.exception.AbstractEternalException;
import run.eternal.app.exception.AlreadyExistsException;
import run.eternal.app.exception.AuthenticationException;
import run.eternal.app.exception.NotFoundException;
import run.eternal.app.mapper.UserMapper;
import run.eternal.app.model.dto.user.UserDetailDTO;
import run.eternal.app.model.dto.user.UserLoginPassDTO;
import run.eternal.app.model.entity.User;
import run.eternal.app.model.enums.ResultStatus;
import run.eternal.app.model.vo.LoginVO;
import run.eternal.app.model.vo.UserVO;
import run.eternal.app.service.UserService;

import java.util.List;
import java.util.Optional;

/**
 * <pre>
 *      用户 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Override
    public User authenticate(@NonNull LoginVO loginVo) {
        Assert.notNull(loginVo, "登录信息不能为空！");

        // 构造查询条件
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(User::getUsername, loginVo.getUsername())
                .or()
                .eq(User::getEmail, loginVo.getUsername());

        User user = baseMapper.selectOne(queryWrapper);

        if (null == user) {
            log.error("未找到该用户: {}" + loginVo.getUsername());
            throw new AuthenticationException(ResultStatus.LOGIN_ERROR);
        }
        if (!this.passwordMatch(user, loginVo.getPassword())) {
            log.error("密码错误！" + loginVo.getUsername());
            throw new AuthenticationException(ResultStatus.LOGIN_PASSWORD_ERROR);
        } else {
            return user;
        }
    }

    @NonNull
    @Override
    public Optional<User> getCurrentUser() {
        // 获取所有用户
        List<User> users = baseMapper.selectList(null);

        if (CollectionUtils.isEmpty(users)) {
            // 返回空用户
            return Optional.empty();
        }

        // 返回第一个用户
        return Optional.of(users.get(0));
    }

    @NonNull
    @Override
    public Optional<User> getByUsername(@NonNull String username) {
        return Optional.empty();
    }

    @NonNull
    @Override
    public User updatePassword(@NonNull String oldPassword, @NonNull String newPassword) {
        Assert.notNull(oldPassword, "原密码不能为空！");
        Assert.notNull(newPassword, "新密码不能为空！");

        // 判断当前用户是否存在
        User user = this.getCurrentUser().orElse(null);
        if (null == user) {
            log.error("当前用户不存在，无法进行更新密码操作！");
            throw new NotFoundException(ResultStatus.USER_NOT_EXIST);
        }

        // 判断原密码是否匹配
        boolean match = this.passwordMatch(user, oldPassword);
        if (!match) {
            log.error("用户原密码匹配错误！");
            throw new AuthenticationException(ResultStatus.USER_PASSWORD_NO_MATCH);
        }

        this.setPassword(user, newPassword);
        baseMapper.updateById(user);
        return user;
    }

    @NonNull
    @Override
    @Transactional(rollbackFor = AbstractEternalException.class)
    public User createBy(@NonNull UserVO userVO) {
        Assert.notNull(userVO, "userVO must not be null.");
        User user = User.builder().build();
        BeanUtils.copyProperties(userVO, user);
        setPassword(user, userVO.getPassword());
        if (baseMapper.selectCount(null) > 0) {
            throw new AlreadyExistsException(ResultStatus.USER_EXIT_ERROR);
        }
        baseMapper.insert(user);
        return user;
    }

    @Override
    public boolean passwordMatch(@NonNull User user, String plainPassword) {
        Assert.notNull(user, "User must not be null");

        return !StringUtils.isBlank(plainPassword) && BCrypt.checkpw(plainPassword, user.getPassword());
    }

    @Override
    public void setPassword(@NonNull User user, @NonNull String plainPassword) {
        Assert.notNull(user, "User must not be null.");
        Assert.hasText(plainPassword, "Plain password must not be blank.");

        user.setPassword(BCrypt.hashpw(plainPassword, BCrypt.gensalt()));
    }

    @Override
    public boolean verifyUser(@NonNull String username, @NonNull String password) {
        return false;
    }

    @NonNull
    @Override
    public User update(@NonNull User user) {
        baseMapper.updateById(user);
        return user;
    }

    @NonNull
    @Override
    public UserLoginPassDTO convertTo(@NonNull User user) {
        UserLoginPassDTO userLoginPassDTO = UserLoginPassDTO.builder().build();
        // 设置头像
        userLoginPassDTO.setAvatar(user.getAvatar());
        // 设置username
        userLoginPassDTO.setUsername(user.getUsername());
        // 返回
        return userLoginPassDTO;
    }

    @NonNull
    @Override
    public UserDetailDTO convertToDetail(@NonNull User user) {
        Assert.notNull(user, "用户信息不能为空！");

        UserDetailDTO userDetailDTO = UserDetailDTO.builder().build();
        BeanUtils.copyProperties(user, userDetailDTO);

        return userDetailDTO;
    }

    @Override
    public boolean verifyAdmin(@NonNull String username, @NonNull String email) {
        // 构造查询条件
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(User::getUsername, username)
                .or()
                .eq(User::getEmail, email);
        User user = baseMapper.selectOne(queryWrapper);
        return null != user;
    }

    @Override
    public void updateBy(@NonNull UserVO userVO) {
        Assert.notNull(userVO, "用户信息不能为空！");

        User user = User.builder().build();
        BeanUtils.copyProperties(userVO, user);
        baseMapper.updateById(user);
    }

    @Override
    public User checkToken(@NonNull String id) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getId, id);

        return baseMapper.selectOne(wrapper);
    }
}
