package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import run.eternal.app.exception.AbstractEternalException;
import run.eternal.app.exception.AlreadyExistsException;
import run.eternal.app.mapper.LinksMapper;
import run.eternal.app.model.dto.link.LinkDetailDTO;
import run.eternal.app.model.dto.link.LinkMinimalDTO;
import run.eternal.app.model.entity.Link;
import run.eternal.app.model.enums.ResultStatus;
import run.eternal.app.model.vo.LinkVO;
import run.eternal.app.service.LinksService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <pre>
 *      友链 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Slf4j
@Service
public class LinksServiceImpl extends ServiceImpl<LinksMapper, Link> implements LinksService {

    @NonNull
    @Override
    public LinkDetailDTO convertTo(@NonNull Link link) {
        Assert.notNull(link, "友链信息不能为空！");

        LinkDetailDTO linkDetailDTO = LinkDetailDTO.builder().build();
        BeanUtils.copyProperties(link, linkDetailDTO);
        return linkDetailDTO;
    }

    @NonNull
    @Override
    public List<LinkMinimalDTO> convertTo(List<Link> links) {
        if (null == links) {
            return Collections.emptyList();
        }

        return links.stream()
                .map(link -> LinkMinimalDTO.builder().id(link.getId()).name(link.getName()).logo(link.getLogo()).url(link.getUrl()).sort(link.getSort()).build())
                .collect(Collectors.toList());
    }

    @Transactional(rollbackFor = AbstractEternalException.class)
    @Override
    public void createBy(@NonNull LinkVO linkVO) {
        Assert.notNull(linkVO, "友链信息不能为空！");

        // 判断当前是否已有此友链
        LambdaQueryWrapper<Link> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Link::getName, linkVO.getName())
                .eq(Link::getUrl, linkVO.getUrl());

        Link link = baseMapper.selectOne(wrapper);
        if (null != link) {
            log.error("此友链已存在: {}", link.getName());
            throw new AlreadyExistsException(ResultStatus.LINK_EXIST);
        }

        Link iLink = Link.builder().build();
        BeanUtils.copyProperties(linkVO, iLink);

        baseMapper.insert(iLink);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateBy(@NonNull LinkVO linkVO) {
        Assert.notNull(linkVO, "友链信息不能为空！");

        Link link = Link.builder().build();
        BeanUtils.copyProperties(linkVO, link);
        baseMapper.updateById(link);
    }
}
