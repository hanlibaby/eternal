package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import run.eternal.app.exception.AbstractEternalException;
import run.eternal.app.exception.AlreadyExistsException;
import run.eternal.app.mapper.CategoriesMapper;
import run.eternal.app.model.dto.category.CategoryMinimalDTO;
import run.eternal.app.model.dto.category.CategoryDetailDTO;
import run.eternal.app.model.entity.Category;
import run.eternal.app.model.enums.ResultStatus;
import run.eternal.app.model.vo.CategoryVO;
import run.eternal.app.service.CategoriesService;
import run.eternal.app.service.PostCategoriesService;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <pre>
 *      分类 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Slf4j
@Service
public class CategoriesServiceImpl extends ServiceImpl<CategoriesMapper, Category> implements CategoriesService {

    @Autowired
    private PostCategoriesService postCategoriesService;

    @Transactional(rollbackFor = AbstractEternalException.class)
    @NonNull
    @Override
    public Category createBy(@NonNull CategoryVO categoryVO) {
        Assert.notNull(categoryVO, "category to create must not be null");

        // 判断当前是否已有此分类
        LambdaQueryWrapper<Category> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Category::getName, categoryVO.getName());
        Category categoriesWrapper = baseMapper.selectOne(wrapper);
        if (null != categoriesWrapper) {
            log.error("此分类已存在: {}", categoriesWrapper.getName());
            throw new AlreadyExistsException(ResultStatus.CATEGORY_EXIST);
        }

        Category categories = Category.builder().build();
        BeanUtils.copyProperties(categoryVO, categories);

        // 新增
        int insert = baseMapper.insert(categories);
        if (insert > 0) {
            log.info("新增分类：{}", categories.getName());
        }
        return categories;
    }

    @NonNull
    @Override
    public List<CategoryMinimalDTO> convertTo(List<Category> categories) {
        if (null == categories) {
            return Collections.emptyList();
        }

        return categories.stream()
                .sorted((a, b) -> b.getGmtCreate().compareTo(a.getGmtCreate()))
                .map(category -> {
                    int postCount = postCategoriesService.countByCategoryId(category.getId());
                    return CategoryMinimalDTO.builder()
                            .id(category.getId())
                            .name(category.getName())
                            .alias(category.getAlias())
                            .postCount(postCount)
                            .build();
                })
                .collect(Collectors.toList());
    }

    @NonNull
    @Override
    public CategoryDetailDTO convertTo(@NonNull Category category) {
        Assert.notNull(category, "分类信息不能为空！");

        CategoryDetailDTO categoryDetailDTO = CategoryDetailDTO.builder().build();
        BeanUtils.copyProperties(category, categoryDetailDTO);

        return categoryDetailDTO;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateBy(@NonNull CategoryVO categoryVO) {
        Assert.notNull(categoryVO, "分类信息不能为空！");

        Category category = Category.builder().build();
        BeanUtils.copyProperties(categoryVO, category);

        baseMapper.updateById(category);
    }

    @NonNull
    @Override
    public Category getByName(@NonNull String name) {
        Assert.notNull(name, "category name must not be null.");

        LambdaQueryWrapper<Category> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(Category::getName, name);

        return baseMapper.selectOne(wrapper);
    }
}
