package run.eternal.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import run.eternal.app.model.dto.StatisticDTO;
import run.eternal.app.model.enums.CommentStatus;
import run.eternal.app.model.enums.PostStatus;
import run.eternal.app.model.properties.PrimaryProperties;
import run.eternal.app.service.*;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author : lwj
 * @since : 2020-08-12
 */
@Service
public class StatisticServiceImpl implements StatisticService {

    @Autowired
    private PostsService postsService;

    @Autowired
    private CommentsService commentsService;

    @Autowired
    private SheetsService sheetsService;

    @Autowired
    private JournalsService journalsService;

    @Autowired
    private OptionsService optionsService;

    @Autowired
    private LinksService linksService;

    @Autowired
    private CategoriesService categoriesService;

    @Autowired
    private TagsService tagsService;

    @Autowired
    private AttachmentsService attachmentsService;

    @NonNull
    @Override
    public StatisticDTO getStatistic() {
        // 获取博客地址
        String blogUrl = optionsService.getBlogBaseUrl();

        // 统计文章数量（已发布）
        int postCount = postsService.countByStatus(PostStatus.PUBLISHED);

        // 统计评论数量（已审核）
        int commentCount = commentsService.countByStatus(CommentStatus.PASS);

        // 统计总访问人数
        Long postVisit = postsService.countVisit();
        Long sheetVisit = sheetsService.countVisit();

        // 统计点赞人数
        Long postLike = postsService.countLike();
        Long sheetLike = sheetsService.countLike();

        // 获得博客建立时间
        long birth = optionsService.getByPropertyOrDefault(PrimaryProperties.BIRTHDAY, Long.class, 0L);
        String birthday = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(birth));
        // 建立天数
        long days = (System.currentTimeMillis() - birth) / (1000 * 24 * 3600);

        // 统计友链数
        int linkCount = linksService.count();

        // 统计随记数
        int journalCount = journalsService.count();

        // 统计分类数
        int categoryCount = categoriesService.count();

        // 统计标签数
        int tagCount = tagsService.count();

        // 统计附件数
        int attachmentCount = attachmentsService.count();

        return StatisticDTO.builder()
                .blogUrl(blogUrl)
                .postCount(postCount)
                .commentCount(commentCount)
                .visitCount(postVisit + sheetVisit)
                .likeCount(postLike + sheetLike)
                .birthday(birthday)
                .establishDays(days)
                .linkCount(linkCount)
                .journalCount(journalCount)
                .categoryCount(categoryCount)
                .tagCount(tagCount)
                .attachmentCount(attachmentCount)
                .build();
    }
}
