package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import run.eternal.app.handler.file.support.FileHandlers;
import run.eternal.app.mapper.AttachmentsMapper;
import run.eternal.app.model.dto.attachment.AttachmentMinimalDTO;
import run.eternal.app.model.entity.Attachment;
import run.eternal.app.model.enums.StorageType;
import run.eternal.app.model.enums.ValueEnum;
import run.eternal.app.model.properties.AttachmentProperties;
import run.eternal.app.model.properties.BlogProperties;
import run.eternal.app.model.properties.PrimaryProperties;
import run.eternal.app.model.support.UploadResult;
import run.eternal.app.model.vo.query.AttachmentQueryVO;
import run.eternal.app.service.AttachmentsService;
import run.eternal.app.service.OptionsService;
import run.eternal.app.utils.EternalUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <pre>
 *      附件 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Slf4j
@Service
public class AttachmentsServiceImpl extends ServiceImpl<AttachmentsMapper, Attachment> implements AttachmentsService {

    @Autowired
    private OptionsService optionsService;

    @Autowired
    private FileHandlers fileHandlers;

    @NonNull
    @Override
    public List<String> listMedia() {
        return baseMapper.listMedia();
    }

    @Override
    public List<String> listStorage() {
        return baseMapper.listStorage();
    }

    @NonNull
    @Override
    public IPage<AttachmentMinimalDTO> pageBy(int page, int size) {
        IPage<Attachment> attachmentPage = new Page<>(page, size);
        LambdaQueryWrapper<Attachment> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(Attachment::getGmtCreate);
        return getPage(attachmentPage, wrapper);
    }

    @NonNull
    @Override
    public IPage<AttachmentMinimalDTO> pageByQuery(int page, int size, AttachmentQueryVO attachmentQueryVO) {
        if ("".equals(attachmentQueryVO.getKeyword()) && "".equals(attachmentQueryVO.getStorageType()) && "".equals(attachmentQueryVO.getMediaType())) {
            return pageBy(page, size);
        }

        LambdaQueryWrapper<Attachment> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(Attachment::getName, attachmentQueryVO.getKeyword())
                .likeRight(Attachment::getStorageType, attachmentQueryVO.getStorageType())
                .likeRight(Attachment::getMediaType, attachmentQueryVO.getMediaType());

        IPage<Attachment> attachmentPage = new Page<>(page, size);
        return getPage(attachmentPage, wrapper);
    }

    private IPage<AttachmentMinimalDTO> getPage(IPage<Attachment> attachmentPage, LambdaQueryWrapper<Attachment> wrapper) {
        baseMapper.selectPage(attachmentPage, wrapper);
        baseMapper.selectPage(attachmentPage, wrapper);

        List<Attachment> attachments = attachmentPage.getRecords();

        List<AttachmentMinimalDTO> attachmentMinimals = attachments.stream()
                .map(this::convertToMinimal)
                .collect(Collectors.toList());

        IPage<AttachmentMinimalDTO> pageable = new Page<>();
        BeanUtils.copyProperties(attachmentPage, pageable);
        pageable.setRecords(attachmentMinimals);
        return pageable;
    }

    @NonNull
    @Override
    public Attachment upload(@NonNull MultipartFile file) {
        Assert.notNull(file, "附件不能为空！");

        StorageType storageType = getStorageType();

        log.debug("开始上传附件... 附件存储位置: [{}], 文件: [{}]", storageType, file.getOriginalFilename());

        // 上传附件
        UploadResult uploadResult = fileHandlers.upload(file, storageType);

        log.debug("附件存储位置: [{}]", storageType);
        log.debug("上传结果: [{}]", uploadResult);

        Attachment attachment = Attachment.builder()
                .name(uploadResult.getFilename())
                // 转换分隔符
                .path(EternalUtils.changeFileSeparatorToUrlSeparator(uploadResult.getFilePath()))
                .fileKey(uploadResult.getKey())
                .thumbPath(uploadResult.getThumbPath())
                .mediaType(uploadResult.getMediaType().toString())
                .suffix(uploadResult.getSuffix())
                .width(uploadResult.getWidth())
                .height(uploadResult.getHeight())
                .size(uploadResult.getSize())
                .storageType(storageType.getValue())
                .build();

        log.debug("创建附件信息: [{}]", attachment);

        // 新增并返回
        baseMapper.insert(attachment);
        return attachment;
    }

    @Override
    public void removePermanently(@NonNull String attachmentId) {
        Assert.notNull(attachmentId, "附件id不能为空！");

        // 从数据库中删除
        Attachment attachment = baseMapper.selectById(attachmentId);
        baseMapper.deleteById(attachmentId);

        // 删除文件
        fileHandlers.delete(attachment);

        log.debug("已删除附件: [{}]", attachment);
    }

    @Override
    public void removePermanently(List<String> ids) {
        if (!CollectionUtils.isEmpty(ids)) {
            ids.forEach(this::removePermanently);
        }
    }

    @Override
    public IPage<AttachmentMinimalDTO> pageByQueryImage(int page, int size, AttachmentQueryVO attachmentQueryVO) {
        IPage<AttachmentMinimalDTO> pageByQuery = this.pageByQuery(page, size, attachmentQueryVO);

        List<AttachmentMinimalDTO> records = pageByQuery.getRecords();
        List<AttachmentMinimalDTO> images = records.stream()
                .filter(attachmentMinimalDTO -> {
                    return attachmentMinimalDTO.getMediaType().contains("image");
                }).collect(Collectors.toList());

        pageByQuery.setRecords(images);
        return pageByQuery;
    }

    /**
     * 从博客设置中获取附件存储类型
     *
     * @return 附件存储类型
     */
    @NonNull
    private StorageType getStorageType() {
        return ValueEnum.valueToEnum(StorageType.class,
                Objects.requireNonNull(optionsService.getByPropertyOrDefault(AttachmentProperties.STORAGE_TYPE, String.class, StorageType.LOCAL.getValue())));
    }


}
