package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import run.eternal.app.handler.theme.support.Group;
import run.eternal.app.handler.theme.support.Item;
import run.eternal.app.mapper.ThemeSettingsMapper;
import run.eternal.app.model.entity.ThemeSetting;
import run.eternal.app.service.ThemeService;
import run.eternal.app.service.ThemeSettingsService;
import run.eternal.app.utils.ServiceUtils;

import java.util.*;

/**
 * <pre>
 *      主题设置 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Slf4j
@Service
public class ThemeSettingsServiceImpl extends ServiceImpl<ThemeSettingsMapper, ThemeSetting> implements ThemeSettingsService {

    @Autowired
    private ThemeService themeService;

    @Override
    public ThemeSetting save(@NonNull String key, String value, @NonNull String themeId) {
        Assert.notNull(key, "Setting key must not be null");
        assertThemeIdHasText(themeId);

        log.debug("Starting saving theme setting key: [{}], value: [{}]", key, value);

        // Find setting by id and key
        LambdaQueryWrapper<ThemeSetting> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(ThemeSetting::getThemeId, themeId).eq(ThemeSetting::getSettingKey, key);
        ThemeSetting themeSetting = baseMapper.selectOne(wrapper);

        if (StringUtils.isBlank(value)) {
            // Delete it
            baseMapper.deleteById(themeSetting);
            log.debug("Removed theme setting: [{}]", themeSetting);
        }

        // Get config item map
        Map<String, Item> itemMap = getConfigItemMap(themeId);

        // Get item info
        Item item = itemMap.get(key);

        // Update or create
        if (null == themeSetting) {
            ThemeSetting setting = new ThemeSetting();
            setting.setSettingKey(key);
            setting.setSettingValue(value);
            setting.setThemeId(themeId);
            baseMapper.insert(setting);
            log.debug("Creating theme setting: [{}]", setting);
        } else {
            log.debug("Updating theme setting: [{}]", themeSetting);
            themeSetting.setSettingValue(value);
            baseMapper.updateById(themeSetting);
            log.debug("Updated theme setting: [{}]", themeSetting);
        }

        return themeSetting;
    }

    @Override
    public void save(@NonNull Map<String, Object> settings, @NonNull String themeId) {
        assertThemeIdHasText(themeId);

        if (CollectionUtils.isEmpty(settings)) {
            return;
        }

        // Save the settings
        settings.forEach((key, value) -> save(key, value.toString(), themeId));
    }

    @Override
    public List<ThemeSetting> listBy(String themeId) {
        assertThemeIdHasText(themeId);

        LambdaQueryWrapper<ThemeSetting> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(ThemeSetting::getThemeId, themeId);

        return baseMapper.selectList(wrapper);
    }

    @NonNull
    @Override
    public Map<String, Object> listAsMapBy(@NonNull String themeId) {
        // Convert to item map(key: item name, value: item)
        Map<String, Item> itemMap = getConfigItemMap(themeId);

        // Get theme setting
        List<ThemeSetting> themeSettings = listBy(themeId);

        Map<String, Object> result = new HashMap<>(16);

        // Build settings from user-defined
        themeSettings.forEach(themeSetting -> {
            String key = themeSetting.getSettingKey();

            Item item = itemMap.get(key);

            if (item == null) {
                return;
            }

            Object convertedValue = item.getDataType().convertTo(themeSetting.getSettingValue());
            log.debug("Converted user-defined data from [{}] to [{}], type: [{}]", themeSetting.getSettingValue(), convertedValue, item.getDataType());

            result.put(key, convertedValue);
        });

        // Build settings from pre-defined
        itemMap.forEach((name, item) -> {
            log.debug("Name: [{}], item: [{}]", name, item);

            if (item.getDefaultValue() == null || result.containsKey(name)) {
                return;
            }

            // Set default value
            Object convertedDefaultValue = item.getDataType().convertTo(item.getDefaultValue());
            log.debug("Converted pre-defined data from [{}] to [{}], type: [{}]", item.getDefaultValue(), convertedDefaultValue, item.getDataType());

            result.put(name, convertedDefaultValue);
        });

        return result;
    }

    @Override
    public List<ThemeSetting> replaceUrl(@NonNull String oldUrl, @NonNull String newUrl) {
        List<ThemeSetting> themeSettings = baseMapper.selectList(null);
        List<ThemeSetting> replaced = new ArrayList<>();
        themeSettings.forEach(themeSetting -> {
            if (StringUtils.isNotEmpty(themeSetting.getSettingValue())) {
                themeSetting.setSettingValue(themeSetting.getSettingValue().replaceAll(oldUrl, newUrl));
            }
            replaced.add(themeSetting);
        });
        super.updateBatchById(replaced);
        return replaced;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteInactivated() {
        String themeId = themeService.getActivatedThemeId();
        LambdaQueryWrapper<ThemeSetting> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(ThemeSetting::getThemeId, themeId);

        baseMapper.delete(wrapper);
    }

    /**
     * Gets config item map. (key: item name, value: item)
     *
     * @param themeId theme id must not be blank
     * @return config item map
     */
    private Map<String, Item> getConfigItemMap(@NonNull String themeId) {
        // Get theme configuration
        List<Group> groups = themeService.fetchConfig(themeId);

        // Mix all items
        Set<Item> items = new LinkedHashSet<>();
        groups.forEach(group -> items.addAll(group.getItems()));

        // Convert to item map(key: item name, value: item)
        return ServiceUtils.convertToMap(items, Item::getName);
    }

    /**
     * Asserts theme id has text.
     *
     * @param themeId theme id to be checked
     */
    private void assertThemeIdHasText(String themeId) {
        Assert.hasText(themeId, "Theme id must not be null");
    }

}
