package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import run.eternal.app.mapper.PostTagsMapper;
import run.eternal.app.model.entity.PostTags;
import run.eternal.app.model.entity.Tag;
import run.eternal.app.service.PostTagsService;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <pre>
 *      文章-标签 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Slf4j
@Service
public class PostTagsServiceImpl extends ServiceImpl<PostTagsMapper, PostTags> implements PostTagsService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void removeByPostId(@NonNull String id) {
        Assert.notNull(id, "文章id不能为空！");

        LambdaQueryWrapper<PostTags> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(PostTags::getPostId, id);
        int delete = baseMapper.delete(wrapper);
        log.info("根据文章id：{}，共删除：{} 条数据", id, delete);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void createBy(@NonNull String id, @NonNull List<Tag> tags) {
        Assert.notNull(id, "文章id不能为空！");
        Assert.notNull(tags, "标签集合不能为空！");

        tags.forEach(tag -> {
            // 创建 PostTags 对象
            PostTags postTags = PostTags.builder()
                    .postId(id)
                    .tagId(tag.getId())
                    .build();
            baseMapper.insert(postTags);
        });
    }

    @NonNull
    @Override
    public List<PostTags> listTagsByPostId(@NonNull String postId) {
        Assert.notNull(postId, "文章id不能为空！");

        LambdaQueryWrapper<PostTags> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(PostTags::getPostId, postId);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public int countByTagId(@NonNull String tagId) {
        Assert.notNull(tagId, "标签id不能为空！");

        LambdaQueryWrapper<PostTags> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(PostTags::getTagId, tagId);
        return baseMapper.selectCount(wrapper);
    }

    @Override
    public List<String> listPostIdsByTagId(@NonNull String id) {
        Assert.notNull(id, "tag id must not be null.");

        LambdaQueryWrapper<PostTags> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(PostTags::getTagId, id);

        List<PostTags> postTags = baseMapper.selectList(wrapper);

        return postTags.stream()
                .map(PostTags::getPostId)
                .collect(Collectors.toList());
    }
}
