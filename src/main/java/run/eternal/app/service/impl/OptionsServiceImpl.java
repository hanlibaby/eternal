package run.eternal.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import run.eternal.app.exception.MissingPropertyException;
import run.eternal.app.mapper.OptionsMapper;
import run.eternal.app.model.entity.Option;
import run.eternal.app.model.properties.*;
import run.eternal.app.service.OptionsService;
import run.eternal.app.utils.DateUtils;
import run.eternal.app.utils.ServiceUtils;

import java.util.*;

/**
 * <pre>
 *      博客设置 服务实现类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Service
public class OptionsServiceImpl extends ServiceImpl<OptionsMapper, Option> implements OptionsService {

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * blog options map (only read)
     */
    private final Map<String, PropertyEnum> propertyEnumMap = Collections.unmodifiableMap(PropertyEnum.getValuePropertyEnumMap());

    /**
     * Get blog option value by key
     *
     * @param key option key
     * @return an optional
     */
    private Optional<Object> getByKey(String key) {
        Assert.hasText(key, "Option key must not be blank.");

        // build query condition
        LambdaQueryWrapper<Option> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Option::getOptionKey, key);
        Option options = baseMapper.selectOne(queryWrapper);

        String optionValue = null == options ? "" : options.getOptionValue();
        // return an optional
        return Optional.ofNullable(optionValue);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveByMap(Map<String, Object> optionsMap) {
        if (CollectionUtils.isEmpty(optionsMap)) {
            return;
        }

        // converts to map
        Map<String, Option> optionKeyMap = ServiceUtils.convertToMap(baseMapper.selectList(null), Option::getOptionKey);

        // options to create
        List<Option> optionsToCreate = new LinkedList<>();

        // options to update
        List<Option> optionsToUpdate = new LinkedList<>();

        optionsMap.forEach((key, value) -> {
            // get old option from
            Option oldOption = optionKeyMap.get(key);
            if (null == oldOption || !StringUtils.equals(oldOption.getOptionValue(), value.toString())) {
                Option option = new Option();
                option.setOptionKey(key);
                option.setOptionValue(value.toString());

                if (null == oldOption) {
                    // add to optionsToCreate
                    optionsToCreate.add(option);
                } else if (!StringUtils.equals(oldOption.getOptionValue(), value.toString())) {
                    oldOption.setOptionValue(value.toString());
                    // add to optionsToUpdate
                    optionsToUpdate.add(oldOption);
                }
            }
        });

        // to update
        super.updateBatchById(optionsToUpdate);

        // to create
        super.saveBatch(optionsToCreate);
    }

    @NonNull
    @Override
    public Map<String, Object> listOptions() {
        // TODO change get blog options from cache
        List<Option> options = baseMapper.selectList(null);

        // get all blog option keys
        List<String> keys = ServiceUtils.fetchProperty(options, Option::getOptionKey);

        // change options to map
        Map<String, Object> userDefinedOptionMap = ServiceUtils.convertToMap(options, Option::getOptionKey, option -> {
            String key = option.getOptionKey();

            // get blog property enum from already defined properties enum map
            PropertyEnum propertyEnum = propertyEnumMap.get(key);

            // return directly value if property enum is null
            if (null == propertyEnum) {
                return option.getOptionValue();
            }

            // return already transformed blog option value
            return PropertyEnum.convertTo(option.getOptionValue(), propertyEnum);
        });

        // add blog default option to map
        Map<String, Object> result = new HashMap<>(userDefinedOptionMap);
        propertyEnumMap.keySet()
                .stream()
                .filter(key -> !keys.contains(key))
                .forEach(key -> {
                    PropertyEnum propertyEnum = propertyEnumMap.get(key);

                    if (StringUtils.isBlank(propertyEnum.defaultValue())) {
                        return;
                    }

                    result.put(key, PropertyEnum.convertTo(propertyEnum.defaultValue(), propertyEnum));
                });

        // TODO add to cache

        return result;
    }

    @NonNull
    @Override
    public Optional<Object> getByProperty(PropertyEnum property) {
        Assert.notNull(property, "blog property must not be null!");

        return getByKey(property.getValue());
    }

    @NonNull
    @Override
    public <T> Optional<T> getByProperty(PropertyEnum property, Class<T> propertyType) {
        return getByProperty(property).map(propertyValue -> PropertyEnum.convertTo(propertyValue.toString(), propertyType));
    }

    @NonNull
    @Override
    public <T> T getByPropertyOrDefault(PropertyEnum property, Class<T> propertyType, T defaultValue) {
        return getByProperty(property, propertyType).orElse(defaultValue);
    }

    @Override
    public <T> T getByPropertyOrDefault(PropertyEnum property, Class<T> propertyType) {
        return getByProperty(property, propertyType).orElse(property.defaultValue(propertyType));
    }

    @NonNull
    @Override
    public Object getByKeyOfNonNull(@NonNull String key) {
        return getByKey(key).orElseThrow(() -> new MissingPropertyException("你没有： " + key + " 此项设置"));
    }

    @NonNull
    @Override
    public Object getByPropertyOfNonNull(@NonNull PropertyEnum property) {
        Assert.notNull(property, "blog property must not be null!");

        return getByKeyOfNonNull(property.getValue());
    }

    @NonNull
    @Override
    public String getBlogBaseUrl() {
        // get server post
        String serverPort = applicationContext.getEnvironment().getProperty("server.port", "9527");

        String blogUrl = getByProperty(BlogProperties.BLOG_URL).orElse("").toString();

        if (StringUtils.isNotBlank(blogUrl)) {
            blogUrl = StringUtils.removeEnd(blogUrl, "/");
        } else {
            blogUrl = String.format("http://%s:%s", "127.0.0.1", serverPort);
        }

        return blogUrl;
    }

    @Override
    public void saveProperties(Map<? extends PropertyEnum, String> properties) {
        Assert.notEmpty(properties, "properties map must not be null!");

        properties.forEach((property, value) -> {
            Option options = new Option();
            options.setOptionKey(property.getValue());
            options.setOptionValue(value);
            baseMapper.insert(options);
        });
    }

    private void save(@NonNull String key, @Nullable String value) {
        Assert.hasText(key, "Option key must not be blank");

        saveByMap(Collections.singletonMap(key, value));
    }

    @Override
    public void saveProperty(@NonNull PropertyEnum property, String value) {
        Assert.notNull(property, "Property must not be null");

        save(property.getValue(), value);
    }

    @NonNull
    @Override
    public String getBlogTitle() {
        return getByProperty(BlogProperties.BLOG_TITLE).orElse("").toString();
    }

    @Override
    public String getSeoKeywords() {
        return getByProperty(SeoProperties.KEYWORDS).orElse("").toString();
    }

    @Override
    public String getSeoDescription() {
        return getByProperty(SeoProperties.DESCRIPTION).orElse("").toString();
    }

    @Override
    public long getBirthday() {
        return getByProperty(PrimaryProperties.BIRTHDAY, Long.class).orElseGet(() -> {
            long currentTime = DateUtils.now().getTime();
            saveProperty(PrimaryProperties.BIRTHDAY, String.valueOf(currentTime));
            return currentTime;
        });
    }

    @Override
    public int getIndexPostPageSize() {
        return getByPropertyOrDefault(PostProperties.INDEX_PAGE_SIZE, Integer.class, 10);
    }

}
