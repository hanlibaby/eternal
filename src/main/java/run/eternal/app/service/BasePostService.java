package run.eternal.app.service;

/**
 * @author : lwj
 * @since : 2020-08-23
 */
public interface BasePostService {

    /**
     * Increase post visit
     *
     * @param postId post id
     */
    void increaseVisit(String postId);

    /**
     * Publish post visit event
     *
     * @param postId post id
     */
    void publishVisitEvent(String postId);

    /**
     * Get post count
     * @return post count
     */
    int statisticsCount();

}
