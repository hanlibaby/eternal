package run.eternal.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import run.eternal.app.model.dto.user.UserDetailDTO;
import run.eternal.app.model.dto.user.UserLoginPassDTO;
import run.eternal.app.model.entity.User;
import run.eternal.app.model.vo.LoginVO;
import run.eternal.app.model.vo.UserVO;

import java.util.Optional;

/**
 * <pre>
 *      用户 服务类
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
public interface UserService extends IService<User> {

    /**
     * 登录认证
     *
     * @param loginVo 用户名或邮箱/密码
     * @return User对象
     */
    User authenticate(LoginVO loginVo);

    /**
     * 获取当前用户
     *
     * @return an optional user
     */
    @NonNull
    Optional<User> getCurrentUser();

    /**
     * 通过用户名获取用户
     *
     * @param username 用户名不能为空
     * @return an optional user
     */
    @NonNull
    Optional<User> getByUsername(@NonNull String username);

    /**
     * 更新用户密码
     *
     * @param oldPassword 旧密码不能为空
     * @param newPassword 新密码不能为空
     * @param userId      用户ID不能为空
     * @return updated user detail
     */
    @NonNull
    User updatePassword(@NonNull String oldPassword, @NonNull String newPassword);

    /**
     * 创建一个用户
     *
     * @param userVO 用户参数不能为null
     * @return created user
     */
    @NonNull
    User createBy(@NonNull UserVO userVO);

    /**
     * 验证密码是否匹配
     *
     * @param user          User对象
     * @param plainPassword 密码
     * @return <code>true</code> 匹配
     */
    boolean passwordMatch(@NonNull User user, @Nullable String plainPassword);

    /**
     * 设置用户密码
     *
     * @param user          用户不能为空
     * @param plainPassword 密码不能为空
     */
    void setPassword(@NonNull User user, @NonNull String plainPassword);

    /**
     * 验证用户的电子邮件和用户名
     *
     * @param username 用户名不能为空
     * @param password 密码不能为空
     * @return boolean
     */
    boolean verifyUser(@NonNull String username, @NonNull String password);

    /**
     * 更新用户
     *
     * @param user 用户
     * @return <code>true</code> 更新成功
     */
    @NonNull
    User update(@NonNull User user);

    /**
     * 提取LoginPassDTO
     *
     * @param user 用户信息
     * @return LoginPass
     */
    @NonNull
    UserLoginPassDTO convertTo(@NonNull User user);

    /**
     * 转为UserDetailDTO
     *
     * @param user user信息
     * @return UserDetailDTO
     */
    @NonNull
    UserDetailDTO convertToDetail(@NonNull User user);

    /**
     * 验证是否是管理员评论
     *
     * @param username 用户名
     * @param email    邮箱
     * @return boolean
     */
    boolean verifyAdmin(@NonNull String username, @NonNull String email);

    /**
     * 更新用户信息
     *
     * @param userVO 用户信息
     */
    void updateBy(@NonNull UserVO userVO);

    /**
     * 判断token
     *
     * @param id id
     * @return 是否存在
     */
    User checkToken(@NonNull String id);
}
