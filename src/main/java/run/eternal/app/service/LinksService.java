package run.eternal.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.lang.NonNull;
import run.eternal.app.model.dto.link.LinkDetailDTO;
import run.eternal.app.model.dto.link.LinkMinimalDTO;
import run.eternal.app.model.entity.Link;
import run.eternal.app.model.vo.LinkVO;

import java.util.List;

/**
 * Links service
 *
 * @author lwjppz
 * @since 2020-08-08
 */
public interface LinksService extends IService<Link> {

    /**
     * Converts to LinkMDetailDTO
     *
     * @param link link info
     * @return LinkDetailDTO
     */
    @NonNull
    LinkDetailDTO convertTo(Link link);

    /**
     * Converts to LinkMinimalDTO
     *
     * @param links link list
     * @return LinkMinimalDTO list
     */
    @NonNull
    List<LinkMinimalDTO> convertTo(List<Link> links);

    /**
     * Add link
     *
     * @param linkVO link info
     */
    void createBy(@NonNull LinkVO linkVO);

    /**
     * Update link info
     *
     * @param linkVO link info
     */
    void updateBy(@NonNull LinkVO linkVO);
}
