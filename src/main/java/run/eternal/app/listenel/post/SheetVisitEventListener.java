package run.eternal.app.listenel.post;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import run.eternal.app.event.post.SheetVisitEvent;
import run.eternal.app.service.SheetsService;

/**
 * @author : lwj
 * @since : 2020-08-22
 */
@Component
public class SheetVisitEventListener extends AbstractVisitEventListener {

    protected SheetVisitEventListener(SheetsService sheetsService) {
        super(sheetsService);
    }

    @Async
    @EventListener
    public void onSheetVisitEvent(SheetVisitEvent event) throws InterruptedException {
        handleVisitEvent(event);
    }

}
