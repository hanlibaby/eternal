package run.eternal.app.listenel.post;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import run.eternal.app.event.post.PostVisitEvent;
import run.eternal.app.service.PostsService;


/**
 * @author : lwj
 * @since : 2020-08-22
 */
@Component
public class PostVisitEventListener extends AbstractVisitEventListener {

    public PostVisitEventListener(PostsService postsService) {
        super(postsService);
    }

    @Async
    @EventListener
    public void onPostVisitEvent(PostVisitEvent event) throws InterruptedException {
        handleVisitEvent(event);
    }

}
