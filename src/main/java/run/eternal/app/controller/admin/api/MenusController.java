package run.eternal.app.controller.admin.api;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.model.dto.menu.MenuDTO;
import run.eternal.app.model.dto.menu.MenuDetailDTO;
import run.eternal.app.model.dto.menu.MenuMinimalDTO;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.vo.MenuVO;
import run.eternal.app.service.MenusService;

import java.util.List;

/**
 * Menus controller
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@RestController
@RequestMapping("/api/admin/menus")
@Api(value = "MenusController", tags = "菜单管理")
public class MenusController {

    @Autowired
    private MenusService menusService;

    @Authorize
    @ApiOperation("菜单列表")
    @GetMapping("/list")
    public CommonResult list() {
        final List<MenuDTO> menus = menusService.convertTo(menusService.list());
        return CommonResult.ok().data("menus", menus);
    }

    @Authorize
    @ApiOperation("获取所有没有子菜单的菜单")
    @GetMapping("/listNoChild")
    public CommonResult listNoChild() {
        final List<MenuMinimalDTO> menus = menusService.selectNoChild(menusService.list());
        return CommonResult.ok().data("menusNoChild", menus);
    }

    @Authorize
    @ApiOperation("根据菜单id获取菜单信息")
    @GetMapping("/info/{menuId}")
    public CommonResult info(@ApiParam(name = "menuId", value = "菜单id") @PathVariable(name = "menuId") String menuId) {
        final MenuDetailDTO menuDetailDTO = menusService.convertToDetail(menusService.getById(menuId));
        return CommonResult.ok().data("menu", menuDetailDTO);
    }

    @Authorize
    @Log(logType = LogType.MENU_PUBLISH, saveRequestParams = true)
    @ApiOperation("新增菜单")
    @ApiOperationSupport(ignoreParameters = {"id"})
    @PostMapping("/create")
    public CommonResult create(@ApiParam(name = "menuVO", value = "菜单信息") @RequestBody MenuVO menuVO) {
        menusService.createBy(menuVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.MENU_EDIT, saveRequestParams = true)
    @ApiOperation("修改菜单")
    @PutMapping("/update")
    public CommonResult update(@ApiParam(name = "menuVO", value = "菜单信息") @RequestBody MenuVO menuVO) {
        menusService.updateBy(menuVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.MENU_DELETE, saveRequestParams = true)
    @ApiOperation("删除菜单")
    @DeleteMapping("/delete")
    public CommonResult delete(@ApiParam(name = "menuId", value = "菜单id") @RequestParam(name = "menuId") String menuId) {
        menusService.removeById(menuId);
        return CommonResult.ok();
    }
}

