package run.eternal.app.controller.admin.api;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.model.dto.user.UserLoginPassDTO;
import run.eternal.app.model.entity.User;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.properties.PrimaryProperties;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.vo.LoginVO;
import run.eternal.app.service.impl.OptionsServiceImpl;
import run.eternal.app.service.impl.UserServiceImpl;
import run.eternal.app.utils.TokenUtils;

/**
 * Admin Controller
 *
 * @author : lwj
 * @since : 2020-08-08
 */
@RestController
@RequestMapping("/api/admin")
@Api(value = "AdminController", tags = "管理员操作")
public class AdminController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private OptionsServiceImpl optionsService;

    @Log(logType = LogType.LOGGED_IN, saveRequestParams = true)
    @ApiOperation("登录")
    @ApiOperationSupport(ignoreParameters = {"id"})
    @PostMapping("/login")
    public CommonResult login(@ApiParam(name = "登录信息", value = "loginVo") @RequestBody LoginVO loginVo) {
        User user = userService.authenticate(loginVo);
        if (null != user) {
            return CommonResult.ok().data("token", TokenUtils.getToken(user.getId()));
        }
        return CommonResult.error();
    }

    @Authorize
    @ApiOperation("获取用户信息")
    @GetMapping("/info")
    public CommonResult info() {
        final UserLoginPassDTO userLoginPassDTO = userService.convertTo(userService.getCurrentUser().get());
        return CommonResult.ok().data("user", userLoginPassDTO);
    }

    @Authorize
    @Log(logType = LogType.LOGGED_OUT, saveRequestParams = true)
    @ApiOperation("退出登录")
    @GetMapping("/logout")
    public CommonResult logout() {
        return CommonResult.ok();
    }

    @ApiOperation("检查博客是否已安装")
    @GetMapping("/isInstall")
    public CommonResult isInstall() {
        final boolean isInstall = optionsService.getByPropertyOrDefault(PrimaryProperties.IS_INSTALLED, Boolean.class, false);
        return CommonResult.ok().data("isInstall", isInstall);
    }


}
