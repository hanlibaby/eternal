package run.eternal.app.controller.admin.api;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.model.dto.tag.TagDTO;
import run.eternal.app.model.dto.tag.TagDetailDTO;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.vo.TagVO;
import run.eternal.app.service.TagsService;

import java.util.List;

/**
 * Tags controller
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@RestController
@RequestMapping("/api/admin/tags")
@Api(value = "TagsController", tags = "标签管理")
public class TagsController {

    @Autowired
    private TagsService tagsService;

    @Authorize
    @ApiOperation("标签列表")
    @GetMapping("/list")
    public CommonResult list() {
        final List<TagDTO> tags = tagsService.convertTo(tagsService.list());
        return CommonResult.ok().data("tags", tags);
    }

    @Authorize
    @ApiOperation("根据标签id获取标签信息")
    @GetMapping("/info/{tagId}")
    public CommonResult info(@ApiParam(name = "tagId", value = "标签id") @PathVariable("tagId") String tagId) {
        final TagDetailDTO tagDetailDTO = tagsService.convertTo(tagsService.getById(tagId));
        return CommonResult.ok().data("tag", tagDetailDTO);
    }

    @Authorize
    @Log(logType = LogType.TAG_PUBLISHED, saveRequestParams = true)
    @ApiOperation("新增标签")
    @ApiOperationSupport(ignoreParameters = {"id"})
    @PostMapping("/create")
    public CommonResult create(@ApiParam(name = "tagVo", value = "标签信息") @RequestBody TagVO tagVO) {
        tagsService.createBy(tagVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.TAG_EDIT, saveRequestParams = true)
    @ApiOperation("编辑标签")
    @PutMapping("/update")
    public CommonResult update(@ApiParam(name = "tagVo", value = "标签信息") @RequestBody TagVO tagVO) {
        tagsService.updateBy(tagVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.TAG_DELETE, saveRequestParams = true)
    @ApiOperation("删除标签")
    @DeleteMapping("/delete")
    public CommonResult delete(@ApiParam(name = "tagId", value = "标签id") @RequestParam(name = "tagId") String tagId) {
        tagsService.removeById(tagId);
        return CommonResult.ok();
    }

}

