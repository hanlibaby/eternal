package run.eternal.app.controller.admin.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.model.dto.sheet.SheetDTO;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.vo.SheetVO;
import run.eternal.app.service.SheetsService;

/**
 * Sheets controller
 *
 * @author : lwj
 * @since : 2020-08-15
 */
@RestController
@RequestMapping("/api/admin/sheets")
@Api(value = "SheetsController", tags = "页面管理")
public class SheetsController {

    @Autowired
    private SheetsService sheetsService;

    @Authorize
    @ApiOperation("分页页面")
    @GetMapping
    public CommonResult pageBy(@ApiParam(name = "page", value = "当前页") @RequestParam(name = "page", defaultValue = "1") int page,
                               @ApiParam(name = "size", value = "每页条数") @RequestParam(name = "size", defaultValue = "10") int size) {
        final IPage<SheetDTO> sheets = sheetsService.pageBy(page, size);
        return CommonResult.ok().data("sheets", sheets);
    }

    @Authorize
    @ApiOperation("根据页面id获取页面")
    @GetMapping("/info/{sheetId}")
    public CommonResult info(@ApiParam(name = "sheetId", value = "页面id") @PathVariable String sheetId) {
        final SheetDTO sheetDTO = sheetsService.convertTo(sheetsService.getById(sheetId));
        return CommonResult.ok().data("sheet", sheetDTO);
    }

    @Authorize
    @Log(logType = LogType.SHEET_PUBLISHED)
    @ApiOperation("新增页面")
    @ApiOperationSupport(ignoreParameters = {"id"})
    @PostMapping("/create")
    public CommonResult create(@ApiParam(name = "sheetVO", value = "页面信息") @RequestBody SheetVO sheetVO) {
        sheetsService.createBy(sheetVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.SHEET_EDIT)
    @ApiOperation("修改页面")
    @PutMapping("/update")
    public CommonResult update(@ApiParam(name = "sheetVO", value = "页面信息") @RequestBody SheetVO sheetVO) {
        sheetsService.createBy(sheetVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.SHEET_STATUS, saveRequestParams = true)
    @ApiOperation("修改页面状态")
    @PutMapping("/status")
    public CommonResult status(@ApiParam(name = "sheetId", value = "页面id") @RequestParam(name = "sheetId") String sheetId,
                               @ApiParam(name = "status", value = "页面状态") @RequestParam(name = "status") int status) {
        sheetsService.updateStatus(sheetId, status);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.SHEET_DELETE, saveRequestParams = true)
    @ApiOperation("根据页面id删除该页面")
    @DeleteMapping("/delete")
    public CommonResult delete(@ApiParam(name = "sheetId", value = "页面id") @RequestParam(name = "sheetId") String sheetId) {
        sheetsService.removeById(sheetId);
        return CommonResult.ok();
    }

}
