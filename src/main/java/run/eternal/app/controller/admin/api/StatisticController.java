package run.eternal.app.controller.admin.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import run.eternal.app.model.dto.StatisticDTO;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.service.StatisticService;

/**
 * Statistic controller
 *
 * @author : lwj
 * @since : 2020-08-12
 */
@RestController
@RequestMapping("/api/admin/statistics")
@Api(value = "StatisticController", tags = "统计信息")
public class StatisticController {

    @Autowired
    private StatisticService statisticService;

    @GetMapping
    @ApiOperation("获取统计信息")
    public CommonResult statistics() {
        final StatisticDTO statisticDTO = statisticService.getStatistic();
        return CommonResult.ok().data("statistics", statisticDTO);
    }


}
