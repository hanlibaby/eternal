package run.eternal.app.controller.admin.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.model.dto.post.PostDTO;
import run.eternal.app.model.dto.post.PostMinimalDTO;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.vo.query.PostQueryVO;
import run.eternal.app.model.vo.PostVO;
import run.eternal.app.service.PostsService;

import java.util.List;

/**
 * Posts controller
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@RestController
@RequestMapping("/api/admin/posts")
@Api(value = "PostsController", tags = "文章管理")
public class PostsController {

    @Autowired
    private PostsService postsService;

    @Authorize
    @ApiOperation("文章分页")
    @GetMapping
    public CommonResult pageBy(@ApiParam(name = "page", value = "当前页") @RequestParam(name = "page", defaultValue = "1") int page,
                               @ApiParam(name = "size", value = "每页条数") @RequestParam(name = "size", defaultValue = "10") int size) {
        final IPage<PostDTO> posts = postsService.pageBy(page, size);
        return CommonResult.ok().data("posts", posts);
    }


    @Authorize
    @ApiOperation("获取前top篇文章")
    @GetMapping("/latest")
    public CommonResult pageLatest(@ApiParam(name = "top", value = "前top篇")
                                   @RequestParam(name = "top", defaultValue = "5") int top) {
        final List<PostMinimalDTO> postMinimals = postsService.convertToMinimal(postsService.pageLatest(top));
        return CommonResult.ok().data("posts", postMinimals);
    }

    @Authorize
    @ApiOperation("根据查询条件分页文章")
    @PostMapping("/query/{page}/{size}")
    public CommonResult getByQuery(@ApiParam(name = "page", value = "当前页", defaultValue = "1") @PathVariable int page,
                                   @ApiParam(name = "size", value = "每页条数", defaultValue = "10") @PathVariable int size,
                                   @ApiParam(name = "postQueryVO", value = "postQueryVo") @RequestBody(required = false) PostQueryVO postQueryVO) {
        final IPage<PostDTO> posts = postsService.pageByQuery(page, size, postQueryVO);
        return CommonResult.ok().data("posts", posts);
    }

    @Authorize
    @ApiOperation("根据文章id获取文章")
    @GetMapping("/info/{postId}")
    public CommonResult info(@ApiParam(name = "postId", value = "文章id") @PathVariable String postId) {
        final PostDTO postDTO = postsService.convertTo(postsService.getById(postId));
        return CommonResult.ok().data("post", postDTO);
    }

    @Authorize
    @Log(logType = LogType.POST_PUBLISHED)
    @ApiOperation("新增文章")
    @PostMapping("/create")
    public CommonResult create(@ApiParam(name = "postVO", value = "文章信息") @RequestBody PostVO postVO) {
        postsService.createBy(postVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.POST_EDIT)
    @ApiOperation("修改文章")
    @PutMapping("/update")
    public CommonResult update(@ApiParam(name = "postVO", value = "文章信息") @RequestBody PostVO postVO) {
        postsService.createBy(postVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.POST_STATUS, saveRequestParams = true)
    @ApiOperation("修改文章状态")
    @PutMapping("/status")
    public CommonResult status(@ApiParam(name = "postId", value = "文章id") @RequestParam(name = "postId") String postId,
                               @ApiParam(name = "status", value = "文章状态") @RequestParam(name = "status") int status) {
        postsService.updateStatus(postId, status);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.POST_DELETED, saveRequestParams = true)
    @ApiOperation("根据文章id删除该文章")
    @DeleteMapping("/delete")
    public CommonResult delete(@ApiParam(name = "postId", value = "文章id") @RequestParam(name = "postId") String postId) {
        postsService.removeById(postId);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.TOP_POST, saveRequestParams = true)
    @ApiOperation("置顶文章")
    @PutMapping("/top")
    public CommonResult top(@ApiParam(name = "postId", value = "文章id") @RequestParam(name = "postId") String postId,
                            @ApiParam(name = "isTop", value = "是否置顶") @RequestParam(name = "isTop") int isTop) {
        postsService.setIsTop(postId, isTop);
        return CommonResult.ok();
    }

}

