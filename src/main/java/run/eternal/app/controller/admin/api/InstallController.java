package run.eternal.app.controller.admin.api;

import cn.hutool.crypto.SecureUtil;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ansi.AnsiColor;
import org.springframework.boot.ansi.AnsiOutput;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.exception.BadRequestException;
import run.eternal.app.model.dto.post.PostDTO;
import run.eternal.app.model.entity.Category;
import run.eternal.app.model.entity.Tag;
import run.eternal.app.model.entity.User;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.enums.PostStatus;
import run.eternal.app.model.enums.ResultStatus;
import run.eternal.app.model.enums.StorageType;
import run.eternal.app.model.properties.*;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.vo.*;
import run.eternal.app.service.*;
import run.eternal.app.service.impl.OptionsServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Install controller
 *
 * @author : lwj
 * @since : 2020-08-09
 */
@RestController
@RequestMapping("/api/admin/installations")
@Api(value = "InstallController", tags = "初始化博客")
@Slf4j
public class InstallController {

    @Autowired
    private UserService userService;

    @Autowired
    private PostsService postsService;

    @Autowired
    private CategoriesService categoriesService;

    @Autowired
    private TagsService tagsService;

    @Autowired
    private MenusService menusService;

    @Autowired
    private SheetsService sheetsService;

    @Autowired
    private CommentsService commentsService;

    @Autowired
    private OptionsServiceImpl optionsService;

    @Log(logType = LogType.BLOG_INITIALIZED, saveRequestParams = true)
    @ApiOperation(value = "初始化博客")
    @ApiOperationSupport(ignoreParameters = {"id", "avatar", "description"})
    @PostMapping
    public CommonResult installBlog(@RequestBody InstallVO installVO) {
        // 检查博客是否已安装
        Boolean isInstall = optionsService.getByPropertyOrDefault(PrimaryProperties.IS_INSTALLED, Boolean.class, false);
        if (isInstall) {
            throw new BadRequestException(ResultStatus.ALREADY_INSTALL_BLOG);
        }
        // 初始化设置
        initSettings(installVO);

        // 创建默认用户
        User user = createUser(installVO);
        log.info(AnsiOutput.toString(AnsiColor.CYAN, "创建用户：" + user + "，原密码：" + installVO.getPassword()));

        // 创建默认分类
        Category category = createDefaultCategory();

        // 创建默认标签
        Tag tag = createDefaultTag();

        // 创建默认文章
        PostDTO post = createDefaultPost(category, tag);

        // 创建默认页面
        createDefaultSheet();

        // 创建默认菜单
        createDefaultMenu();

        // 创建默认评论
        createDefaultComment(post);

        return CommonResult.ok().message("安装完成！");
    }

    private void createDefaultMenu() {
        int menuCount = menusService.count();
        if (menuCount > 0) {
            return;
        }

        // 创建首页
        MenuVO menuIndex = MenuVO.builder()
                .name("首页")
                .sort(0)
                .url("/")
                .icon("fa fa-home")
                .build();
        menusService.createBy(menuIndex);

        // 创建文章归页
        MenuVO menuArchive = MenuVO.builder()
                .name("文章归档")
                .sort(1)
                .url("/archives")
                .icon("fa fa-archive")
                .build();
        menusService.createBy(menuArchive);

        // 创建默认分类页
        MenuVO menuCategory = MenuVO.builder()
                .name("默认分类")
                .sort(2)
                .url("/categories/default")
                .icon("fa fa-binoculars")
                .build();
        menusService.createBy(menuCategory);

        // 创建关于页面
        MenuVO menuSheet = MenuVO.builder()
                .name("关于页面")
                .sort(3)
                .url("/s/about")
                .icon("fa fa-leaf")
                .build();
        menusService.createBy(menuSheet);
    }

    private Tag createDefaultTag() {
        int tagCount = tagsService.count();
        if (tagCount > 0) {
            return null;
        }
        TagVO tagVO = TagVO.builder()
                .name("默认标签")
                .alias("这是你的默认标签，如不需要，删除即可。")
                .build();
        return tagsService.createBy(tagVO);
    }

    private Category createDefaultCategory() {
        int categoryCount = categoriesService.count();
        if (categoryCount > 0) {
            return null;
        }
        CategoryVO categoryVO = CategoryVO.builder()
                .name("默认分类")
                .alias("default")
                .description("这是你的默认分类，如不需要，删除即可。")
                .build();
        return categoriesService.createBy(categoryVO);
    }

    private void createDefaultComment(@Nullable PostDTO post) {
        if (null == post) {
            return;
        }

        int commentCount = commentsService.count();

        if (commentCount > 0) {
            return;
        }

        // 创建默认评论
        CommentVO comment = CommentVO.builder()
                .author("eternal")
                .authorUrl("https://www.lwjppz.cn")
                .content("欢迎使用 Eternal，这是你的第一条评论，头像来自 [Gravatar](https://cn.gravatar.com)，你也可以通过注册 [Gravatar](https://cn.gravatar.com) 来显示自己的头像。")
                .email("hello@eternal.com")
                .postId(post.getId())
                .build();
        commentsService.createBy(comment);
    }

    private void createDefaultSheet() {
        int publishedCount = sheetsService.countByStatus(PostStatus.PUBLISHED);
        if (publishedCount > 0) {
            return;
        }
        SheetVO sheetVO = SheetVO.builder()
                .title("关于页面")
                .status(PostStatus.PUBLISHED.getValue())
                .url("about")
                .originalContent("## 关于页面\n" +
                        "\n" +
                        "这是一个自定义页面，你可以在后台的 `页面` -> `所有页面` -> `自定义页面` 找到它，你可以用于新建关于页面、留言板页面等等。发挥你自己的想象力！\n" +
                        "\n" +
                        "> 这是一篇自动生成的页面，你可以在后台删除它。")
                .build();
        sheetsService.createBy(sheetVO);
    }

    private PostDTO createDefaultPost(Category category, Tag tag) {
        int count = postsService.countByStatus(PostStatus.PUBLISHED);
        if (count > 0) {
            return null;
        }
        PostVO postVO = PostVO.builder()
                .title("hello eternal")
                .url("hello-eternal")
                .status(PostStatus.PUBLISHED.getValue())
                .originalContent("## Hello Eternal\n" +
                        "\n" +
                        "如果你看到了这一篇文章，那么证明你已经安装成功了，感谢使用 ***Eternal*** 进行创作，希望能够使用愉快。\n" +
                        "\n" +
                        "> 这是一篇自动生成的文章，请删除这篇文章之后开始你的创作吧！")
                .build();
        List<String> categoryIds = new ArrayList<>();
        List<String> tagIds = new ArrayList<>();
        if (null != category) {
            categoryIds.add(category.getId());
            postVO.setCategoryIds(categoryIds);
        }
        if (null != tag) {
            tagIds.add(tag.getId());
            postVO.setTagIds(tagIds);
        }
        return postsService.createBy(postVO, tagIds, categoryIds);
    }

    private User createUser(InstallVO installVO) {
        return userService.getCurrentUser().map(user -> {
            // 更新user
            BeanUtils.copyProperties(installVO, user);
            // 手动设置密码（为密码加密）
            userService.setPassword(user, installVO.getPassword());
            // Update user
            return userService.update(user);
        }).orElseGet(() -> {
            String gravatar = "//cn.gravatar.com/avatar/" + SecureUtil.md5(installVO.getEmail()) +
                    "?s=256&d=mm";
            installVO.setAvatar(gravatar);
            return userService.createBy(installVO);
        });
    }

    private void initSettings(InstallVO installVO) {
        // 初始化默认设置
        Map<PropertyEnum, String> properties = Maps.newHashMap();
        properties.put(PrimaryProperties.IS_INSTALLED, Boolean.TRUE.toString());
        properties.put(AttachmentProperties.STORAGE_TYPE, StorageType.LOCAL.getValue());
        properties.put(BlogProperties.BLOG_TITLE, installVO.getTitle());
        properties.put(CommentProperties.NEW_NEED_CHECK, Boolean.TRUE.toString());
        properties.put(BlogProperties.BLOG_URL, StringUtils.isBlank(installVO.getUrl()) ? optionsService.getBlogBaseUrl() : installVO.getUrl());
        properties.put(PrimaryProperties.BIRTHDAY, String.valueOf(System.currentTimeMillis()));

        // 保存设置
        optionsService.saveProperties(properties);
    }
}
