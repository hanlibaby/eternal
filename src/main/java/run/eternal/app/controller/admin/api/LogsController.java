package run.eternal.app.controller.admin.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.model.dto.log.LogDTO;
import run.eternal.app.model.dto.log.LogMinimalDTO;
import run.eternal.app.model.entity.Logs;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.vo.query.LogQueryVO;
import run.eternal.app.service.LogsService;
import run.eternal.app.utils.ServiceUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * Logs controller
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@RestController
@RequestMapping("/api/admin/logs")
@Api(value = "LogsController", tags = "日志管理")
public class LogsController {

    @Autowired
    private LogsService logsService;

    @Authorize
    @ApiOperation("获取前top条日志信息")
    @GetMapping("/latest")
    public CommonResult pageLatest(@ApiParam(name = "top", value = "top")
                                   @RequestParam(name = "top", defaultValue = "10") int top) {
        final List<LogMinimalDTO> logMinimals = logsService.convertToMinimal(logsService.pageLatest(top));
        return CommonResult.ok().data("logs", logMinimals);
    }

    @Authorize
    @ApiOperation("获取所有操作类型")
    @GetMapping("/logTypes")
    public CommonResult listLogTypes() {
        final List<String> logTypes = logsService.listLogTypes();
        return CommonResult.ok().data("logTypes", logTypes);
    }

    @Authorize
    @ApiOperation("条件查询日志信息")
    @PostMapping("/query/{page}/{size}")
    public CommonResult getByQuery(@ApiParam(name = "page", value = "当前页") @PathVariable(name = "page") int page,
                                   @ApiParam(name = "size", value = "每页条数") @PathVariable(name = "size") int size,
                                   @ApiParam(name = "LogQueryVO", value = "日志查询条件") @RequestBody LogQueryVO logQueryVO) {
        final IPage<LogDTO> logs = logsService.getByQuery(page, size, logQueryVO);
        return CommonResult.ok().data("logs", logs);
    }


    @Authorize
    @ApiOperation("批量删除日志")
    @DeleteMapping("/delete")
    public CommonResult deleteBatch(@ApiParam(name = "ids", value = "日志id集合") @RequestBody List<String> ids) {
        logsService.removeByIds(ids);
        return CommonResult.ok();
    }

    @Authorize
    @ApiOperation("清空日志")
    @DeleteMapping("/clear")
    public CommonResult clearAll() {
        logsService.removeByIds(ServiceUtils.fetchProperty(logsService.list(), Logs::getId));
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.EXPORT_LOGS)
    @ApiOperation("导出日志")
    @GetMapping("/export")
    public void export(HttpServletResponse response) throws IOException {
//        response.setContentType("application/vnd.ms-excel");
//        response.setCharacterEncoding("utf-8");
//        String fileName = URLEncoder.encode("Eternal-logs" + System.currentTimeMillis(), "UTF-8");
//        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
//        EasyExcel.write(response.getOutputStream(), LogDTO.class).sheet("日志").doWrite(logsService.convertTo(logsService.list()));
    }

}

