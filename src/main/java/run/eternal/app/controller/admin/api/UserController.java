package run.eternal.app.controller.admin.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.model.dto.user.UserDetailDTO;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.vo.UserVO;
import run.eternal.app.service.UserService;

import java.util.Objects;

/**
 * User controller
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@RestController
@RequestMapping("/api/admin/user")
@Api(value = "UserController", tags = "用户管理")
public class UserController {

    @Autowired
    private UserService userService;

    @Authorize
    @ApiOperation("获取用户信息")
    @GetMapping("/profiles")
    public CommonResult profile() {
        final UserDetailDTO user = userService.convertToDetail(Objects.requireNonNull(userService.getCurrentUser().orElse(null)));
        return CommonResult.ok().data("profile", user);
    }

    @Authorize
    @Log(logType = LogType.PROFILE_UPDATED, saveRequestParams = true)
    @ApiOperation("更新用户信息")
    @PutMapping("/update")
    public CommonResult update(@ApiParam(name = "userVO", value = "用户信息") @RequestBody UserVO userVO) {
        userService.updateBy(userVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.PASSWORD_UPDATED, saveRequestParams = true)
    @ApiOperation("更新密码")
    @PutMapping("/profiles/password")
    public CommonResult password(@ApiParam(name = "oldPassword", value = "原密码") @RequestParam(name = "oldPassword") String oldPassword,
                                 @ApiParam(name = "newPassword", value = "新密码") @RequestParam(name = "newPassword") String newPassword) {
        userService.updatePassword(oldPassword, newPassword);
        return CommonResult.ok();
    }

}

