package run.eternal.app.controller.admin.api;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.model.dto.link.LinkDetailDTO;
import run.eternal.app.model.dto.link.LinkMinimalDTO;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.vo.LinkVO;
import run.eternal.app.service.LinksService;

import java.util.List;

/**
 * Links controller
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@RestController
@RequestMapping("/api/admin/links")
@Api(value = "LinksController", tags = "友链管理")
public class LinksController {

    @Autowired
    private LinksService linksService;

    @Authorize
    @ApiOperation("获取友链列表")
    @GetMapping("/list")
    public CommonResult list() {
        final List<LinkMinimalDTO> links = linksService.convertTo(linksService.list());
        return CommonResult.ok().data("links", links);
    }

    @Authorize
    @ApiOperation("根据友链id获取友链信息")
    @GetMapping("/info/{linkId}")
    public CommonResult info(@ApiParam(name = "linkId", value = "友链id") @PathVariable(name = "linkId") String linkId) {
        final LinkDetailDTO linkDetailDTO = linksService.convertTo(linksService.getById(linkId));
        return CommonResult.ok().data("link", linkDetailDTO);
    }

    @Authorize
    @Log(logType = LogType.LINK_PUBLISHED, saveRequestParams = true)
    @ApiOperation("新增友链")
    @ApiOperationSupport(ignoreParameters = {"id"})
    @PostMapping("create")
    public CommonResult create(@ApiParam(name = "linkVO", value = "友链表单") @RequestBody LinkVO linkVO) {
        linksService.createBy(linkVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.LINK_EDIT, saveRequestParams = true)
    @ApiOperation("编辑友链")
    @PutMapping("/update")
    public CommonResult update(@ApiParam(name = "linkVO", value = "友链表单") @RequestBody LinkVO linkVO) {
        linksService.updateBy(linkVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.LINK_DELETE, saveRequestParams = true)
    @ApiOperation("删除友链")
    @DeleteMapping("/delete")
    public CommonResult delete(@ApiParam(name = "linkId", value = "友链id", required = true) @RequestParam(name = "linkId") String linkId) {
        linksService.removeById(linkId);
        return CommonResult.ok();
    }

}

