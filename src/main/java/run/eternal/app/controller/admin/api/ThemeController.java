package run.eternal.app.controller.admin.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.handler.theme.support.Group;
import run.eternal.app.handler.theme.support.ThemeProperty;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.support.ThemeFile;
import run.eternal.app.service.ThemeService;
import run.eternal.app.service.ThemeSettingsService;

import java.util.List;
import java.util.Map;

/**
 * Themes controller
 *
 * @author : lwj
 * @since : 2020-08-21
 */
@RestController
@RequestMapping("/api/admin/themes")
@Api(value = "ThemeController", tags = "主题管理")
public class ThemeController {

    @Autowired
    private ThemeService themeService;

    @Autowired
    private ThemeSettingsService themeSettingsService;

    @Authorize
    @GetMapping("{themeId}")
    @ApiOperation("通过主题id获主题信息")
    public CommonResult getBy(@PathVariable("themeId") String themeId) {
        ThemeProperty themeProperty = themeService.getThemeOfNonNullBy(themeId);
        return CommonResult.ok().data("theme", themeProperty);
    }

    @Authorize
    @GetMapping
    @ApiOperation("获取所有主题")
    public CommonResult listAll() {
        List<ThemeProperty> themes = themeService.getThemes();
        return CommonResult.ok().data("themes", themes);
    }

    @Authorize
    @GetMapping("/activation/files")
    @ApiOperation("列出所有激活的主题文件")
    public CommonResult listFiles() {
        List<ThemeFile> themeFiles = themeService.listThemeFolderBy(themeService.getActivatedThemeId());
        return CommonResult.ok().data("themeFiles", themeFiles);
    }

    @Authorize
    @GetMapping("{themeId}/files")
    @ApiOperation("根据主题ID列出所有主题文件")
    public CommonResult listFiles(@PathVariable("themeId") String themeId) {
        List<ThemeFile> themeFiles = themeService.listThemeFolderBy(themeId);
        return CommonResult.ok().data("themeFiles", themeFiles);
    }

    @Authorize
    @PostMapping("{themeId}/activation")
    @ApiOperation("激活主题")
    public CommonResult active(@PathVariable("themeId") String themeId) {
        themeService.activateTheme(themeId);
        return CommonResult.ok();
    }

    @Authorize
    @GetMapping("/activation")
    @ApiOperation("获取已激活主题")
    public CommonResult getActivateTheme() {
        ThemeProperty themeProperty = themeService.getThemeOfNonNullBy(themeService.getActivatedThemeId());
        return CommonResult.ok().data("theme", themeProperty);
    }

    @Authorize
    @GetMapping("/activation/configurations")
    @ApiOperation("获取当前激活的主题配置")
    public CommonResult fetchConfig() {
        List<Group> groups = themeService.fetchConfig(themeService.getActivatedThemeId());
        return CommonResult.ok().data("groups", groups);
    }

    @Authorize
    @GetMapping("{themeId}/configurations")
    @ApiOperation("通过主题ID获取主题配置")
    public CommonResult fetchConfig(@PathVariable("themeId") String themeId) {
        List<Group> groups = themeService.fetchConfig(themeId);
        return CommonResult.ok().data("groups", groups);
    }

    @Authorize
    @GetMapping("/activation/settings")
    @ApiOperation("列出当前激活的主题设置")
    public CommonResult listSettingsBy() {
        Map<String, Object> map = themeSettingsService.listAsMapBy(themeService.getActivatedThemeId());
        return CommonResult.ok().data("settings", map);
    }

    @Authorize
    @GetMapping("{themeId}/settings")
    @ApiOperation("通过主题id获取该主题所有设置")
    public CommonResult listSettingsBy(@PathVariable("themeId") String themeId) {
        Map<String, Object> map = themeSettingsService.listAsMapBy(themeId);
        return CommonResult.ok().data("settings", map);
    }

    @Authorize
    @PostMapping("activation/settings")
    @ApiOperation("保存主题（当前激活主题）设置")
    public CommonResult saveSettingsBy(@RequestBody Map<String, Object> settings) {
        themeSettingsService.save(settings, themeService.getActivatedThemeId());
        return CommonResult.ok();
    }

    @Authorize
    @PostMapping("{themeId}/settings")
    @ApiOperation("通过主题id保存主题设置")
    public CommonResult saveSettingsBy(@PathVariable("themeId") String themeId,
                                       @RequestBody Map<String, Object> settings) {
        themeSettingsService.save(settings, themeId);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.THEME_DELETE, saveRequestParams = true)
    @DeleteMapping("{themeId}")
    @ApiOperation("删除主题")
    public CommonResult deleteBy(@PathVariable("themeId") String themeId) {
        themeService.deleteTheme(themeId);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.THEME_UPLOAD)
    @PostMapping("upload")
    @ApiOperation("上传主题")
    public CommonResult uploadTheme(@RequestPart("file") MultipartFile file) {
        themeService.upload(file);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.THEME_UPDATE)
    @PutMapping("upload/{themeId}")
    @ApiOperation("更新主题")
    public CommonResult updateThemeByUpload(@PathVariable("themeId") String themeId,
                                            @RequestPart("file") MultipartFile file) {
        themeService.update(themeId, file);
        return CommonResult.ok();
    }

}
