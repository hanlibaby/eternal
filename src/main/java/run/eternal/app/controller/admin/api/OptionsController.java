package run.eternal.app.controller.admin.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.service.OptionsService;

import java.util.Map;

/**
 * Options controller
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@RestController
@RequestMapping("/api/admin/options")
@Api(value = "OptionsController", tags = "博客设置管理")
public class OptionsController {

    @Autowired
    private OptionsService optionsService;

    @ApiOperation("获取博客所有设置项key-value")
    @GetMapping("/map-view")
    public CommonResult listOptions() {
        final Map<String, Object> options = optionsService.listOptions();
        return CommonResult.ok().data("options", options);
    }

    @ApiOperation("保存博客设置")
    @PostMapping("/save")
    public CommonResult save(@ApiParam(name = "optionsMap", value = "博客设置集合") @RequestBody Map<String, Object> optionsMap) {
        optionsService.saveByMap(optionsMap);
        return CommonResult.ok().data("options", optionsMap);
    }

}

