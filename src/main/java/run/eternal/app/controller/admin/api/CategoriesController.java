package run.eternal.app.controller.admin.api;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.model.dto.category.CategoryMinimalDTO;
import run.eternal.app.model.dto.category.CategoryDetailDTO;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.vo.CategoryVO;
import run.eternal.app.service.CategoriesService;

import java.util.List;

/**
 * Categories controller
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@RestController
@RequestMapping("/api/admin/categories")
@Api(value = "CategoriesController", tags = "分类管理")
public class CategoriesController {

    @Autowired
    private CategoriesService categoriesService;

    @Authorize
    @ApiOperation("获取分类列表")
    @GetMapping("/list")
    public CommonResult list() {
        final List<CategoryMinimalDTO> categories = categoriesService.convertTo(categoriesService.list());
        return CommonResult.ok().data("categories", categories);
    }

    @Authorize
    @ApiOperation("根据分类id获取分类信息")
    @GetMapping("/info/{categoryId}")
    public CommonResult info(@ApiParam(name = "categoryId", value = "分类id", required = true) @PathVariable("categoryId") String categoryId) {
        final CategoryDetailDTO categoryDetailDTO = categoriesService.convertTo(categoriesService.getById(categoryId));
        return CommonResult.ok().data("category", categoryDetailDTO);
    }

    @Authorize
    @Log(logType = LogType.CATEGORY_PUBLISHED, saveRequestParams = true)
    @ApiOperationSupport(ignoreParameters = {"id"})
    @ApiOperation("新增分类")
    @PostMapping("/create")
    public CommonResult create(@ApiParam(name = "categoryVO", value = "分类信息") @RequestBody CategoryVO categoryVO) {
        categoriesService.createBy(categoryVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.CATEGORY_EDIT, saveRequestParams = true)
    @ApiOperation("编辑分类")
    @PutMapping("/update")
    public CommonResult update(@ApiParam(name = "categoryVO", value = "分类信息") @RequestBody CategoryVO categoryVO) {
        categoriesService.updateBy(categoryVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.CATEGORY_DELETE, saveRequestParams = true)
    @ApiOperation("删除分类")
    @DeleteMapping("/delete")
    public CommonResult delete(@ApiParam(name = "categoryId", value = "分类id", required = true) @RequestParam(name = "categoryId") String categoryId) {
        categoriesService.removeById(categoryId);
        return CommonResult.ok();
    }

}

