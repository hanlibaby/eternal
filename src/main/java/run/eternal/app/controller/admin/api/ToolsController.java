package run.eternal.app.controller.admin.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.service.ToolsService;

import java.io.IOException;
import java.text.ParseException;

/**
 * Tools controller
 *
 * @author : lwj
 * @since : 2020-08-23
 */
@RestController
@RequestMapping("/api/admin/tools")
@Api(value = "ToolsController", tags = "博客工具管理")
public class ToolsController {

    @Autowired
    private ToolsService toolsService;

    @ApiOperation("hexo文章导入")
    @PostMapping("/import")
    public CommonResult importPost(@ApiParam(name = "file", value = "markdown文件") @RequestPart("file") MultipartFile file) throws IOException, ParseException {
        toolsService.importMarkdown(file);
        return CommonResult.ok();
    }

}
