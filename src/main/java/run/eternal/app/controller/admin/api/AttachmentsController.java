package run.eternal.app.controller.admin.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.model.dto.attachment.AttachmentDetailDTO;
import run.eternal.app.model.dto.attachment.AttachmentMinimalDTO;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.vo.query.AttachmentQueryVO;
import run.eternal.app.service.AttachmentsService;

import java.util.List;

/**
 * Attachment controller
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@RestController
@RequestMapping("/api/admin/attachments")
@Api(value = "AttachmentsController", tags = "附件管理")
public class AttachmentsController {

    @Autowired
    private AttachmentsService attachmentsService;

    @Authorize
    @ApiOperation("获取所有附件类型")
    @GetMapping("/listMedia")
    public CommonResult listMedia() {
        final List<String> medias = attachmentsService.listMedia();
        return CommonResult.ok().data("medias", medias);
    }

    @Authorize
    @ApiOperation("获取所有附件存储位置")
    @GetMapping("/listStorage")
    public CommonResult listStorage() {
        final List<String> listStorage = attachmentsService.listStorage();
        return CommonResult.ok().data("storage", listStorage);
    }

    @Authorize
    @ApiOperation("分页条件查询附件")
    @PostMapping("/query/{page}/{size}")
    public CommonResult getByQuery(@ApiParam(name = "page", value = "当前页", defaultValue = "1") @PathVariable int page,
                                   @ApiParam(name = "size", value = "每页条数", defaultValue = "10") @PathVariable int size,
                                   @ApiParam(name = "attachmentQueryVO", value = "attachmentQueryVO") @RequestBody(required = false) AttachmentQueryVO attachmentQueryVO) {
        final IPage<AttachmentMinimalDTO> records = attachmentsService.pageByQuery(page, size, attachmentQueryVO);
        return CommonResult.ok().data("records", records);
    }

    @Authorize
    @ApiOperation("分页条件查询附件（图片）")
    @PostMapping("/queryImages/{page}/{size}")
    public CommonResult getByQueryImage(@ApiParam(name = "page", value = "当前页", defaultValue = "1") @PathVariable int page,
                                        @ApiParam(name = "size", value = "每页条数", defaultValue = "10") @PathVariable int size,
                                        @ApiParam(name = "attachmentQueryVO", value = "attachmentQueryVO") @RequestBody(required = false) AttachmentQueryVO attachmentQueryVO) {
        final IPage<AttachmentMinimalDTO> records = attachmentsService.pageByQueryImage(page, size, attachmentQueryVO);
        return CommonResult.ok().data("records", records);
    }

    @Authorize
    @Log(logType = LogType.ATTACHMENT_UPLOAD)
    @ApiOperation("上传附件")
    @PostMapping("/upload")
    public CommonResult upload(@ApiParam(name = "attachment", value = "附件") @RequestBody MultipartFile attachment) {
        final AttachmentMinimalDTO attachmentMinimalDTO = attachmentsService.convertToMinimal(attachmentsService.upload(attachment));
        return CommonResult.ok().data("attachment", attachmentMinimalDTO);
    }

    @Authorize
    @Log(logType = LogType.ATTACHMENT_UPLOAD_BATCH)
    @ApiOperation("批量上传附件")
    @PostMapping("/batch-upload")
    public CommonResult batchUpload(@ApiParam(name = "attachments", value = "附件集合") @RequestBody MultipartFile[] attachments) {
        for (MultipartFile attachment : attachments) {
            attachmentsService.upload(attachment);
        }
        return CommonResult.ok();
    }


    @Authorize
    @ApiOperation("根据附件id获取附件信息")
    @GetMapping("/info/{attachmentId}")
    public CommonResult info(@ApiParam(name = "attachmentId", value = "附件id") @PathVariable(name = "attachmentId") String attachmentId) {
        final AttachmentDetailDTO attachmentDetailDTO = attachmentsService.convertToDetail(attachmentsService.getById(attachmentId));
        return CommonResult.ok().data("attachment", attachmentDetailDTO);
    }

    @Authorize
    @Log(logType = LogType.ATTACHMENT_DELETE, saveRequestParams = true)
    @ApiOperation("根据附件id删除附件")
    @DeleteMapping("/delete")
    public CommonResult delete(@ApiParam(name = "attachmentId", value = "附件id") @RequestParam(name = "attachmentId") String attachmentId) {
        attachmentsService.removePermanently(attachmentId);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.ATTACHMENT_DELETE_BATCH, saveRequestParams = true)
    @ApiOperation("根据附件id集合批量删除附件")
    @DeleteMapping("/batch-delete")
    public CommonResult batchDelete(@ApiParam(name = "附件id集合", value = "ids") @RequestBody List<String> ids) {
        attachmentsService.removePermanently(ids);
        return CommonResult.ok();
    }

}

