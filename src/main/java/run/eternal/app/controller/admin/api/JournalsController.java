package run.eternal.app.controller.admin.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.logger.annotation.Log;
import run.eternal.app.model.dto.journal.JournalDTO;
import run.eternal.app.model.dto.journal.JournalDetailDTO;
import run.eternal.app.model.enums.LogType;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.model.vo.JournalVO;
import run.eternal.app.model.vo.query.JournalQueryVO;
import run.eternal.app.service.JournalsService;

import java.util.List;

/**
 * Journals controller
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@RestController
@RequestMapping("/api/admin/journals")
@Api(value = "JournalsController", tags = "随记管理")
public class JournalsController {

    @Autowired
    private JournalsService journalsService;

    @Authorize
    @ApiOperation("分页随记")
    @GetMapping
    public CommonResult pageBy(@ApiParam(name = "page", value = "当前页") @RequestParam(name = "page", defaultValue = "1") int page,
                               @ApiParam(name = "size", value = "每页条数") @RequestParam(name = "size", defaultValue = "10") int size) {
        final IPage<JournalDTO> journals = journalsService.pageBy(page, size);
        return CommonResult.ok().data("journals", journals);
    }

    @Authorize
    @ApiOperation("根据查询条件查询随记")
    @PostMapping("/query")
    public CommonResult getByQuery(@ApiParam(name = "journalQueryVO", value = "随记查询表单") @RequestBody(required = false) JournalQueryVO journalQueryVO) {
        final List<JournalDTO> journals = journalsService.queryVO(journalQueryVO);
        return CommonResult.ok().data("journals", journals);
    }

    @Authorize
    @ApiOperation("根据查询条件分页查询随记")
    @PostMapping("/query/{page}/{size}")
    public CommonResult getByQueryPage(@ApiParam(name = "page", value = "当前页", defaultValue = "1") @PathVariable int page,
                                       @ApiParam(name = "size", value = "每页条数", defaultValue = "10") @PathVariable int size,
                                       @ApiParam(name = "journalQueryVO", value = "随记查询表单") @RequestBody(required = false) JournalQueryVO journalQueryVO) {
        final IPage<JournalDTO> journals = journalsService.pageByQuery(page, size, journalQueryVO);
        return CommonResult.ok().data("journals", journals);
    }

    @Authorize
    @ApiOperation("根据随记id获取随记信息")
    @GetMapping("/info/{journalId}")
    public CommonResult info(@ApiParam(name = "journalId", value = "随记id") @PathVariable String journalId) {
        final JournalDetailDTO journal = journalsService.convertToDetail(journalsService.getById(journalId));
        return CommonResult.ok().data("journal", journal);
    }

    @Authorize
    @Log(logType = LogType.JOURNAL_PUBLISHED, saveRequestParams = true)
    @ApiOperation("新增随记")
    @ApiOperationSupport(ignoreParameters = {"id"})
    @PostMapping("/create")
    public CommonResult create(@ApiParam(name = "journalVO", value = "随记信息") @RequestBody JournalVO journalVO) {
        journalsService.createBy(journalVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.JOURNAL_EDIT, saveRequestParams = true)
    @ApiOperation("修改随记")
    @PutMapping("/update")
    public CommonResult update(@ApiParam(name = "journalVO", value = "随记信息") @RequestBody JournalVO journalVO) {
        journalsService.createBy(journalVO);
        return CommonResult.ok();
    }

    @Authorize
    @Log(logType = LogType.JOURNAL_DELETE, saveRequestParams = true)
    @ApiOperation("根据随记id删除随记")
    @DeleteMapping("/delete")
    public CommonResult delete(@ApiParam(name = "journalId", value = "随记id") @RequestParam(name = "journalId") String journalId) {
        journalsService.removeById(journalId);
        return CommonResult.ok();
    }

}

