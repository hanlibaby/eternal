package run.eternal.app.controller.admin.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import run.eternal.app.config.annotation.Authorize;
import run.eternal.app.model.dto.comment.PostCommentMinimalDTO;
import run.eternal.app.model.enums.CommentStatus;
import run.eternal.app.model.support.CommonResult;
import run.eternal.app.service.CommentsService;

import java.util.List;

/**
 * Post Comments controller
 *
 * @author : lwj
 * @since : 2020-08-11
 */
@RestController
@RequestMapping("/api/admin/posts/comments")
@Api(value = "PostCommentController", tags = "文章评论管理")
public class PostCommentController {

    @Autowired
    private CommentsService commentsService;

    @Authorize
    @ApiOperation("获取前top条文章评论")
    @GetMapping("/latest")
    public CommonResult pageLatest(@ApiParam(name = "top", value = "top")
                                   @RequestParam(name = "top", defaultValue = "10") int top) {
        final List<PostCommentMinimalDTO> commentMinimals = commentsService.convertToPostMinimal(commentsService.pageLatestPostByStatus(top, CommentStatus.PASS));
        return CommonResult.ok().data("postComments", commentMinimals);
    }

}
