package run.eternal.app.controller.content.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import run.eternal.app.model.dto.sheet.SheetDTO;
import run.eternal.app.service.SheetsService;
import run.eternal.app.service.ThemeService;

/**
 * @author : lwj
 * @since : 2020-08-23
 */
@Component
public class SheetModel {

    @Autowired
    private SheetsService sheetsService;

    @Autowired
    private ThemeService themeService;

    @Autowired
    private ModelProcess modelProcess;

    public String content(String slug, Model model) {

        SheetDTO sheet = sheetsService.convertTo(sheetsService.getBySlug(slug));

        sheetsService.publishVisitEvent(sheet.getId());

        model.addAttribute("post", sheet);
        modelProcess.addGlobalVariable(model);
        return themeService.render("sheet");
    }
}
