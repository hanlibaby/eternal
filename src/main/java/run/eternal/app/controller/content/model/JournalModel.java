package run.eternal.app.controller.content.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import run.eternal.app.model.entity.Journal;
import run.eternal.app.service.JournalsService;
import run.eternal.app.service.ThemeService;

import java.util.List;

/**
 * @author : lwj
 * @since : 2020-08-23
 */
@Component
public class JournalModel {

    @Autowired
    private JournalsService journalsService;

    @Autowired
    private ThemeService themeService;

    @Autowired
    private ModelProcess modelProcess;

    public String list(Model model) {

        List<Journal> journals = journalsService.listPublic();

        model.addAttribute("journals", journals);
        modelProcess.addGlobalVariable(model);
        return themeService.render("journals");
    }

}
