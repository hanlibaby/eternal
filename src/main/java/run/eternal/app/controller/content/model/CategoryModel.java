package run.eternal.app.controller.content.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import run.eternal.app.model.entity.Category;
import run.eternal.app.model.vo.PostListVO;
import run.eternal.app.service.*;

import java.util.List;

/**
 * @author : lwj
 * @since : 2020-08-21
 */
@Component
public class CategoryModel {

    @Autowired
    private CategoriesService categoriesService;

    @Autowired
    private ThemeService themeService;

    @Autowired
    private PostCategoriesService postCategoriesService;

    @Autowired
    private PostsService postsService;

    @Autowired
    private ModelProcess modelProcess;

    /**
     * List category posts.
     *
     * @param model model
     * @param category  category
     * @param page  current page
     * @return template name
     */
    public String listPost(Model model, String category) {

        Category currentCategory = categoriesService.getByName(category);

        List<String> postIds =  postCategoriesService.listPostIdsByCategoryId(currentCategory.getId());

        List<PostListVO> posts = postsService.convertToListVO(postsService.getByIds(postIds));

        model.addAttribute("posts", posts);
        model.addAttribute("category", currentCategory);
        modelProcess.addGlobalVariable(model);
        return themeService.render("categories");
    }

}
