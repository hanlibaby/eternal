package run.eternal.app.controller.content.model;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import run.eternal.app.model.dto.post.PostDTO;
import run.eternal.app.model.enums.PostStatus;
import run.eternal.app.model.vo.PostListVO;
import run.eternal.app.service.OptionsService;
import run.eternal.app.service.PostsService;
import run.eternal.app.service.ThemeService;

import java.util.List;
import java.util.Map;

/**
 * @author : lwj
 * @since : 2020-08-21
 */
@Component
public class PostModel {

    @Autowired
    private PostsService postsService;

    @Autowired
    private OptionsService optionsService;

    @Autowired
    private ThemeService themeService;

    @Autowired
    private ModelProcess modelProcess;

    public String list(Integer page, Model model) {
        // get post size index page
        int postSize = optionsService.getIndexPostPageSize();

        // get post count
        int totalPost = postsService.countByStatus(PostStatus.PUBLISHED);

        // count total post pages
        int totalPage = 1;
        if (totalPost > postSize) {
            totalPage = totalPost / postSize == 0 ? totalPost / postSize : totalPost / postSize + 1;
        }

        boolean hasPrev = page != 1;
        boolean hasNext = page != totalPage;

        if (hasPrev) {
            model.addAttribute("prevPage", page - 1);
        }

        if (hasNext) {
            int nextPage = page + 1;
            model.addAttribute("nextPage", nextPage);
        }

        IPage<PostDTO> postPage = postsService.pageBy(page, postSize);
        List<PostListVO> posts = postsService.convertToListVO(postPage.getRecords());

        model.addAttribute("is_index", true);
        model.addAttribute("posts", posts);
        model.addAttribute("currentPage", page);
        model.addAttribute("hasPrev", hasPrev);
        model.addAttribute("hasNext", hasNext);
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("meta_keywords", optionsService.getSeoKeywords());
        model.addAttribute("meta_description", optionsService.getSeoDescription());

        modelProcess.addGlobalVariable(model);
        return themeService.render("index");
    }

    public String content(String postUrl, Model model) {

        List<PostListVO> posts = postsService.selectPrevAndNext(postUrl);

        posts.forEach(postListVO -> {
            if (postUrl.equals(postListVO.getUrl())) {
                postsService.publishVisitEvent(postListVO.getId());
            }
        });

        if (posts.size() == 3) {
            model.addAttribute("prevPost", posts.get(0));
            model.addAttribute("post", posts.get(1));
            model.addAttribute("nextPost", posts.get(2));
        } else {
            if (posts.get(0).getUrl().equals(postUrl)) {
                model.addAttribute("post", posts.get(0));
                model.addAttribute("nextPost", posts.get(1));
            } else {
                model.addAttribute("prevPost", posts.get(0));
                model.addAttribute("post", posts.get(1));
            }
        }

        modelProcess.addGlobalVariable(model);
        return themeService.render("post");
    }

    public String archives(Model model) {

        Map<String, Map<String, List<PostListVO>>> archives = postsService.convertArchivesVO(postsService.list());

        model.addAttribute("archives", archives);
        modelProcess.addGlobalVariable(model);
        return themeService.render("archives");
    }

    public String search(String keyword, Model model) {

        List<PostListVO> posts = postsService.convertToListVO(postsService.listSearch(keyword));

        model.addAttribute("posts", posts);
        model.addAttribute("keyword", keyword);
        modelProcess.addGlobalVariable(model);
        return themeService.render("search");
    }
}
