package run.eternal.app.controller.content.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import run.eternal.app.model.dto.link.LinkMinimalDTO;
import run.eternal.app.service.LinksService;
import run.eternal.app.service.ThemeService;

import java.util.List;

/**
 * @author : lwj
 * @since : 2020-08-23
 */
@Component
public class LinkModel {

    @Autowired
    private LinksService linksService;

    @Autowired
    private ThemeService themeService;

    @Autowired
    private ModelProcess modelProcess;

    public String list(Model model) {

        List<LinkMinimalDTO> links = linksService.convertTo(linksService.list());

        model.addAttribute("links", links);
        modelProcess.addGlobalVariable(model);
        return themeService.render("links");
    }

}
