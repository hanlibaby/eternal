package run.eternal.app.controller.content;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import run.eternal.app.controller.content.model.*;

/**
 * Content controller
 *
 * @author : lwj
 * @since : 2020-08-22
 */
@Controller
@RequestMapping
public class ContentController {

    @Autowired
    private PostModel postModel;

    @Autowired
    private CategoryModel categoryModel;

    @Autowired
    private TagModel tagModel;

    @Autowired
    private JournalModel journalModel;

    @Autowired
    private SheetModel sheetModel;

    @Autowired
    private LinkModel linkModel;

    /**
     * Render post page
     *
     * @param postUrl post url
     * @param model   Model
     * @return templates path: themes/{theme}/post.html
     */
    @GetMapping("/archives/{postUrl}")
    public String postContent(@PathVariable(value = "postUrl") String postUrl, Model model) {
        return postModel.content(postUrl, model);
    }

    /**
     * Render archives page
     *
     * @param model Model
     * @return templates path: themes/{theme}/archives.html
     */
    @GetMapping("/archives")
    public String archivesContent(Model model) {
        return postModel.archives(model);
    }

    /**
     * Render category page
     *
     * @param category category name
     * @param model    Model
     * @return templates path: themes/{theme}/categories.html
     */
    @GetMapping("/categories/{category}")
    public String categoryContent(@PathVariable(value = "category") String category, Model model) {
        return categoryModel.listPost(model, category);
    }

    /**
     * Render tags page
     *
     * @param model Model
     * @return templates path: themes/{theme}/tags.html
     */
    @GetMapping("/tags")
    public String tags(Model model) {
        return tagModel.list(model);
    }

    /**
     * Render tag post page
     *
     * @param tag   tag name
     * @param model Model
     * @return templates path: themes/{theme}/tag.html
     */
    @GetMapping("/tags/{tag}")
    public String tags(@PathVariable(value = "tag") String tag, Model model) {
        return tagModel.content(tag, model);
    }

    /**
     * Render journals page
     *
     * @param model Model
     * @return templates path: themes/{theme}/journals.html
     */
    @GetMapping("/journals")
    public String journals(Model model) {
        return journalModel.list(model);
    }

    /**
     * Render custom sheet
     *
     * @param slug  sheet slug
     * @param model Model
     * @return templates path: themes/{theme}/sheet.html
     */
    @GetMapping("/s/{slug}")
    public String sheet(@PathVariable(value = "slug") String slug, Model model) {
        return sheetModel.content(slug, model);
    }

    /**
     * Render link page
     *
     * @param model Model
     * @return templates path: themes/{theme}/links.html
     */
    @GetMapping("/links")
    public String links(Model model) {
        return linkModel.list(model);
    }

    /**
     * Render search page
     *
     * @param keyword post keyword
     * @param model   Model
     * @return templates path: themes/{theme}/index.html
     */
    @GetMapping("/search")
    public String search(@RequestParam("keyword") String keyword, Model model) {
        return postModel.search(keyword, model);
    }

}
