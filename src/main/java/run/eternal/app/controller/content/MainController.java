package run.eternal.app.controller.content;

import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import run.eternal.app.exception.ServiceException;
import run.eternal.app.model.entity.User;
import run.eternal.app.model.enums.ResultStatus;
import run.eternal.app.model.properties.BlogProperties;
import run.eternal.app.model.support.EternalConst;
import run.eternal.app.service.OptionsService;
import run.eternal.app.service.UserService;
import run.eternal.app.utils.EternalUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Main controller
 *
 * @author : lwj
 * @since : 2020-08-12
 */
@Controller
@Api(value = "Main Controller", tags = "MainController")
public class MainController {

    @Autowired
    private UserService userService;

    @Autowired
    private OptionsService optionsService;

    @GetMapping("/version")
    @ResponseBody
    public String version() {
        return EternalConst.ETERNAL_VERSION;
    }

    @GetMapping("/avatar")
    public void avatar(HttpServletResponse response) throws IOException {
        User user = userService.getCurrentUser().orElseThrow(() -> new ServiceException(ResultStatus.USER_NOT_EXIST));
        if (StringUtils.isNotEmpty(user.getAvatar())) {
            response.sendRedirect(EternalUtils.normalizeUrl(user.getAvatar()));
        }
    }

    @GetMapping("/logo")
    public void logo(HttpServletResponse response) throws IOException {
        String blogLogo = optionsService.getByProperty(BlogProperties.BLOG_LOGO).orElse("").toString();
        if (StringUtils.isNotEmpty(blogLogo)) {
            response.sendRedirect(EternalUtils.normalizeUrl(blogLogo));
        }
    }

    @GetMapping("favicon.ico")
    public void favicon(HttpServletResponse response) throws IOException {
        String favicon = optionsService.getByProperty(BlogProperties.BLOG_FAVICON).orElse("").toString();
        if (StringUtils.isNotEmpty(favicon)) {
            response.sendRedirect(EternalUtils.normalizeUrl(favicon));
        }
    }

    @GetMapping({"/admin", "/login", "/install"})
    public String installation() {
        return "admin/index";
    }

}
