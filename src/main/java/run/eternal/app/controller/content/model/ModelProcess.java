package run.eternal.app.controller.content.model;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import run.eternal.app.model.dto.menu.MenuDTO;
import run.eternal.app.model.entity.User;
import run.eternal.app.model.properties.BlogProperties;
import run.eternal.app.service.*;

import java.util.List;
import java.util.Map;

/**
 * @author : lwj
 * @since : 2020-08-21
 */
@Component
public class ModelProcess {

    @Autowired
    private ThemeService themeService;

    @Autowired
    private ThemeSettingsService themeSettingsService;

    @Autowired
    private OptionsService optionsService;

    @Autowired
    private UserService userService;

    @Autowired
    private MenusService menusService;

    public void addGlobalVariable(Model model) {
        // get theme settings
        Map<String, Object> settings = themeSettingsService.listAsMapBy(themeService.getActivatedThemeId());

        // get blog url
        String blogBaseUrl = optionsService.getBlogBaseUrl();

        // get blog title
        String blogTitle = optionsService.getBlogTitle();

        // get blog footer info
        String blogFooterInfo = optionsService.getByPropertyOrDefault(BlogProperties.BLOG_FOOTER_INFO, String.class, "");

        // get blog logo
        String blogLogo = optionsService.getByPropertyOrDefault(BlogProperties.BLOG_LOGO, String.class, "");

        // get blog favicon
        String blogFavicon = optionsService.getByPropertyOrDefault(BlogProperties.BLOG_FAVICON, String.class, "");

        // get theme base
        String themeBase = StringUtils.removeEnd(blogBaseUrl, "/") + "/" +  themeService.getActivatedThemePath();

        // get theme name
        String themeName = themeService.getActivatedThemeId();

        User user = userService.getCurrentUser().orElse(null);

        List<MenuDTO> menus = menusService.convertTo(menusService.list());

        model.addAttribute("settings", settings);
        model.addAttribute("menus", menus);
        model.addAttribute("blog_url", blogBaseUrl);
        model.addAttribute("blog_title", blogTitle);
        model.addAttribute("blog_logo", blogLogo);
        model.addAttribute("blog_favicon", blogFavicon);
        model.addAttribute("blog_footer_info", blogFooterInfo);
        model.addAttribute("theme_base", themeBase);
        model.addAttribute("theme_name", themeName);
        model.addAttribute("user", user);
    }

}
