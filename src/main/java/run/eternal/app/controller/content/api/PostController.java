package run.eternal.app.controller.content.api;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : lwj
 * @since : 2020-08-12
 */
@RestController
@RequestMapping("/api/content/post")
@Api(value = "前台文章控制器", tags = "PostController")
public class PostController {


}
