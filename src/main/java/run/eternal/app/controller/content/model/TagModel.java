package run.eternal.app.controller.content.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import run.eternal.app.model.dto.tag.TagDTO;
import run.eternal.app.model.entity.Tag;
import run.eternal.app.model.vo.PostListVO;
import run.eternal.app.service.PostTagsService;
import run.eternal.app.service.PostsService;
import run.eternal.app.service.TagsService;
import run.eternal.app.service.ThemeService;

import java.util.List;

/**
 * @author : lwj
 * @since : 2020-08-23
 */
@Component
public class TagModel {

    @Autowired
    private TagsService tagsService;

    @Autowired
    private PostTagsService postTagsService;

    @Autowired
    private PostsService postsService;

    @Autowired
    private ThemeService themeService;

    @Autowired
    private ModelProcess modelProcess;

    public String content(String tag, Model model) {

        Tag currentTag = tagsService.getByName(tag);

        List<String> postIds = postTagsService.listPostIdsByTagId(currentTag.getId());

        List<PostListVO> posts = postsService.convertToListVO(postsService.getByIds(postIds));

        model.addAttribute("tag", currentTag);
        model.addAttribute("posts", posts);
        modelProcess.addGlobalVariable(model);
        return themeService.render("tag");
    }

    public String list(Model model) {

        List<TagDTO> tags = tagsService.convertTo(tagsService.list());

        model.addAttribute("tags", tags);
        modelProcess.addGlobalVariable(model);
        return themeService.render("tags");
    }

}
