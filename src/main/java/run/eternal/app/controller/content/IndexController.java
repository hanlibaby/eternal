package run.eternal.app.controller.content;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import run.eternal.app.controller.content.model.PostModel;

/**
 * Index controller
 *
 * @author : lwj
 * @since : 2020-08-21
 */
@Controller
@RequestMapping
public class IndexController {

    @Autowired
    private PostModel postModel;

    /**
     * Render blog index
     *
     * @param model model
     * @return templates path: themes/{theme}/index.html
     */
    @GetMapping
    public String index(Model model) {
        return this.index(model, 1);
    }

    /**
     * Render blog index
     *
     * @param model model
     * @param page  current page
     * @return templates path: themes/{theme}/index.html
     */
    @GetMapping("/page/{page}")
    public String index(Model model, @PathVariable(value = "page") Integer page) {
        return postModel.list(page, model);
    }

}
