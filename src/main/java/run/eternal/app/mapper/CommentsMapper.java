package run.eternal.app.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import run.eternal.app.model.entity.Comment;

import java.util.List;

/**
 * <pre>
 *  Mapper 接口
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-11
 */
@Mapper
public interface CommentsMapper extends BaseMapper<Comment> {

    /**
     * 筛选出文章评论
     *
     * @param top 前top条
     * @return 文章评论集合
     */
    @Select("SELECT * FROM blog_comments c WHERE c.post_id IN (SELECT p.id FROM blog_posts p) ORDER BY c.gmt_create LIMIT 0,#{top}")
    List<Comment> listPostComments(int top);

    /**
     * 筛选出文章评论
     *
     * @param top           前top条
     * @param commentStatus 评论状态
     * @return 集合
     */
    @Select("SELECT * FROM blog_comments c WHERE c.post_id IN (SELECT p.id FROM blog_posts p) AND c.status = #{commentStatus} ORDER BY c.gmt_create LIMIT 0,#{top}")
    List<Comment> listPostCommentsByStatus(int top, int commentStatus);

    /**
     * 筛选出页面评论
     *
     * @param top 前top条
     * @return 页面评论集合
     */
    @Select("SELECT * FROM blog_comments c WHERE c.post_id IN (SELECT s.id FROM blog_sheets s) ORDER BY c.gmt_create LIMIT 0,#{top}")
    List<Comment> listSheetComments(int top);

    /**
     * 筛选出页面评论
     *
     * @param top           前top条
     * @param commentStatus 评论状态
     * @return 集合
     */
    @Select("SELECT * FROM blog_comments c WHERE c.post_id IN (SELECT s.id FROM blog_sheets s) AND c.status = #{commentStatus} ORDER BY c.gmt_create LIMIT 0,#{top}")
    List<Comment> listSheetCommentsByStatus(int top, int commentStatus);

}
