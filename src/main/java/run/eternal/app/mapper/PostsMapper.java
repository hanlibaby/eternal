package run.eternal.app.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import run.eternal.app.model.entity.Post;

import java.util.List;

/**
 * <pre>
 *      文章 Mapper 接口
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Mapper
public interface PostsMapper extends BaseMapper<Post> {

    /**
     * 获取所有文章访问总次数
     *
     * @return 总访问数量
     */
    @Select("SELECT SUM(p.visit) FROM blog_posts p")
    Long countVisit();

    /**
     * 统计点赞人数
     *
     * @return 点赞人数
     */
    @Select("SELECT SUM(p.likes) FROM blog_posts p")
    Long countLike();

    /**
     * Query prev post, current post, next post
     *
     * @param id current post id
     * @return three post
     */
    @Select("SELECT * FROM blog_posts WHERE id IN((SELECT id FROM blog_posts WHERE id<#{id} ORDER BY id DESC LIMIT 1),#{id},(SELECT id FROM blog_posts WHERE id>#{id} ORDER BY id LIMIT 1)) ORDER BY " +
            "id")
    List<Post> selectPrevAndNext(String id);

    /**
     * Increase post visit
     *
     * @param postId post id
     */
    @Update("UPDATE blog_posts SET visit = visit + 1 WHERE id = #{postId}")
    void increaseVisit(String postId);
}
