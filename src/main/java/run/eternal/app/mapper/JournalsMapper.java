package run.eternal.app.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import run.eternal.app.model.entity.Journal;

/**
 * <pre>
 *      随记 Mapper 接口
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Mapper
public interface JournalsMapper extends BaseMapper<Journal> {

}
