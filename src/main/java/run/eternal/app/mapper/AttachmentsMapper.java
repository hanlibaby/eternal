package run.eternal.app.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import run.eternal.app.model.entity.Attachment;

import java.util.List;

/**
 * <pre>
 *      附件 Mapper 接口
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Mapper
public interface AttachmentsMapper extends BaseMapper<Attachment> {

    /**
     * 获取所有附件类型
     *
     * @return 附件类型列表
     */
    @Select("SELECT DISTINCT media_type FROM blog_attachments")
    List<String> listMedia();

    /**
     * 获取当前所有附件存储位置类型
     *
     * @return 附件存储位置列表
     */
    @Select("SELECT DISTINCT storage_type FROM blog_attachments")
    List<String> listStorage();

}
