package run.eternal.app.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import run.eternal.app.model.entity.Logs;

import java.util.List;

/**
 * <pre>
 *      日志 Mapper 接口
 * </pre>
 *
 * @author lwjppz
 * @since 2020-08-08
 */
@Mapper
public interface LogsMapper extends BaseMapper<Logs> {

    /**
     * list all logTypes
     *
     * @return a list logTypes
     */
    @Select("SELECT DISTINCT log_type from blog_logs")
    List<String> listLogTypes();

}
