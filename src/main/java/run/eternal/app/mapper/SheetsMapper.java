package run.eternal.app.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import run.eternal.app.model.entity.Sheet;

/**
 * <pre>
 *     自定义页面 mapper接口
 * </pre>
 *
 * @author : lwj
 * @since : 2020-08-11
 */
@Mapper
public interface SheetsMapper extends BaseMapper<Sheet> {

    /**
     * 获取所有自定义页面访问总次数
     *
     * @return 总访问次数
     */
    @Select("SELECT SUM(s.visit) FROM blog_sheets s")
    Long countVisit();

    /**
     * 统计点赞人数
     *
     * @return 点赞人数
     */
    @Select("SELECT SUM(s.likes) FROM blog_sheets s")
    Long countLike();

    /**
     * Increase sheet visit
     *
     * @param sheetId sheet id
     */
    @Update("UPDATE blog_sheets SET visit = visit + 1 WHERE id = #{sheetId}")
    void increaseVisit(String sheetId);

}
