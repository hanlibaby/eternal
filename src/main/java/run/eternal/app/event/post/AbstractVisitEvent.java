package run.eternal.app.event.post;

import org.springframework.context.ApplicationEvent;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

/**
 * Post visit event.
 *
 * @author : lwj
 * @since : 2020-08-22
 */
public abstract class AbstractVisitEvent extends ApplicationEvent {

    private final String id;

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     * @param id     id
     */
    public AbstractVisitEvent(@NonNull Object source, @NonNull String id) {
        super(source);

        Assert.notNull(id, "Id must not be null");
        this.id = id;
    }

    @NonNull
    public String getId() {
        return id;
    }

}
