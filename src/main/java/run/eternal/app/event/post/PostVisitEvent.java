package run.eternal.app.event.post;

import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import run.eternal.app.utils.ServiceUtils;

/**
 * Post visit event.
 *
 * @author : lwj
 * @since : 2020-08-22
 */
public class PostVisitEvent extends AbstractVisitEvent {

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     * @param postId post id must not be null
     */
    public PostVisitEvent(Object source, @NonNull String postId) {
        super(source, postId);
        Assert.notNull(postId, "Post id must not be null.");

    }
}
