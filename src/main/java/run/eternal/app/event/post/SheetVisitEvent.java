package run.eternal.app.event.post;

/**
 * Sheet visit event.
 *
 * @author : lwj
 * @since : 2020-08-22
 */
public class SheetVisitEvent extends AbstractVisitEvent {

    /**
     * Create a new ApplicationEvent.
     *
     * @param source  the object on which the event initially occurred (never {@code null})
     * @param sheetId sheet id must not be null
     */
    public SheetVisitEvent(Object source, String sheetId) {
        super(source, sheetId);
    }

}
