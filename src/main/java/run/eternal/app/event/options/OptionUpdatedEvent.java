package run.eternal.app.event.options;

import org.springframework.context.ApplicationEvent;

/**
 * @author : lwj
 * @since : 2020-09-05
 */
public class OptionUpdatedEvent extends ApplicationEvent {

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public OptionUpdatedEvent(Object source) {
        super(source);
    }
}
