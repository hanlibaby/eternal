package run.eternal.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Eternal 启动类
 *
 * @author : lwj
 * @since : 2020-08-05
 */
@SpringBootApplication
@EnableAsync
public class Application {
    public static void main(String[] args) {
        // 启动应用
        SpringApplication.run(Application.class, args);
    }
}
