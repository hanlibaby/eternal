package run.eternal.app.handler.theme;

import org.springframework.lang.NonNull;
import run.eternal.app.handler.theme.support.ThemeProperty;

import java.io.IOException;

/**
 * Theme file resolver.
 *
 * @author : lwj
 * @since : 2020-08-21
 */
public interface ThemePropertyResolver {

    /**
     * Resolves the theme file.
     *
     * @param content file content must not be null
     * @return theme file
     * @throws IOException when file not find
     */
    @NonNull
    ThemeProperty resolve(@NonNull String content) throws IOException;

}
