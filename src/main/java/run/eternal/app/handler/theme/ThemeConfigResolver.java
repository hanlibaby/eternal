package run.eternal.app.handler.theme;

import org.springframework.lang.NonNull;
import run.eternal.app.handler.theme.support.Group;

import java.io.IOException;
import java.util.List;

/**
 * Theme config resolver interface.
 *
 * @author : lwj
 * @since : 2020-08-21
 */
public interface ThemeConfigResolver {

    /**
     * Resolves content as group list.
     *
     * @param content content must not be blank
     * @return a list of group
     * @throws IOException throws when content conversion fails
     */
    @NonNull
    List<Group> resolve(@NonNull String content) throws IOException;

}
