package run.eternal.app.handler.theme.support;

import lombok.Data;

/**
 * Theme configuration: option entity.
 *
 * @author : lwj
 * @since : 2020-08-21
 */
@Data
public class Option {

    /**
     * Option label.
     */
    private String label;

    /**
     * Option value.
     */
    private Object value;
}
