package run.eternal.app.handler.theme.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import run.eternal.app.handler.theme.ThemeConfigResolver;
import run.eternal.app.handler.theme.support.Group;
import run.eternal.app.handler.theme.support.Item;
import run.eternal.app.handler.theme.support.Option;
import run.eternal.app.model.enums.DataType;
import run.eternal.app.model.enums.InputType;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Yaml theme config resolver
 *
 * @author : lwj
 * @since : 2020-08-21
 */
@Slf4j
@Service
public class YamlThemeConfigResolverImpl implements ThemeConfigResolver {

    private final ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory());

    @NonNull
    @Override
    public List<Group> resolve(@NonNull String content) throws IOException {
        return handleTabs(yamlMapper.readValue(content, Object.class));
    }

    @SuppressWarnings("unchecked")
    private List<Group> handleTabs(@Nullable Object config) {
        List<Group> groups = new LinkedList<>();

        if (config instanceof List) {
            List<Object> configList = (List<Object>) config;

            // Resolve tab
            configList.forEach(tabYaml -> {
                // tabYaml should be map
                if (!(tabYaml instanceof Map)) {
                    return;
                }

                Map<String, Object> tabMap = (Map<String, Object>) tabYaml;

                Group group = new Group();

                group.setName(tabMap.get("name").toString());
                group.setLabel(tabMap.get("label").toString());

                // Handle items
                group.setItems(handleItems(tabMap.get("items")));

                groups.add(group);
            });

        } else if (config instanceof Map) {
            Map<String, Object> configMap = (Map<String, Object>) config;
            configMap.forEach((key, value) -> {
                // key: tab name
                // value: tab property, should be a map
                if (!(value instanceof Map)) {
                    return;
                }

                Map<String, Object> tabMap = (Map<String, Object>) value;

                Group group = new Group();

                group.setName(key);
                group.setLabel(tabMap.get("label").toString());

                // Handle items
                group.setItems(handleItems(tabMap.get("items")));

                groups.add(group);
            });
        }

        return groups;
    }

    @SuppressWarnings("unchecked")
    private List<Item> handleItems(@Nullable Object items) {

        if (items == null) {
            return Collections.emptyList();
        }

        List<Item> result = new LinkedList<>();

        if (items instanceof List) {
            ((List<Object>) items).forEach(itemYaml -> {
                if (!(itemYaml instanceof Map)) {
                    return;
                }

                // Should be Map
                Map<String, Object> itemMap = (Map<String, Object>) itemYaml;

                // Build item
                Item item = new Item();

                item.setName(itemMap.get("name").toString());
                item.setLabel(itemMap.getOrDefault("label", item.getName()).toString());
                Object dataType = itemMap.getOrDefault("data-type", itemMap.get("dataType"));
                item.setType(InputType.typeOf(itemMap.get("type")));
                item.setDataType(item.getType().equals(InputType.SWITCH) ? DataType.BOOL : DataType.typeOf(dataType));
                item.setDefaultValue(itemMap.get("default"));
                item.setPlaceholder(itemMap.getOrDefault("placeholder", "").toString());
                item.setDescription(itemMap.getOrDefault("description", "").toString());

                // Handle options
                item.setOptions(handleOptions(itemMap.get("options")));

                // Add item
                result.add(item);
            });
        } else if (items instanceof Map) {
            Map<String, Object> itemsMap = (Map<String, Object>) items;

            itemsMap.forEach((key, value) -> {
                if (!(value instanceof Map)) {
                    return;
                }

                Map<String, Object> itemMap = (Map<String, Object>) value;

                // key: item name
                Item item = new Item();
                item.setName(key);
                item.setLabel(itemMap.getOrDefault("label", item.getName()).toString());
                Object dataType = itemMap.getOrDefault("data-type", itemMap.get("dataType"));
                item.setType(InputType.typeOf(itemMap.get("type")));
                item.setDataType(item.getType().equals(InputType.SWITCH) ? DataType.BOOL : DataType.typeOf(dataType));
                item.setDefaultValue(itemMap.get("default"));
                item.setPlaceholder(itemMap.getOrDefault("placeholder", "").toString());
                item.setDescription(itemMap.getOrDefault("description", "").toString());

                // Handle options
                item.setOptions(handleOptions(itemMap.get("options")));

                // Add item
                result.add(item);
            });
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    private List<Option> handleOptions(@Nullable Object options) {

        if (options == null) {
            return Collections.emptyList();
        }

        List<Option> result = new LinkedList<>();

        if (options instanceof List) {
            ((List<Object>) options).forEach(optionYaml -> {
                // optionYaml should be Map
                if (!(optionYaml instanceof Map)) {
                    return;
                }

                Map<String, Object> optionMap = (Map<String, Object>) optionYaml;

                // Build option
                Option option = new Option();
                option.setValue(optionMap.get("value"));
                option.setLabel(optionMap.get("label").toString());

                result.add(option);
            });
        } else if (options instanceof Map) {
            Map<String, Object> optionsMap = (Map<String, Object>) options;
            optionsMap.forEach((key, value) -> {
                // key: option value
                // value: Map that contains option label

                if (!(value instanceof Map)) {
                    return;
                }

                Map<String, Object> optionMap = (Map<String, Object>) value;


                Option option = new Option();
                option.setValue(key);
                option.setLabel(optionMap.get("label").toString());

                result.add(option);
            });
        }

        return result;
    }
}
