package run.eternal.app.handler.theme.support;

import lombok.Data;

import java.util.List;

/**
 * Theme configuration: group entity.
 *
 * @author : lwj
 * @since : 2020-08-21
 */
@Data
public class Group {

    /**
     * Tab name.
     */
    private String name;

    /**
     * Tab label.
     */
    private String label;

    /**
     * Tab's items, default is empty list.
     */
    private List<Item> items;

}
