package run.eternal.app.handler.theme.impl;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import run.eternal.app.handler.theme.ThemePropertyResolver;
import run.eternal.app.handler.theme.support.ThemeProperty;
import run.eternal.app.theme.YamlResolver;

import java.io.IOException;

/**
 * Yaml theme property resolver
 *
 * @author : lwj
 * @since : 2020-08-21
 */
@Service
public class YamlThemePropertyResolverImpl implements ThemePropertyResolver {

    @Override
    @NonNull
    public ThemeProperty resolve(@NonNull String content) throws IOException {
        Assert.hasText(content, "Theme file content must not be null");

        return YamlResolver.INSTANCE.getYamlMapper().readValue(content, ThemeProperty.class);
    }

}
