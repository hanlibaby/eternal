package run.eternal.app.handler.file.support;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import run.eternal.app.exception.FileOperationException;
import run.eternal.app.exception.RepeatTypeException;
import run.eternal.app.model.entity.Attachment;
import run.eternal.app.model.enums.ResultStatus;
import run.eternal.app.model.enums.ValueEnum;
import run.eternal.app.model.enums.StorageType;
import run.eternal.app.model.support.UploadResult;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

/**
 * File handler executed
 *
 * @author : lwj
 * @since : 2020-08-18
 */
@Slf4j
@Component
public class FileHandlers {

    /**
     * The file handlers container
     */
    private final ConcurrentHashMap<StorageType, FileHandler> fileHandlers = new ConcurrentHashMap<>(8);

    public FileHandlers(ApplicationContext applicationContext) {
        // add all file handlers
        addFileHandlers(applicationContext.getBeansOfType(FileHandler.class).values());
        log.info("{} file handlers has been registry.", fileHandlers.size());
    }

    /**
     * Executed upload file
     *
     * @param file        file must not be null
     * @param storageType current storage type
     * @return upload result
     * @throws FileOperationException When failed to upload attachment or no file handler available
     */
    @NonNull
    public UploadResult upload(@NonNull MultipartFile file, @NonNull StorageType storageType) {
        return getSupportedType(storageType).upload(file);
    }

    /**
     * Executed delete file
     *
     * @param attachment a attachment entity
     * @throws FileOperationException When failed to upload attachment or no file handler available
     */
    public void delete(@NonNull Attachment attachment) {
        Assert.notNull(attachment, "attachment must not be null.");
        getSupportedType(ValueEnum.valueToEnum(StorageType.class, attachment.getStorageType()))
                .delete(attachment.getFileKey());
    }

    /**
     * Add file handlers
     *
     * @param fileHandlers collections of file handlers
     */
    public void addFileHandlers(@Nullable Collection<FileHandler> fileHandlers) {
        if (!CollectionUtils.isEmpty(fileHandlers)) {
            for (FileHandler handler : fileHandlers) {
                if (this.fileHandlers.containsKey(handler.getStorageType())) {
                    // throws a RepeatTypeException when current file handler isn't unique
                    throw new RepeatTypeException(ResultStatus.ATTACHMENT_HANDLER_UNIQUE);
                }
                // to add
                this.fileHandlers.put(handler.getStorageType(), handler);
            }
        }
    }

    /**
     * Get file handler by storage type
     *
     * @param type current storage type
     * @return match file handler
     */
    private FileHandler getSupportedType(StorageType type) {
        FileHandler handler = fileHandlers.getOrDefault(type, fileHandlers.get(StorageType.LOCAL));
        if (handler == null) {
            throw new FileOperationException(ResultStatus.FILE_OPERATION);
        }
        return handler;
    }
}
