USE `eternal-blog`;

Delete FROM blog_attachments;

DELETE FROM blog_categories;

DELETE FROM blog_comments;

DELETE FROM blog_journals;

DELETE FROM blog_links;

DELETE FROM blog_logs;

DELETE FROM blog_menus;

DELETE FROM blog_options;

DELETE FROM blog_post_categories;

DELETE FROM blog_post_tags;

DELETE FROM blog_posts;

DELETE FROM blog_sheets;

DELETE FROM blog_tags;

DELETE FROM blog_theme_settings;

DELETE FROM blog_user;