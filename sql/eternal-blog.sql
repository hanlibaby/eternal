/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.25 : Database - eternal-blog
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=''NO_AUTO_VALUE_ON_ZERO'' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`eternal-blog` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `eternal-blog`;

/*Table structure for table `blog_attachments` */

DROP TABLE IF EXISTS `blog_attachments`;

CREATE TABLE `blog_attachments` (
  `id` char(19) NOT NULL COMMENT ''附件id'',
  `name` varchar(255) NOT NULL COMMENT ''附件名'',
  `file_key` varchar(1023) NOT NULL COMMENT ''文件 key'',
  `storage_type` varchar(30) NOT NULL COMMENT ''附件存储位置类型'',
  `media_type` varchar(127) NOT NULL COMMENT ''附件类型'',
  `width` int(10) DEFAULT NULL COMMENT ''宽（附件是图片时才有效）'',
  `height` int(10) DEFAULT NULL COMMENT ''高（附件是图片时才有效）'',
  `size` bigint(19) NOT NULL COMMENT ''大小'',
  `suffix` varchar(50) DEFAULT NULL COMMENT ''文件后缀（jpg、txt...）'',
  `path` varchar(1023) NOT NULL COMMENT ''地址'',
  `thumb_path` varchar(1023) NOT NULL COMMENT ''缩略图地址'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''附件'';

/*Table structure for table `blog_categories` */

DROP TABLE IF EXISTS `blog_categories`;

CREATE TABLE `blog_categories` (
  `id` char(19) NOT NULL COMMENT ''分类id'',
  `name` varchar(255) NOT NULL COMMENT ''分类名'',
  `alias` varchar(255) DEFAULT NULL COMMENT ''分类别名'',
  `thumbnail` varchar(1023) DEFAULT NULL COMMENT ''分类页面封面图'',
  `description` varchar(1023) DEFAULT NULL COMMENT ''分类描述'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''分类'';

/*Table structure for table `blog_comments` */

DROP TABLE IF EXISTS `blog_comments`;

CREATE TABLE `blog_comments` (
  `id` char(19) NOT NULL COMMENT ''评论id'',
  `author` varchar(50) NOT NULL COMMENT ''评论作者'',
  `email` varchar(255) NOT NULL COMMENT ''电子邮件'',
  `author_url` varchar(511) NOT NULL COMMENT ''作者地址'',
  `content` varchar(1023) NOT NULL COMMENT ''评论内容'',
  `gravatar_md5` varchar(255) DEFAULT NULL COMMENT ''头像md5加密'',
  `ip_address` varchar(127) DEFAULT NULL COMMENT ''评论者IP地址'',
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT ''0'' COMMENT ''是否是管理员（0-表示不是管理员，1-表示是管理员）'',
  `post_id` char(19) NOT NULL COMMENT ''评论文章id'',
  `parent_id` char(19) NOT NULL DEFAULT '''' COMMENT ''父评论id'',
  `status` tinyint(1) unsigned NOT NULL DEFAULT ''0'' COMMENT ''评论状态（0-表示待审核，1-表示已通过，2-表示回收站）'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Table structure for table `blog_journals` */

DROP TABLE IF EXISTS `blog_journals`;

CREATE TABLE `blog_journals` (
  `id` char(19) NOT NULL COMMENT ''随记id'',
  `content` text NOT NULL COMMENT ''随记内容'',
  `is_private` tinyint(1) unsigned NOT NULL DEFAULT ''0'' COMMENT ''是否公开（0-不公开，1-公开）'',
  `likes` bigint(19) DEFAULT ''0'' COMMENT ''点赞人数'',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT ''0'' COMMENT ''逻辑删除 1（true）已删除， 0（false）未删除'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''随记'';

/*Table structure for table `blog_links` */

DROP TABLE IF EXISTS `blog_links`;

CREATE TABLE `blog_links` (
  `id` char(19) NOT NULL COMMENT ''友链id'',
  `name` varchar(255) NOT NULL COMMENT ''友链名称'',
  `url` varchar(1023) NOT NULL COMMENT ''友链地址'',
  `logo` varchar(1023) DEFAULT NULL COMMENT ''友链logo'',
  `sort` int(10) unsigned NOT NULL DEFAULT ''0'' COMMENT ''排序'',
  `description` varchar(255) DEFAULT NULL COMMENT ''友链描述'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''友链'';

/*Table structure for table `blog_logs` */

DROP TABLE IF EXISTS `blog_logs`;

CREATE TABLE `blog_logs` (
  `id` char(19) NOT NULL COMMENT ''日志id'',
  `log_type` varchar(255) NOT NULL COMMENT ''日志类型'',
  `request_method` varchar(50) NOT NULL COMMENT ''请求方式'',
  `status` varchar(15) NOT NULL COMMENT ''操作成功与否状态'',
  `params` text NOT NULL COMMENT ''请求参数'',
  `ip_address` varchar(123) NOT NULL COMMENT ''IP地址'',
  `request_time` varchar(50) NOT NULL COMMENT ''请求执行时间'',
  `ope_method` varchar(255) NOT NULL COMMENT ''操作方法'',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT ''0'' COMMENT ''逻辑删除 1（true）已删除， 0（false）未删除'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''日志'';

/*Table structure for table `blog_menus` */

DROP TABLE IF EXISTS `blog_menus`;

CREATE TABLE `blog_menus` (
  `id` char(19) NOT NULL COMMENT ''菜单id'',
  `name` varchar(50) NOT NULL COMMENT ''菜单名'',
  `icon` varchar(50) DEFAULT NULL COMMENT ''菜单图标（font-awesome）'',
  `parent_id` char(19) DEFAULT '''' COMMENT ''父id'',
  `target` varchar(20) DEFAULT NULL COMMENT ''打开方式（_self、_blank）'',
  `sort` int(10) unsigned NOT NULL DEFAULT ''0'' COMMENT ''排序'',
  `url` varchar(1023) NOT NULL COMMENT ''菜单的地址'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''菜单'';

/*Table structure for table `blog_options` */

DROP TABLE IF EXISTS `blog_options`;

CREATE TABLE `blog_options` (
  `id` char(19) NOT NULL COMMENT ''设置内容项id'',
  `option_key` varchar(100) NOT NULL COMMENT ''设置内容key'',
  `option_value` text COMMENT ''设置内容value'',
  `gmt_create` datetime DEFAULT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime DEFAULT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''博客设置'';

/*Table structure for table `blog_post_categories` */

DROP TABLE IF EXISTS `blog_post_categories`;

CREATE TABLE `blog_post_categories` (
  `id` char(19) NOT NULL COMMENT ''id'',
  `category_id` char(19) NOT NULL COMMENT ''分类id'',
  `post_id` char(19) NOT NULL COMMENT ''文章id'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''文章-分类'';

/*Table structure for table `blog_post_tags` */

DROP TABLE IF EXISTS `blog_post_tags`;

CREATE TABLE `blog_post_tags` (
  `id` char(19) NOT NULL COMMENT ''id'',
  `tag_id` char(19) NOT NULL COMMENT ''标签id'',
  `post_id` char(19) NOT NULL COMMENT ''文章id'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''文章-标签'';

/*Table structure for table `blog_posts` */

DROP TABLE IF EXISTS `blog_posts`;

CREATE TABLE `blog_posts` (
  `id` char(19) NOT NULL COMMENT ''文章id'',
  `title` varchar(255) NOT NULL COMMENT ''文章标题'',
  `summary` text COMMENT ''文章摘要'',
  `format_content` text COMMENT ''文章内容的html格式'',
  `original_content` text NOT NULL COMMENT ''文章内容的markdown格式'',
  `allow_comment` tinyint(1) unsigned NOT NULL DEFAULT ''0'' COMMENT ''是否允许评论（0-允许，1-不允许）'',
  `status` tinyint(1) unsigned NOT NULL DEFAULT ''0'' COMMENT ''文章状态（0-表示已发布，1-表示草稿，2-表示回收站）'',
  `thumbnail` varchar(1023) DEFAULT NULL COMMENT ''文章缩略图'',
  `url` varchar(255) DEFAULT NULL COMMENT ''文章地址'',
  `is_top` tinyint(1) unsigned NOT NULL DEFAULT ''0'' COMMENT ''是否置顶'',
  `sort` int(10) unsigned NOT NULL DEFAULT ''0'' COMMENT ''排序（值越大越靠前）'',
  `visit` bigint(19) DEFAULT ''0'' COMMENT ''文章访问人数'',
  `likes` bigint(19) DEFAULT ''0'' COMMENT ''文章点赞人数'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''文章'';

/*Table structure for table `blog_sheets` */

DROP TABLE IF EXISTS `blog_sheets`;

CREATE TABLE `blog_sheets` (
  `id` char(19) NOT NULL COMMENT ''页面id'',
  `title` varchar(255) NOT NULL COMMENT ''页面标题'',
  `format_content` text COMMENT ''页面内容的html格式'',
  `original_content` text NOT NULL COMMENT ''页面内容的markdown格式'',
  `thumbnail` varchar(1023) DEFAULT NULL COMMENT ''自定义页面缩略图'',
  `allow_comment` tinyint(1) unsigned NOT NULL DEFAULT ''0'' COMMENT ''是否允许评论（0-允许，1-不允许）'',
  `status` tinyint(1) unsigned NOT NULL DEFAULT ''0'' COMMENT ''页面状态（0-表示已发布，1-表示草稿，2-表示回收站）'',
  `url` varchar(255) DEFAULT NULL COMMENT ''页面地址'',
  `visit` bigint(19) DEFAULT ''0'' COMMENT ''页面访问人数'',
  `likes` bigint(19) DEFAULT ''0'' COMMENT ''页面点赞人数'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''页面'';

/*Table structure for table `blog_tags` */

DROP TABLE IF EXISTS `blog_tags`;

CREATE TABLE `blog_tags` (
  `id` char(19) NOT NULL COMMENT ''标签id'',
  `name` varchar(255) NOT NULL COMMENT ''标签名'',
  `alias` varchar(255) DEFAULT NULL COMMENT ''标签别名'',
  `thumbnail` varchar(1023) DEFAULT NULL COMMENT ''标签页封面'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''标签'';

/*Table structure for table `blog_theme_settings` */

DROP TABLE IF EXISTS `blog_theme_settings`;

CREATE TABLE `blog_theme_settings` (
  `id` char(19) NOT NULL COMMENT ''主题设置id'',
  `theme_id` varchar(255) NOT NULL COMMENT ''主题id'',
  `setting_key` varchar(255) NOT NULL COMMENT ''主题设置字段key'',
  `setting_value` text NOT NULL COMMENT ''主题设置字段value'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''主题设置'';

/*Table structure for table `blog_user` */

DROP TABLE IF EXISTS `blog_user`;

CREATE TABLE `blog_user` (
  `id` char(19) NOT NULL COMMENT '' 用户id'',
  `username` varchar(50) NOT NULL COMMENT ''用户名'',
  `nickname` varchar(255) NOT NULL COMMENT ''昵称'',
  `password` varchar(255) NOT NULL COMMENT ''密码'',
  `avatar` varchar(1023) DEFAULT NULL COMMENT ''头像地址'',
  `email` varchar(127) DEFAULT NULL COMMENT ''邮箱'',
  `description` varchar(1023) DEFAULT NULL COMMENT ''个人描述'',
  `gmt_create` datetime NOT NULL COMMENT ''创建时间'',
  `gmt_modified` datetime NOT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT=''用户'';

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
